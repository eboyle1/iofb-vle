
<div class="form-group">
	<div class="fieldcontain ${hasErrors(bean: questionGroup, field: 'owner', 'error')} required">
		<label for="questionGroup" class="control-label col-md-3">
			Test
			<span class="required-indicator">*</span>
		</label>
		<div class="col-md-9">
             <f:widget bean="${questionGroup}" property="owner" class="form-check-input" />
		</div>
	</div>
</div>

<div class="form-group">
	<div class="fieldcontain ${hasErrors(bean: questionGroup, field: 'orderNo', 'error')} required">
		<label for="orderNo" class="control-label col-md-3">
			Order
			<span class="required-indicator">*</span>
		</label>
		<div class="col-md-2">
			<g:field name="orderNo" type="number" min="1" max="100" value="${questionGroup?.orderNo}" required=""  class="form-control"/>
		</div>
	</div>
</div>

<%--
<div class="form-group">
    <label for="isVisible" class="col-md-3 control-label">Published </label>
    <div class="col-md-9 ${hasErrors(bean:questionGroup, field:'isVisible', 'has-warning')}">
        <div class="form-checkbox"><f:widget bean="${questionGroup}" property="isVisible" class="form-check-input" /></div>
    </div>
</div>
--%>

<div class="form-group">
	<div class="fieldcontain ${hasErrors(bean: questionGroup, field: 'questionGroupText', 'error')} " required>
		<label for="questionText"  class="control-label col-md-3">
			<g:message code="question.questionGroupText.label" default="Question Group Text " /><span class="required-indicator">*</span>
		</label>
	<div class="col-md-9">
		<ckeditor:editor name="questionGroupText" toolbar="custom" height="200px" width="100%">
	 	${questionGroup?.questionGroupText}
		</ckeditor:editor>
	</div>
</div>