<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>

        <div  role="main">
            <p>Courses ></p>   
        <div class = "title_left">
            <h3>Courses</h3>
        </div>
        <div class = "title_right">
            <sec:ifAnyGranted roles="ROLE_ADMIN">
            <g:link class="create" action="create" class="btn btn-nav">New course</g:link>
            </sec:ifAnyGranted>
        </div>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>
                    <sec:ifAnyGranted roles="ROLE_ADMIN">                 
                    <%--<g:sortableColumn property="name" title="Name" />--%>
                    <th><div class="name-header">Name</div></th>
                    <th><div class="action-header">Action</div></th>
                    </sec:ifAnyGranted>
                </tr>
            </thead>
            <tbody>
                <g:each in="${courseList.sort{it.orderNo}}" status="i" var="courseInstance">
                <g:if test="${courseInstance.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]'}">
                    <tr>                  
                        <td><g:link action="show" id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "name")}</g:link></td>
                        <td>
                        <sec:ifAnyGranted roles="ROLE_ADMIN">
                        <div class="action">
                        <div class="dropdown">
                        <button class="dropbtn" type="button" data-toggle="dropdown">Select <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><g:link action="show" id="${courseInstance.id}">List modules</g:link></li>
                            <li><g:link controller="module" class="create" action="create" params="[course:courseInstance.id]">New module</g:link></li>
                            <li><g:link action="edit" id="${courseInstance.id}">Edit course</g:link></li>
                            <li>
                            <g:form id="${courseInstance.id}" method="DELETE">
<%--
                            <g:actionSubmit class="delete-button" action="delete" value="Delete course" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
--%>
                            <p:renderCourseDeleteLink curCourse="${courseInstance}"/>
                            </g:form>
                            </li>
                        </ul>
                        </div>
                        </div>
                        </sec:ifAnyGranted>
                        </td>
                    </tr>
                </g:if>    
                </g:each>
            </tbody>
        </table>
<%--
        <div class="pagination">
            <p:bootstrapPagination total="${courseCount ?: 0}" domainBean="uk.ac.ed.bric.elearn.vle.Course" />
        </div>
--%>
    </body>
</html>