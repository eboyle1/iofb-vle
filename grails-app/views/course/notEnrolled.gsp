<!doctype html>
<html>
    <head>
        <title>Not Enrolled</title>
        <meta name="layout" content="main">
        <g:if env="development"><asset:stylesheet src="errors.css"/></g:if>
    </head>
    <body>
        <p><b>${course.name}</b></p>
        <p>You are not enrolled on this course.</p>
        <p>If you think this is an error, please contact the course administrators.</p>
    </body>
</html>