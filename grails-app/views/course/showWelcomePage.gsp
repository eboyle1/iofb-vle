<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div  role="main">
            <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${course.id}"><f:display bean="course" property="name" /></g:link> > Welcome </p>
            <h3><%--Course:<br/>--%><f:display bean="course" property="name" /></h3>
        <div class = "title_left">
        </div>
        <div class = "title_right">
        </div>
        </div>

        <div class="showContent">
            <p><f:display bean="course" property="welcomePage" /></p>
        </div>

    </body>
</html>