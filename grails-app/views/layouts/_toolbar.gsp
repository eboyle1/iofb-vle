	<sec:access expression="hasRole('ROLE_ADMIN')">
	  <nav class="navbar navbar-default" role="navigation">
		<ul class="nav navbar-nav">

			<div class="navbar-header">
			<%--
  				<a class="navbar-brand" href="/dummy"><i class="fa fa-cog" aria-hidden="true"></i></a>
  			--%>
			</div>
			
			<li class="dropdown">
				<a href="/dummy" class="dropdown-toggle" data-toggle="dropdown">Manage courses <b class="caret"></b></a>
				<ul class="dropdown-menu">
						<li><g:link controller="course" action="index">List courses</g:link></li>
						<li><g:link controller="course" action="create">New course</g:link></li>				 		
		        </ul>
			</li>

			<li class="dropdown">
			<a href="/dummy" class="dropdown-toggle" data-toggle="dropdown">User test reports <b class="caret"></b></a>
			<ul class="dropdown-menu">

				<li><g:link controller="test" action="userReport1" params="[sort:'userId', order:'asc']">All students test history</g:link></li>
				<li><g:link controller="test" action="userReport2" params="[sort:'userId', order:'asc']">All completed students</g:link></li>
		</ul>	
	</nav>
	</sec:access>

	 
