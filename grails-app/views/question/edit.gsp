<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
        <ckeditor:resources/>
    </head>
    <body>

        <g:render template="/shared-templates/test_head" model="['test':question.owner.owner]"/>
        
        <div class="panel panel-primary">
            <div class="panel-heading">Edit Question</div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                <g:hasErrors bean="${question}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${question}" var="error">
                            <p <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></p>
                        </g:eachError>
                    </div>
                </g:hasErrors>
                </ul>
            
                <g:form role="form" class="form-horizontal" url="[resource:question, action:'update']" method="PUT">

                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <br/>
                <fieldset class="buttons pull-right">
              
                <g:link controller="test" action="show" id="${question.owner.owner.id}" class="btn btn-default" ><i class='fa fa-times-circle' aria-hidden='true'></i> Cancel</g:link>

                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
          
                </fieldset>
                </g:form>
                <br/>
                <br/>
                <hr>

                <g:render template="choices"/>

                <hr> 
            </div>
        </div>

        <script type="text/javascript">

            function deleteChoice(choiceId) {
                var params = {choiceId:choiceId}
                var data = $.param(params)
                $.post("${grailsApplication.config.vle.url}/question/deleteChoice", data, function(response) {
                    $('#choicesDiv').html( response )   
                })
                .fail(function() {
                    $( "#choicesDiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
                })   
            };

            function addChoice(questionId) {
               
                var choiceOrder = $('#choiceOrderField').val()
                var choiceText = $('#choiceTextField').val()
                if ($('#choiceStatusField').prop('checked'))
                {
                    var choiceStatus = "true"
                }
                else
                {
                    var choiceStatus = "false"
                }                  

                var params = {questionId:questionId, choiceOrder:choiceOrder, choiceText:choiceText, choiceStatus:choiceStatus}
                var data = $.param(params)
                var response
                $.post("${grailsApplication.config.vle.url}/question/addChoice",data, function(response) {
                    $('#choicesDiv').html( response )
                })  
                .fail(function() {
                    $( "#choicesDiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
                })   
            };

            $(document).ready(function() {
              
            });

        </script>

    </body>
</html>
