<div id="choicesDiv">
<g:if test="${flash.ajaxerror}">
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				${flash.ajaxerror}
			</div>
	</g:if>

	<form class="form-inline">
		
	    <div  role="main">
        	<div class = "title_left">
			<h4>Choices</h4>
			</div>
            <div class = "title_right-test">
            <g:link controller="test" id="${question.owner.owner.id}" class="show" action="show" class="btn btn-nav">Questions</g:link>
            </div>
        </div>

        <div class="listChoices"> 
		<ul class="list-group">
		<g:each in="${choiceList}" var="choice">
		 	<li class="list-group-item">

		 	<a href="#/" title="Delete choice" data-toggle="tooltip"  onclick="deleteChoice('${choice?.id}')"><i class="fa fa-trash-o fa-lg text-danger pull-right" aria-hidden="true"></i></a>
		 	   
		 	   <p:renderChoice choice="${choice}"/>
		 	
		 	</li>
		</g:each>
		</ul>
		</div>
		
		<g:if test="${choiceList.size() > 0}">
			<g:set var="choiceOrder" value="${choiceList.size()+1}"/>
		</g:if>
		<g:else>
			<g:set var="choiceOrder" value="1"/>
		</g:else>

	Add Choice:<br/>
	  <div class="form-group">
	    <label for="choiceOrderLabel">Order</label>
	    <g:field name="choiceOrderField" type="number" min="1" max="10" size="2" value="${choiceOrder}" class="form-control"/> 
	  </div>
	  
	  <div class="form-group">
	    <label for="choiceText">Text</label>
	    <g:field name="choiceTextField" type="text" size="50" class="form-control"/>
	  </div>

	  <div class="form-group">
	    <label for="choiceStatus">Correct?&nbsp;</label>
	 	<g:checkBox name="choiceStatusField" class="form-check-input"/>
	  </div>

  	 <a href="#/" title='Add choice' data-toggle='tooltip' class="btn btn-primary pull-right"" onclick="addChoice('${question?.id}')"><i class="fa fa-plus-square-o fa-lg" aria-hidden="true"></i> 
                 </a>
	</form>

</div>
<BR>