
<div class="form-group">
	<div class="fieldcontain ${hasErrors(bean: question, field: 'owner', 'error')} required">
		<label for="questionGroup" class="control-label col-md-3">
			Question Group
			<span class="required-indicator">*</span>
		</label>
		<div class="col-md-9">
             <f:widget bean="${question}" property="owner" class="form-check-input" />
		</div>
	</div>
</div>

<div class="form-group">
	<div class="fieldcontain ${hasErrors(bean: question, field: 'orderNo', 'error')} required">
		<label for="orderNo" class="control-label col-md-3">
			Order
			<span class="required-indicator">*</span>
		</label>
		<div class="col-md-2">
			<g:field name="orderNo" type="number" min="1" max="100" value="${question?.orderNo}" required=""  class="form-control"/>
		</div>
	</div>
</div>

<div class="form-group">
	<div class="fieldcontain ${hasErrors(bean: question, field: 'questionText', 'error')} " required>
		<label for="questionText"  class="control-label col-md-3">
			<g:message code="question.questionText.label" default="Question Text " /><span class="required-indicator">*</span>
		</label>
	<div class="col-md-9">
		<ckeditor:editor name="questionText" toolbar="custom" height="200px" width="100%">
	 	${question?.questionText}
		</ckeditor:editor>
	</div>
</div>