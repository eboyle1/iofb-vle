<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>

        <script type="text/javascript">

            function deleteChoice(choiceId) {
                var params = {choiceId:choiceId}
                var data = $.param(params)
                $.post("/question/deleteChoice", data, function(response) {
                    $('#choicesDiv').html( response )   
                })
                .fail(function() {
                    $( "#choicesDiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
                })   
            };
            
            function addChoice(questionId) {
               
                var choiceOrder = $('#choiceOrderField').val()
                var choiceText = $('#choiceTextField').val()
                //var choiceStatus = $('#choiceStatusField').val()
                if ($('#choiceStatusField').prop('checked'))
                {
                    var choiceStatus = "true"
                }
                else
                {
                    var choiceStatus = "false"
                }
                var params = {questionId:questionId, choiceOrder:choiceOrder, choiceText:choiceText, choiceStatus:choiceStatus}
                var data = $.param(params)
                var response
                $.post("/question/addChoice",data, function(response) {
                    $('#choicesDiv').html( response )
                })  
                .fail(function() {
                    $( "#choicesDiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
                })   
            };

        </script>

    </head>
    <body>

<%--
        <a href="#show-question" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="show-question" class="content scaffold-show" role="main">
            <h1><g:message code="default.show.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:display bean="question" />
            <g:form resource="${this.question}" method="DELETE">
                <fieldset class="buttons">
                    <g:link class="edit" action="edit" resource="${this.question}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <input class="delete" type="submit" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                </fieldset>
            </g:form>
        </div>
--%>

<%--
        <g:render template="/shared-templates/test_head"/>
--%>

        <div  role="main">
            <div class = "title_left">
            <h4>Question Group:</h4> <f:display bean="${questionGroup}" property="questionGroupText" />
<%--
            ${questionGroup.questionGroupText}
--%>
            <h4>Question ${question.orderNo}</h4>
            <ul>
                <g:if test="${question?.questionText}">
                <li class="fieldcontain">
                    
                        <span class="property-value" aria-labelledby="questionText-label">
                        ${raw(question.questionText)}
                        
                        </span>
                    
                </li>
                </g:if>
            </ul>
            </div>
<%--
            <div class = "title_right">

            <button class="btn"><g:link controller="test" id="${test.id}" class="show" action="show">Test</g:link></button>

            </div>
--%>
        </div>
<%--
        <div id="show-question" role="main">
            <ul>
                <g:if test="${question?.questionText}">
                <li class="fieldcontain">
                    
                        <span class="property-value" aria-labelledby="questionText-label">
                        ${raw(question.questionText)}
                        
                        </span>
                    
                </li>
                </g:if>
            </ul>
--%>   
            <hr>
          
        <div class="listChoices"> 
            <g:render template="choices"/>
           
        </div>

    </body>
</html>
