
<g:each var="questionGroup" in="${test.questionGroups.sort{it.orderNo}}">

<div class='row'>

<div class='col-md-10'>
<p:renderQuestionGroup questionGroup="${questionGroup}"/>
</div>


<div class='col-md-2'>   
  <g:link controller='questionGroup' action='edit' id='${questionGroup.id}' data-toggle='tooltip' title='Edit question group'><i class='fa fa-pencil-square-o fa-lg' aria-hidden='true'></i></g:link>            &#124; 
  <g:link controller='questionGroup' action='delete' id='${questionGroup.id}' data-confirm='Are you sure you want to delete this question group? This action cannot be undone.'  data-toggle='tooltip' title='Delete question group'><i class='fa fa-trash-o fa-lg text-danger' aria-hidden='true'></i></g:link>
</div>

</div>

  <g:each var="question" in="${questionGroup.questions.sort{it.orderNo}}">

              <div class='row'>
                  <div class='col-md-10'>
                    <p:renderQuestion questionGroup="${questionGroup}" question="${question}"/>
                  </div>

                 <div class='col-md-2'>   
                 <g:link controller='question' action='edit' id='${question.id}'  data-toggle='tooltip' title='Edit question'><i class='fa fa-pencil-square-o fa-lg' aria-hidden='true'></i></g:link>            &#124; 
                 <g:link controller='question' action='delete' id='${question.id}' data-confirm='Are you sure you want to delete this question? This action cannot be undone.'  data-toggle='tooltip' title='Delete question'><i class='fa fa-trash-o fa-lg text-danger' aria-hidden='true'></i></g:link>
       
                 </div>
                  
            </div>
  </g:each>

<hr/>

</g:each>

