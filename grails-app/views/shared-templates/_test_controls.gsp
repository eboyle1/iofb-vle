<div class = "title_left-test">
	<h3>Questions for test: <f:display bean="test" property="name" /></h3>
</div>

<g:form url="[resource:test, action:'delete']" method="DELETE" class="form-horizontal">

   	<div class="btn-toolbar">

        <g:link controller="userTest" params="[testId:test.id]" action="beginTest" class="btn btn-nav pull-right">Display Test </g:link>

	    <g:link controller="question" action="create" params="[testId:test.id]" class="btn btn-primary pull-right"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Question</g:link>

	    <g:link controller="questionGroup" action="create" params="[testId:test.id,owner:test.id]" class="btn btn-primary pull-right"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Question Group</g:link>

	    <p:link controller="test" action="edit" resource="${test}" class="btn btn-primary pull-right" actionType="edit test"/>
<%--
        <button type="submit" class="btn btn-danger pull-right" onclick="return confirm('Are you sure? This action cannot be undone.');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete Test</button>
--%>
        <p:renderTestDeleteButton curTest="${test}"/>

	    <p:link controller="lecture" action="listTests" id="${test.lecture.id}" class="btn btn-default pull-right" actionType="cancel"/>

    </div>

</g:form>

<hr/>
