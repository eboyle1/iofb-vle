<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
        <ckeditor:resources/>

        <script type="text/javascript">

            function deleteOption(contentFileId) {
                var params = {contentFileId:contentFileId}
                handleOption(params,"${grailsApplication.config.vle.url}/lecture/deleteContentFile")
            };

            function handleOption(params, action) {
                var data = $.param(params)
                var response
                $.post(action,data, function(response) {
                    $('#optionsDiv').html( response )
                })  
                .fail(function() {
                    $( "#optionsDiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
                })   
            };
    
    </script>

    </head>
    <body>

        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${lecture.module.course.id}"><f:display bean="lecture" property="module.course.name" /></g:link> > <g:link controller="module" action="show" id="${lecture.module.id}"><f:display bean="lecture" property="module.name" /></g:link> > Edit Lecture Details</p>

        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.edit.label" args="[entityName]" /></div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                <g:hasErrors bean="${lecture}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${lecture}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </div>
                </g:hasErrors>          
                </ul>

                <g:form url="[resource:lecture, action:'update']" class="form-horizontal" method="PUT" >
                    <g:hiddenField name="version" value="${lecture?.version}" />

                    <fieldset class="form">
                        <g:render template="form_fields"/>
                    </fieldset>

                    <div class="show-buttons">
                    <div class="btn-toolbar">

                    <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
              
                    <button type="submit" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
  
                    <g:link controller="lecture" action="show" id="${lecture.id}" class="btn btn-nav">Lecture</g:link>

                    </div>
                    </div>
                </g:form>

            </div>
        </div>

        <g:if test="${lecture.lectureFile}">

        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.editLectureFile.label" args="[entityName]" /></div>
            <div class="panel-body">

            <g:form url="[resource:lecture, action:'updateLectureFileName']" class="form-horizontal" method="PUT" >
 
                <fieldset class="form">
                    <g:render template="form_fields_file"/>
                </fieldset>

                <div class="show-buttons">
                <div class="btn-toolbar">

                    <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
              
                    <button type="submit" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
  
                    <g:link controller="lecture" action="show" id="${lecture.id}" class="btn btn-nav">Lecture</g:link>

                </div>
                </div>

            </g:form>

            <g:form controller="lecture" action="deleteLectureFile2" id="${lecture.id}" method="DELETE">
            
            <div class="show-buttons">
            <div class="btn-toolbar">
                <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? This action cannot be undone.');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
            </div>
            </div>
            </g:form>

            </div>
        </div> 

        </g:if>

        <g:else>

            <div class="panel panel-primary">
                <div class="panel-heading"><g:message code="default.addLectureFile.label" args="[entityName]" /></div>
                <div class="panel-body">

                <ul class="errors" role="alert">
                <g:hasErrors bean="${fileCommand}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${fileCommand}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>

                <p>
                This is the main content of the lecture and should be an Adobe Presenter package in .zip file format.
                </p>

                <g:uploadForm name="upload" action="uploadLectureFile2" method="post" controller="Lecture">
 
                <g:hiddenField name="lectureId" value="${lecture.id}" />
 
                <input type="file" class="btn btn-default" name="uploadedFile" />
                <br/>
                Display text for file (optional): <input type="text" name="uploadedLectureFileName" />
                <br/>

                <div class="btn-toolbar">
                     <button type="submit" class="btn btn-primary pull-right" value="${message(code: 'lecture.lectureFile.upload.label', default: 'Upload')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Upload</button>
                     <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
                </div>
                </g:uploadForm>

                </div>
            </div>
        </g:else>

        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.editContentFiles.label" args="[entityName]" /></div>
            <div class="panel-body">

                <ul class="errors" role="alert">
                <g:hasErrors bean="${contentFileCommand}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${contentFileCommand}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                        </g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>
                
                <p>
                These are supporting files for the lecture and can be in .doc, .docx, .pdf or .mp3 file formats.
                </p>

                <g:uploadForm name="upload" action="uploadContentFile2" method="post" controller="Lecture">
 
                <g:hiddenField name="lectureId" value="${lecture.id}" />
 
                <input type="file" class="btn btn-default" name="uploadedContentFile" />
                <br/>
                Type of file (mandatory): <select name="uploadedContentFileType" required>
                <option value="">Select one:</option>
                <option value="0">Slides</option>
                <option value="1">Audio</option>
                <option value="2">Resources</option>
                <option value="3">Other</option>
                </select>
                <br/>
                <br/>
                Display text for file (optional): <input type="text" name="uploadedContentFileName" />
                <br/>

                <div class="show-buttons">
                <div class="btn-toolbar">

                    <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>

                    <button type="submit" class="btn btn-primary" value="${message(code: 'lecture.lectureFile.upload.label', default: 'Upload')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Upload</button>

                    <g:link controller="lecture" action="show" id="${lecture.id}" class="btn btn-nav">Lecture</g:link>
                </div>
                </div>

                </g:uploadForm>
                <br/>

                <g:render template="content_files" bean="${lecture}"/>

            </div>
        </div> 

    </body>
</html>
