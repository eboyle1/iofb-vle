<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-lecture" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-lecture" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${lectureList}" />

            <div class="pagination">
                <g:paginate total="${lectureCount ?: 0}" />
            </div>
        </div>

                 <div  role="main">
            <h2>Lectures</h2>
            
            <table class="table table-striped">
            <thead>
                    <tr>
                    
                        <g:sortableColumn property="name" title="Name" />
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                <g:each in="${lectureList}" status="i" var="lectureInstance">
                    <tr>
                    
                        <td>${fieldValue(bean: lectureInstance, field: "name")}</td>
                        <td><g:link action="show" id="${lectureInstance.id}">Show</g:link></td>
                    </tr>
                </g:each>
                </tbody>
            </table>
            <div class="pagination">
                <p:bootstrapPagination total="${lectureCount ?: 0}" domainBean="uk.ac.ed.bric.elearn.vle.Lecture" />
            </div>
        </div>
               
    </body>
</html>