<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:lecture, field:'name', 'has-warning')}">
                    <g:field type="text" name="name" bean="${lecture}" value="" class="form-control" />
             </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Module <span class="required-indicator">*</span></label>
            <div class="col-md-10 ${hasErrors(bean:lecture, field:'module', 'has-warning')}">
            <f:widget bean="${lecture}" property="module" class="form-check-input" />
             </div>
        </div>

        <div class="form-group">
            <label for="orderNo" class="col-md-2 control-label">Order no. <span class="required-indicator">*</span></label>
             <div class="col-md-2 ${hasErrors(bean:lecture, field:'orderNo', 'has-warning')}">
                    <g:field type="number" min="1" max="100" name="orderNo" bean="${lecture}" value="" class="form-control" />
             </div>
        </div>
<%--
		<div class="form-group">
	        <label for="code" class="col-md-2 control-label">Code <span class="required-indicator">*</span></label>
	         <div class="col-md-10 ${hasErrors(bean:lecture,field:'code', 'has-warning')}">
	         		<g:field type="text" name="code" bean="${lecture}" value="" class="form-control" />
	         </div>
	    </div>
--%>
        <div class="form-group">
            <label for="isVisible" class="col-md-2 control-label">Published </label>
             <div class="col-md-10 ${hasErrors(bean:lecture,field:'isVisible', 'has-warning')}">
                    <div class="form-checkbox"><f:widget bean="${lecture}" property="isVisible" class="form-check-input" /></div>
             </div>
        </div>
       
	    <div class="form-group">
            <label for="description" class="col-md-2 control-label">Description <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:lecture,field:'description', 'has-warning')}">
             		<g:field type="text" name="description" bean="${lecture}" value="" class="form-control"/>
             		
             </div>
        </div>

        <div class="form-group">
            <label for="authors" class="col-md-2 control-label">Author(s) </label>
             <div class="col-md-10 ${hasErrors(bean:lecture,field:'authors', 'has-warning')}">
                    <g:field type="text" name="authors" bean="${lecture}" value="" class="form-control"/>
                    
             </div>
        </div>

        <div class="form-group">
            <label for="editors" class="col-md-2 control-label">Editor(s) </label>
             <div class="col-md-10 ${hasErrors(bean:lecture,field:'editors', 'has-warning')}">
                    <g:field type="text" name="editors" bean="${lecture}" value="" class="form-control"/>
                    
             </div>
        </div>
       
        <div class="form-group">
        <label for="learningObjectives" class="col-md-2 control-label">Learning Objectives </label>
            <div class="col-md-10">
                <ckeditor:editor name="learningObjectives" toolbar="custom" height="300px" width="100%">

                </ckeditor:editor>
            </div>
        </div>

<%--
        <div class="form-group">
        <label for="lectureContent" class="col-md-2 control-label">Lecture Content </label>
            <div class="col-md-10">
                <ckeditor:editor name="lectureContent" toolbar="custom" height="300px" width="100%">

                </ckeditor:editor>
            </div>
        </div>
--%>

    </div>