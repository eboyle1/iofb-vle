    <div class="col-md-12">

        <div class="form-group">
            <label for="lectureFile.name" class="col-md-2 control-label">Name </label>
             <div class="col-md-10 ${hasErrors(bean:lecture, field:'lectureFile.name', 'has-warning')}">

                 <f:display bean="lecture" property="lectureFile.name"> 
                 <g:textField name="lectureFileName" value="${lecture.lectureFile.name}" class="form-control" />
                 </f:display>

             </div>
        </div>
       
    </div>