<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
        <ckeditor:resources/>
    </head>
    <body>

        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${lecture.module.course.id}"><f:display bean="lecture" property="module.course.name" /></g:link> > <g:link controller="module" action="show" id="${lecture.module.id}"><f:display bean="lecture" property="module.name" /></g:link> > Create Lecture</p>

        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.create.label" args="[entityName]" /></div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                <g:hasErrors bean="${lecture}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${lecture}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>

                <g:form url="[resource:lecture, action:'save']" class="form-horizontal" method="POST" >
                    <g:hiddenField name="version" value="${lecture?.version}" />

                    <fieldset class="form">
                        <g:render template="form_fields_create"/>
                    </fieldset>
                
                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary pull-right" value="${message(code: 'default.button.create.label', default: 'Create')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Create</button>   

                        <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>

                    </div>
                </g:form>
            </div>
        </div>

    </body>
</html>
