<div id="optionsDiv">
	
	<g:if test="${flash.ajaxerror}">
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			${flash.ajaxerror}
		</div>
	</g:if>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-8">
			<p><strong>Current supporting files:</strong></p>
		</div>
	</div>

	<div class="row">
		<div class="col-md-1"></div>
			<div class="col-md-9">
				<g:if test="${lecture.contentFiles}">
					<ul class="list-group">
						<g:each in="${lecture.contentFiles}" var="contentFile">
						 	<li class="list-group-item">${contentFile.fileName} (display text: ${contentFile.name}) 
						 	<div class="delete-buttons">
						 	<a href="#" class="pull-right text-danger" onClick="deleteOption('${contentFile?.id}')">
						 	<button class="btn btn-danger">
						 	<i class="fa fa-trash-o" aria-hidden="true"></i> Delete
						 	</button>
						 	</a>
						 	</div>
							</li>
						</g:each>
					</ul>
				</g:if>
				<g:else>
					<p><em>No supporting files added yet</em></p>
				</g:else>
			</div>
		</div>
	</div>

</div>