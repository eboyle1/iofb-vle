<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div role="main">

            <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${lecture.module.course.id}"><f:display bean="lecture" property="module.course.name" /></g:link> > <g:link controller="module" action="show" id="${lecture.module.id}"><f:display bean="lecture" property="module.name" /></g:link> > <g:link controller="lecture" action="show" id="${lecture.id}"><f:display bean="lecture" property="name" /></g:link> > Tests</p>

            <div class = "title_left">
            <h3>Tests for lecture:<br/> <f:display bean="lecture" property="name" /></h3>
            </div>
            <div class = "title_right">
            <g:link controller="test" class="create" action="create" params="[lecture:lecture.id]" class="btn btn-nav">New test</g:link>
            </div>
        </div>

        <div class="listTests">
            <g:each in="${lecture.tests.sort{it.orderNo}}" status="i" var="testInstance">
                <div class="well">
                   
                    <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-7">
                            <dl>
                              <dt class="text-info">Name</dt>
                              <dd>${testInstance.name}</dd>
                            </dl>
                            <dl>
                              <dt class="text-info">Description</dt>
                              <dd>${testInstance.description}</dd>
                            </dl>
<%--
                            <dl>
                              <dt class="text-info">Published?</dt>
                              <dd>${testInstance.isVisible}</dd>
                            </dl>
                            <dl>
                              <dt class="text-info">Date Created</dt>
                              <dd>${testInstance.dateCreated}</dd>
                            </dl>
--%>
                        </div>
                    </div>
                        
                    <div class="show-buttons">
                        <g:form url="[resource:testInstance, action:'delete']" method="DELETE">
                        <div class="btn-toolbar">       

                        <g:link controller="test" action="edit" id="${testInstance.id}" class="btn btn-primary">
                            <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Test Details</g:link>

                        <g:link controller="test" action="show" resource="${testInstance}" class="btn btn-primary"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Test Questions</g:link>
                            
                        <g:link controller="userTest" params="[testId:testInstance.id]" action="beginTest" class="btn btn-nav pull-right">Display Test </g:link>
<%--
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? This action cannot be undone.');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
--%>                     
                        <p:renderTestDeleteButton curTest="${testInstance}"/>

                        </div>
                        </g:form>
                    </div>
                    
                </div>
            </g:each> 
        </div>

    </body>
</html>