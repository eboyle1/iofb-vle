
<div id="questiondiv">

  	<g:if test="${flash.message}">
		<div class="alert alert-dismissable alert-info">
			<button type="button" class="close" data-dismiss="alert">×</button>
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			${flash.error}
		</div>
	</g:if>

	<div class="listQuestions">

    <p>An unlimited number of attempts are allowed for each test. You have to eventually achieve 100% in each test in order to complete the course</p>
    
	<p>Page ${questionGroup.orderNo} of ${test.questionGroups.max{it.orderNo}.orderNo}</p>

	<div class="well">

	<div class="row">
		<div class="col-md-12">
			<p>${raw(questionGroup.questionGroupText)}</p>
			<g:each in="${questionGroup.qGImages}" var="image">
				<%--<p><img src='${grailsApplication.config.external.webserver}/images/${image.fileName}' width='300' /></p>--%>
				<p><a href='${grailsApplication.config.external.webserver}/images/${image.fileName}' data-featherlight='image' data-featherlight-close-on-click='anywhere'><img src='${grailsApplication.config.external.webserver}/images/${image.fileName}' width='300' /></a></p>
            	[click on the image to expand]
			</g:each>
		</div>
   	</div>

   	</div>
   	</div>

	<g:each in="${questionGroup.questions.sort{it.orderNo}}" var="question">

		<div class="row">
			<g:if test="${question.choices.size() >= 4}">
				<div class="col-md-2">
			</g:if>
			<g:else>
				<div class="col-md-6">
			</g:else>
				<p>
				${raw(question.questionText)}
				</p>
			</div>

			<p:renderAnswer question='${question}' userTestSession='${userTestSession}'/>

		</div>	
	</g:each>

<hr>

	<div class="row">
		<div class="col-md-12">			   

			<g:if test="${questionGroup.orderNo != 1}">
					   <a href="#" class="btn btn-info btn-sm" onclick="previous('${questionGroup?.id}','${test?.id}','${userTestSession?.id}','previousQuestionGroup');"><i class='fa fa-arrow-circle-left' aria-hidden='true'></i> Previous Question Qroup </a>
			</g:if>

			<g:if test="${questionGroup.orderNo != test.questionGroups.max{it.orderNo}.orderNo}">

				<a href="#" class="btn btn-info btn-sm pull-right" onclick="next('${questionGroup?.id}','${test?.id}','${userTestSession?.id}','nextQuestionGroup');">Next Question Group <i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>

			</g:if>
			<g:else>

				<a href="#" class="btn btn-primary btn-sm pull-right" onclick="next('${questionGroup?.id}','${test?.id}','${userTestSession?.id}','submitTest');">Submit Test <i class='fa fa-arrow-circle-right' aria-hidden='true'></i></a>

			</g:else>

		</div>
	</div>
		
</div>
		