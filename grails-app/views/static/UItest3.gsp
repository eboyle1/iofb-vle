<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edinburgh Imaging Academy</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />
<style>
  .box {
    border: 1px solid lightgray;
    padding: 5px;
    border-radius: 10px;
}
</style>
</head>
<body>

   <div  role="main">

    <h3>Module 1: Background and Technique</h3>

    <div class="well">
        <p><b>Lecture 1 - Overview</b></p>
        <p>
        Case studies, current advice, screening process.
        </p>  
        <p>Author(s): Elaine Sandeman, Editor(s):</b> Andrew Farrall</p>
        <p>
On completion of this lecture, you should be able to:
</p>
<ul>
<li>Explain screening for IOFB</li>
<li>Define extended role for the radiographer</li>
<li>Give an overview of some case histories</li>
<li>Analyse whether X-ray radiography is necessary in screening process</li>
<li>Debate the problems around screening for IOFB</li>
</ul>
</p>
<div class="box">
<p>
    Lecture content
</p>
    <ul>
        <li><a href="https://www.learn.ed.ac.uk/bbcswebdav/pid-1467153-dt-content-rid-4687785_1/xid-4687785_1">Lecture slides (Adobe Presenter)</a></li>
        <li><a href="https://www.learn.ed.ac.uk/bbcswebdav/pid-1467153-dt-content-rid-4673820_1/xid-4673820_1">Lecture slides (PDF)</a></li>
        <li><a href="https://www.learn.ed.ac.uk/bbcswebdav/pid-1467153-dt-content-rid-3113709_1/xid-3113709_1">Audio file of lecture content</a></li>
    </ul>

        <p>Tests</p>
        <ul>
            <li><a href="http://www.elearn.malts.ed.ac.uk/appse/perception5/exam.php?group=IOFB&assessment=6761221936449605">Lecture 1 test</a></li>
        </ul>
</div>
<br/>
                    <g:form url="[resource:lecture, action:'delete']" method="DELETE">
                
                <fieldset class="buttons">
                    <g:link class="edit" url="/static/UItest6" class="btn btn-primary" resource="${lecture}">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</g:link>
                    

                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? This action cannot be undone.');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                    
                   
                </fieldset>
            
            </g:form>

    </div>

    <div class="well">
        <b>Lecture 2 - Technique</b> 
    </div>

    <div class="well">
        <b>Lecture 3 - IOFB reviewing – how to</b>
    </div>

    <div class="well">
        Module resources
    </div>


   </div>
            



 </body>
</html>