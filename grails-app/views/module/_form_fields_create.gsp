<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:module, field:'name', 'has-warning')}">
                    <g:field type="text" name="name" bean="${module}" value="" class="form-control" />
             </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Course <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:module, field:'course', 'has-warning')}">

             <f:widget bean="${module}" property="course" class="form-check-input" />

             </div>
        </div>

        <div class="form-group">
            <label for="orderNo" class="col-md-2 control-label">Order no. <span class="required-indicator">*</span></label>
             <div class="col-md-2 ${hasErrors(bean:module, field:'orderNo', 'has-warning')}">
                    <g:field type="number" min="1" max="100" name="orderNo" bean="${module}" value="" class="form-control" />
             </div>
        </div>
<%--
		<div class="form-group">
	        <label for="code" class="col-md-2 control-label">Code <span class="required-indicator">*</span></label>
	         <div class="col-md-10 ${hasErrors(bean:module,field:'code', 'has-warning')}">
	         		<g:field type="text" name="code" bean="${module}" value="" class="form-control" />
	         </div>
	    </div>
--%>
        <div class="form-group">
            <label for="isVisible" class="col-md-2 control-label">Published </label>
             <div class="col-md-10 ${hasErrors(bean:module,field:'isVisible', 'has-warning')}">
                    <div class="form-checkbox"><f:widget bean="${module}" property="isVisible" class="form-check-input" /></div>
             </div>
        </div>
   
        <div class="form-group">
        <label for="moduleResource" class="col-md-2 control-label">Module Resources </label>
            <div class="col-md-10">
                <ckeditor:editor name="moduleResource" toolbar="custom" height="300px" width="100%">

                </ckeditor:editor>
            </div>
        </div>


    </div>