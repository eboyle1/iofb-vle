<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'module.label', default: 'Module')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
        <ckeditor:resources/>
    </head>
    <body>

        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${module.course.id}"><f:display bean="module" property="course.name" /></g:link> > Edit Module Details</p>

        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.edit.label" args="[entityName]" /></div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                <g:hasErrors bean="${module}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${module}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>

            <g:form url="[resource:module, action:'update']" class="form-horizontal" method="PUT" >
                <g:hiddenField name="version" value="${module?.version}" />

                <fieldset class="form">
                    <g:render template="form_fields"/>
                </fieldset>
                <div class="btn-toolbar">
                    <button type="submit" class="btn btn-primary pull-right" value="${message(code: 'default.button.update.label', default: 'Update')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
  
                    <g:link controller="course" action="show" id="${module.course.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
              
                </div>
            </g:form>
        </div>
        </div>

    </body>
</html>
