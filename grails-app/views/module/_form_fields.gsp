<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:module, field:'name', 'has-warning')}">
                    <g:field type="text" name="name" bean="${module}" value="${module?.name}" class="form-control" />
             </div>
        </div>

        <div class="form-group">
             <label for="module" class="col-md-2 control-label">Course </label>
             <div class="col-md-10">
                <div class="form-text"><f:display bean="${module}" property="course.name" class="form-control" /></div>
             </div>
        </div>

        <div class="form-group">
            <label for="orderNo" class="col-md-2 control-label">Order no. <span class="required-indicator">*</span></label>
             <div class="col-md-2 ${hasErrors(bean:module, field:'orderNo', 'has-warning')}">
                    <g:field type="number" min="1" max="100" name="orderNo" bean="${module}" value="${module?.orderNo}" class="form-control" />
             </div>
        </div>
<%--
		<div class="form-group">
	        <label for="code" class="col-md-2 control-label">Code <span class="required-indicator">*</span></label>
	         <div class="col-md-10 ${hasErrors(bean:module,field:'code', 'has-warning')}">
	         		<g:field type="text" name="code" bean="${module}" value="${module?.code}" class="form-control" />
	         </div>
	    </div>
--%>
        <div class="form-group">
            <label for="isVisible" class="col-md-2 control-label">Published </label>
             <div class="col-md-10 ${hasErrors(bean:module,field:'isVisible', 'has-warning')}">
                    <div class="form-checkbox"><f:widget bean="${module}" property="isVisible" class="form-check-input" /></div>
             </div>
        </div>
       

       
        <div class="form-group">
        <label for="moduleResource" class="col-md-2 control-label">Module Resources </label>
            <div class="col-md-10">
                <ckeditor:editor name="moduleResource" toolbar="custom" height="300px" width="100%">
                    ${module?.moduleResource}
                </ckeditor:editor>
            </div>
        </div>




    </div>