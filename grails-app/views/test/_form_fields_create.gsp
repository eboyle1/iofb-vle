<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:test, field:'name', 'has-warning')}">
                    <g:field type="text" name="name" bean="${test}" value="" class="form-control" />
             </div>
        </div>

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Lecture <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:test, field:'lecture', 'has-warning')}">

             <f:widget bean="${test}" property="lecture" class="form-check-input" />

             </div>
        </div>

        <div class="form-group">
            <label for="orderNo" class="col-md-2 control-label">Order no. <span class="required-indicator">*</span></label>
             <div class="col-md-2 ${hasErrors(bean:test, field:'orderNo', 'has-warning')}">
                    <g:field type="number" min="1" max="100" name="orderNo" bean="${test}" value="" class="form-control" />
             </div>
        </div>

        <div class="form-group">
            <label for="isVisible" class="col-md-2 control-label">Published </label>
             <div class="col-md-10 ${hasErrors(bean:test,field:'isVisible', 'has-warning')}">
                    <div class="form-checkbox"><f:widget bean="${test}" property="isVisible" class="form-check-input" /></div>
             </div>
        </div>
   
        <div class="form-group">
            <label for="description" class="col-md-2 control-label">Description <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:test,field:'description', 'has-warning')}">
                    <g:field type="text" name="description" bean="${test}" value="" class="form-control"/>
                    
             </div>
        </div>

    </div>