<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'test.label', default: 'Test')}" />
        <title><g:message code="default.list.label" args="[entityName]" /></title>
    </head>
    <body>
        <a href="#list-test" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
        <div class="nav" role="navigation">
            <ul>
                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
            </ul>
        </div>
        <div id="list-test" class="content scaffold-list" role="main">
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
                <div class="message" role="status">${flash.message}</div>
            </g:if>
            <f:table collection="${testList}" />

            <div class="pagination">
                <g:paginate total="${testCount ?: 0}" />
            </div>
        </div>

<%--
       <div class = "title_left">
          <h3>Tests for lecture:<br/> <f:display bean="${test}" property="lecture.name" /></h3>
                    </div>
            <div class = "title_right">
            <button class="btn"><g:link controller="test" class="create" action="create">New test</g:link></button>
            </div>
        </div>

        <div id="list-test" class="content scaffold-list" role="main">
            
            <h1><g:message code="default.list.label" args="[entityName]" /></h1>

            <g:each in="${testList}" status="i" var="testInstance">
                <div class="well">
                   
                   <div class="row">
                        <div class="col-md-1">
                        </div>
                        <div class="col-md-7">
                            <dl>
                              <dt class="text-info">Name</dt>
                              <dd>${testInstance.name}</dd>
                            </dl>
                            <dl>
                              <dt class="text-info">Description</dt>
                              <dd>${testInstance.description}</dd>
                            </dl>
                            <dl>
                              <dt class="text-info">Published?</dt>
                              <dd>${testInstance.isVisible}</dd>
                            </dl>
                            <dl>
                              <dt class="text-info">Date Created</dt>
                              <dd>${testInstance.dateCreated}</dd>
                            </dl>
                        </div>
  
                        <div class="col-md-4">
                          <p:link controller="test" action="show" actionType="view" resource="${testInstance}" class="btn btn-primary pull-right"/>
                        </div>

                        </div>
                    </div> 
            </g:each>     
        </div>
--%>
    </body>
</html>