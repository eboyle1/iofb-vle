package uk.ac.ed.bric.elearn.vle

/**
 * Our own version of the Grails default paginate tag
 * @author: Andrew Robson
 */

import org.springframework.web.servlet.support.RequestContextUtils as RCU


class PaginateTagLib {
	
	static namespace = 'p'
	
    def paginateWithCommand = { attrs, body ->
        out << bootstrapPagination(attrs)
    }
    
    def bootstrapPagination = { attrs ->
        def writer = out
        if (attrs.total == null) {
            throwTagError("Tag [paginate] is missing required attribute [total]")
        }

        def messageSource = grailsAttributes.messageSource
        def locale = RCU.getLocale(request)

        def total = attrs.int('total') ?: 0
        def action = (attrs.action ? attrs.action : (params.action ? params.action : "index"))
        def offset = params.int('offset') ?: 0

        def max = params.int('max')
        def maxsteps = (attrs.int('maxsteps') ?: 10)

        if (!offset) offset = (attrs.int('offset') ?: 0)
        if (!max) max = (attrs.int('max') ?: 10)

        def linkParams = [:]
        if (attrs.params) linkParams.putAll(attrs.params)
        linkParams.offset = offset - max
        linkParams.max = max
        if (params.sort) linkParams.sort = params.sort
        if (params.order) linkParams.order = params.order

        def linkTagAttrs = [action:action]
        if (attrs.controller) {
            linkTagAttrs.controller = attrs.controller
        }
        if (attrs.id!=null) {
            linkTagAttrs.id = attrs.id
        }
        if (attrs.fragment!=null) {
            linkTagAttrs.fragment = attrs.fragment
        }
        linkTagAttrs.params = linkParams

        // determine paging variables
        def steps = maxsteps > 0
        int currentstep = (offset / max) + 1
        int firststep = 1
        int laststep = Math.round(Math.ceil(total / max))
		
		writer << "<div>"
        writer << "<ul class='pagination'>"
        // display previous link when not on firststep
        if (currentstep > firststep) {
            linkTagAttrs.class = 'prevLink'
            linkParams.offset = offset - max
			writer << '<li>'
            writer << link(linkTagAttrs.clone()) {
                (attrs.prev ? attrs.prev : messageSource.getMessage('paginate.prev', null, messageSource.getMessage('default.paginate.prev', null, 'Previous', locale), locale))
            }
			writer << '<li>'
        }

        // display steps when steps are enabled and laststep is not firststep
        if (steps && laststep > firststep) {
            linkTagAttrs.class = ''

            // determine begin and endstep paging variables
            int beginstep = currentstep - Math.round(maxsteps / 2) + (maxsteps % 2)
            int endstep = currentstep + Math.round(maxsteps / 2) - 1

            if (beginstep < firststep) {
                beginstep = firststep
                endstep = maxsteps
            }
            if (endstep > laststep) {
                beginstep = laststep - maxsteps + 1
                if (beginstep < firststep) {
                    beginstep = firststep
                }
                endstep = laststep
            }

            // display firststep link when beginstep is not firststep
            if (beginstep > firststep) {
                linkParams.offset = 0
				writer << '<li>'
                writer << link(linkTagAttrs.clone()) {firststep.toString()}
				writer << '</li>'
            }

            // display paginate steps
            (beginstep..endstep).each { i ->
                if (currentstep == i) {
                    //writer << "<li class=\"active\">${i}</li>"
                    linkParams.offset = (i - 1) * max
					writer << '<li class="active">'
                    writer << link(linkTagAttrs.clone()) {i.toString()}
					writer << "</li>"
                }
                else {
                    linkParams.offset = (i - 1) * max
					writer << "<li>"
                    writer << link(linkTagAttrs.clone()) {i.toString()}
					writer << "</li>"
                }
            }

            // display laststep link when endstep is not laststep
            if (endstep < laststep) {
                //writer << '<span class="step">..</span>'
                linkParams.offset = (laststep -1) * max
                writer << '<li>'
				writer << link(linkTagAttrs.clone()) { laststep.toString() }
				writer << '</li>'
            }	
        }

        // display next link when not on laststep
        if (currentstep < laststep) {
            linkTagAttrs.class = ''
            linkParams.offset = offset + max
			writer << "<li>"
            writer << link(linkTagAttrs.clone()) {
                (attrs.next ? attrs.next : messageSource.getMessage('paginate.next', null, messageSource.getMessage('default.paginate.next', null, 'Next', locale), locale))
            }
			writer << "</li>"
        }
	
	
		// do display n-m
		def displayRecStart
		def displayRecEnd
		
		if (offset == 0) {
			displayRecStart = 1	
		} else {
			displayRecStart = offset +1
		}
		
		if(total > displayRecStart + max -1) {
			displayRecEnd = displayRecStart + max - 1
		} else {
			displayRecEnd = total
		}
		
		if((displayRecStart) != displayRecEnd) {
			writer << " <li><a class='paginate-info'><strong>Showing Records ${displayRecStart} to ${displayRecEnd} of ${total}</strong></a></li>"
		} else {
			writer << " <li><a class='paginate-info'><strong>Showing Record ${displayRecStart}</strong></a></li> "
		}
		writer << "</ul>"
		writer << "</div>"
    }
	
}