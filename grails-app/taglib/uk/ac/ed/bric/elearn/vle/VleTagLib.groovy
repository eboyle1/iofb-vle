package uk.ac.ed.bric.elearn.vle

import groovy.xml.MarkupBuilder

class VleTagLib {

    static defaultEncodeAs = [taglib:'raw']
    static namespace = 'p'

    def renderQuestionGroup = { attrs ->
        def writer = out
        def questionGroup = attrs['questionGroup'] 
        def questionGroupText = raw(questionGroup.questionGroupText)
        def images = questionGroup.qGImages
        writer << "<p><span class='badge'>${questionGroup.orderNo}</span> ${questionGroupText}</p>"
        images.each {image ->
            writer << "<p><img src='${grailsApplication.config.external.webserver}/images/${image.fileName}' width='300' /></p>"
        }
    }

    def renderQuestion = { attrs ->
        def writer = out
        def question = attrs['question'] 
        def questionGroup = attrs['questionGroup'] 
        def questionText = raw(question.questionText)
        writer << "<p><span class='badge'>${questionGroup.orderNo}.${question.orderNo}</span> ${questionText}</p>"
        writer << "<ul class='list-group'>"
        def choices = question.choices.sort{it.choiceOrder}
        
        choices.each {choice ->
            if (choice.choiceStatus)
            {
                writer << "<li class='list-group-item'>${choice.choiceText} <i class='fa fa-check fa-lg' aria-hidden='true'></i></li>"
            }
            else
            {
                    writer << "<li class='list-group-item'>${choice.choiceText}</li>"
            }
        }
        writer << "<ul>"
    }

    def renderChoice = {attrs ->
        def writer = out
        def choice = attrs['choice']

        if (choice.choiceStatus)
        {
            writer << " ${choice.choiceOrder} ${choice.choiceText} <i class='fa fa-check fa-lg' aria-hidden='true'></i>"
        }
        else
        {
            writer << " ${choice.choiceOrder} ${choice.choiceText}"
        }
    }
    
    def renderAnswer = { attrs ->

        Answer currentAnswer
        def writer = out
        def question = attrs['question']
        def userTestSession = attrs['userTestSession']

        def choices = question.choices.sort{it.choiceOrder}

        userTestSession.answers.each {sessionAnswer ->
            if (sessionAnswer.owner.id == question.id)
            {
                currentAnswer = sessionAnswer
            }
        }

        if (choices.size() >= 4)
        {
            writer << "<div class='col-md-10'>"
            choices.each {choice ->

                writer << "&nbsp;<input type='radio' name='optionsRadios${question.id}' id='${choice.id}' value='${choice.id}'"
                if(currentAnswer) {
                    if(currentAnswer.isChosen(choice)) {
                        writer << "checked='checked'"
                    }
                }
                writer << ">&nbsp;${raw(choice.choiceText)}&nbsp;&nbsp;"
            }
            writer << "</div>"
        }
        else
        {
            choices.each {choice ->
                writer << "<div class='col-md-2'>"
                writer << "&nbsp;<input type='radio' name='optionsRadios${question.id}' id='${choice.id}' value='${choice.id}'"
                if(currentAnswer) {
                    if(currentAnswer.isChosen(choice)) {
                        writer << "checked='checked'"
                    }
                }
                writer << ">&nbsp;${raw(choice.choiceText)}"
                writer << "</div>"
            }
        }
    }

    //only display test delete button if no child user test session exists
    def renderTestDeleteButton = { attrs ->
        def exists = false
        def writer = out
        def curTest = attrs['curTest'] 
        def userTestSessions = UserTestSession.list()
        userTestSessions.each {session ->
            if (session.test.id == curTest.id)
            {
                exists = true
            }
        }

        if (exists)
        {
            writer << ""
        }
        else
        {
            writer << "<button type='submit' class='btn btn-danger pull-right' onclick='return confirm(\"Are you sure? This action cannot be undone.\");'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button>"
        }
    }

    //only display lecture delete link if no child user test session exists
    def renderLectureDeleteLink = { attrs ->
        def exists = false
        def writer = out
        def curLecture = attrs['curLecture']
        def userTestSessions = UserTestSession.list()
        userTestSessions.each {session ->
            if (session.test.lecture.id == curLecture.id)
            {
                exists = true
            }
        }
        if (exists)
        {
            writer << ""
        }
        else
        {
            writer << "<input type='hidden' name='_method' value='DELETE' id='_method' /><input type='submit' name='_action_delete' value='Delete lecture' class='delete-button' onclick='return confirm(&#39;Are you sure?&#39;);' />"
        }
    }

    //only display lecture delete button if no child user test session exists
    def renderLectureDeleteButton = { attrs ->
        def exists = false
        def writer = out
        def curLecture = attrs['curLecture']
        def userTestSessions = UserTestSession.list()
        userTestSessions.each {session ->
            if (session.test.lecture.id == curLecture.id)
            {
                exists = true
            }
        }
        if (exists)
        {
            writer << ""
        }
        else
        {
            writer << "<button type='submit' class='btn btn-danger' onclick='return confirm(\"Are you sure? This action cannot be undone.\");'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</button>"        }
    }

    //only display module delete link if no child user test session exists
    def renderModuleDeleteLink = { attrs ->
        def exists = false
        def writer = out
        def curModule = attrs['curModule']
        def userTestSessions = UserTestSession.list()
        userTestSessions.each {session ->
            if (session.test.lecture.module.id == curModule.id)
            {
                exists = true
            }
        }
        if (exists)
        {
            writer << ""
        }
        else
        {
            writer << "<input type='hidden' name='_method' value='DELETE' id='_method' /><input type='submit' name='_action_delete' value='Delete module' class='delete-button' onclick='return confirm(&#39;Are you sure?&#39;);' />"
        }
    }

    //only display course delete link if no child user test session exists
    def renderCourseDeleteLink = { attrs ->
        def exists = false
        def writer = out
        def curCourse = attrs['curCourse']
        def userTestSessions = UserTestSession.list()
        userTestSessions.each {session ->
            if (session.test.lecture.module.course.id == curCourse.id)
            {
                exists = true
            }
        }
        if (exists)
        {
            writer << ""
        }
        else
        {
            writer << "<input type='hidden' name='_method' value='DELETE' id='_method' /><input type='submit' name='_action_delete' value='Delete course' class='delete-button' onclick='return confirm(&#39;Are you sure?&#39;);' />"
        }
    }
}