package uk.ac.ed.bric.elearn.vle

import org.grails.plugins.web.taglib.ApplicationTagLib

class OverrideDefaultTagLib {
	
    static namespace = 'p'

    // todo - this is beginning to seem like a bad idea ... 
    // just decorates tag body with an icon
    def link = { attrs, body ->
    	
        if(attrs) {
    		if(attrs.actionType == "view") {
    			body = { "<i class='fa fa-eye' aria-hidden='true'></i> View" }
    		} else if (attrs.actionType == "delete") {
    			body = { "<i class='fa fa-trash-o' aria-hidden='true'></i> Delete" }
    		} else if (attrs.actionType == "edit") {
    			body = { "<i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit" }
            }  else if (attrs.actionType == "edit test") {
                body = { "<i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit Test Details" }
    		} else if (attrs.actionType == "cancel") {
    			body = { "<i class='fa fa-times-circle' aria-hidden='true'></i> Cancel" }	
    		} else if (attrs.actionType == "save") {
                body = { "<i class='fa fa-floppy-o' aria-hidden='true'></i> Save"}
            }
    	}
        
        def applicationTagLib = grailsApplication.mainContext.getBean('org.grails.plugins.web.taglib.ApplicationTagLib')
            applicationTagLib.link.call(attrs, body)
        }
    }
