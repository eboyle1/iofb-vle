package elearn.vle

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        //"/"(view:"/index")
        "/" (controller:'course', action:'index') 
        "500"(view:'/error')
        "404"(view:'/notFound')
        "/static/test"(view:"/static/test")
        "/static/UItest1"(view:"/static/UItest1")
        "/static/UItest2"(view:"/static/UItest2")
        "/static/UItest3"(view:"/static/UItest3")
        "/static/UItest4"(view:"/static/UItest4")
        "/static/UItest5"(view:"/static/UItest5")
        "/static/UItest6"(view:"/static/UItest6")
        "/static/UItest7"(view:"/static/UItest7")
        "/rest/courses" (controller:'course', action:'list')
        "/rest/course/enrol" (controller:'course', action:'enrol')
        "/rest/course/withdraw" (controller:'course', action:'withdraw')
        "/rest/course/findUserCourses" (controller:'course', action:'findUserCourses')
    }
}
