package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import uk.ac.ed.bric.elearn.vle.ImageCommand

@Transactional(readOnly = true)
class QuestionGroupController {

    static allowedMethods = [save: "POST", update: "PUT", delete: ["GET","DELETE"]]

    def fileService

    @Secured('ROLE_ADMIN')
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond QuestionGroup.list(params), model:[questionGroupCount: QuestionGroup.count()]
    }

    @Secured('ROLE_ADMIN')
    def show(QuestionGroup questionGroup) {
        respond questionGroup
    }

    @Secured('ROLE_ADMIN')
    def create() {
        //respond new QuestionGroup(params)
        def test = Test.get(params.testId)
        if(test == null) {
            flash.error = message(code: 'cannot.find.test')
            return
        }
      
        respond new QuestionGroup(params), model:[ test:test ]
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def save(QuestionGroup questionGroup) {
        if (questionGroup == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def test = Test.get(params.ownerId)

        if (questionGroup.hasErrors()) {
            transactionStatus.setRollbackOnly()
            //respond questionGroup.errors, view:'create'
            respond questionGroup.errors, view:'create', model:[ questionGroup:questionGroup, test:test, lecture:test.lecture, module: test.lecture.module, course:test.lecture.module.course ]
            return
        }

        questionGroup.save flush:true

        flash.message = message(code: 'default.created.message', args: [message(code: 'questionGroup.label', default: 'Question Group'), questionGroup.id])
        //render view:"show", model:[ test:test]
        redirect controller: "test", action:"show", id: test.id
    }

    @Secured('ROLE_ADMIN')
    def edit(QuestionGroup questionGroup) {
        respond questionGroup
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def update(QuestionGroup questionGroup) {

        Test test = questionGroup.owner

        if (questionGroup == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (questionGroup.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond questionGroup.errors, view:'edit'
            return
        }

        questionGroup.save flush:true

        if (questionGroup.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond questionGroup.errors, view:'edit'
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'questionGroup.label', default: 'Question Group'), questionGroup.id])
        forward controller: "test", action: "show", id: test.id

//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.updated.message', args: [message(code: 'questionGroup.label', default: 'QuestionGroup'), questionGroup.id])
//                redirect questionGroup
//            }
//            '*'{ respond questionGroup, [status: OK] }
//        }
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def delete(QuestionGroup questionGroup) {

        Test test = questionGroup.owner

        if (questionGroup == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        test.removeFromQuestionGroups(questionGroup)
        questionGroup.delete flush:true

        flash.message = message(code:"questionGroup.deleted")
        forward controller:"test", action:"show", id: test.id

//       request.withFormat {
//           form multipartForm {
//               flash.message = message(code: 'default.deleted.message', args: [message(code: 'questionGroup.label', default: 'QuestionGroup'), questionGroup.id])
//               redirect action:"index", method:"GET"
//           }
//           '*'{ render status: NO_CONTENT }
//        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'questionGroup.label', default: 'QuestionGroup'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def uploadQuestionGroupImage(ImageCommand command) {

        QuestionGroup questionGroup = QuestionGroup.get(params.questionGroupId)
        Test test = questionGroup.owner

        if(command.hasErrors()) {
            render view: 'edit', model:[ questionGroup:questionGroup, imageCommand:command ]
            return
        }
 
        fileService.saveFile3(command, questionGroup)
        render view: 'edit', model:[ questionGroup:questionGroup ]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def deleteQuestionGroupImage() {

        QGImage qGImage = QGImage.get(params.int('qGImageId'))
        
        if (qGImage == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        QuestionGroup questionGroup = qGImage.questionGroup
        fileService.deleteQuestionGroupImage(questionGroup, qGImage)

        if(questionGroupImageSaveFail(qGImage)) {
            render template:"images", model:[ questionGroup:questionGroup ]
            return     
        }

        render template:"images", model:[ questionGroup:questionGroup ]

    }

    @Transactional
    def questionGroupImageSaveFail(domainObject) {
        def result = true
        if(domainObject.hasErrors()) {
            transactionStatus.setRollbackOnly()
            flash.ajaxerror = errorsToString(domainObject) 
            result = true
        } else {
            result = false
        }
        return result
    }

    def errorsToString(domainObject) {
        def message = new StringBuilder()
        for(fieldErrors in domainObject.errors) {
            for(error in fieldErrors.allErrors) {
                message.append("\n\t").append(messageSource.getMessage(error, Locale.getDefault()))
            }
        }
        message.toString()       
    }

}
