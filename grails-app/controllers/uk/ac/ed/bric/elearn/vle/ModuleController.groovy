package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import uk.ac.ed.bric.elearn.User

@Transactional(readOnly = true)
class ModuleController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured('ROLE_ADMIN') 
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Module.list(params), model:[moduleCount: Module.count()]
    }

    @Secured('ROLE_ADMIN') 
    def show(Module module) {
        respond module
    }

    def springSecurityService

    @Secured(['ROLE_USER','ROLE_ADMIN']) 
    def studentView(Module module) {
        //get current user ID
        User user = springSecurityService.currentUser
        //use hql to select the user's test history        
        String hql = " select a.userId, a.test.id, max(a.mark) from UserTestSession a where a.userId = " + user.id + " group by a.test.id "
        def result = UserTestSession.executeQuery(hql)
        respond module, model:[ userTestSessions : result ]
    }

    @Secured('ROLE_ADMIN') 
    def create() {
        respond new Module(params)
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def save(Module module) {
        if (module == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (module.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond module.errors, view:'create'
            return
        }

        module.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'module.label', default: 'Module'), module.id])
                redirect module
            }
            '*' { respond module, [status: CREATED] }
        }
    }

    @Secured('ROLE_ADMIN') 
    def edit(Module module) {
        respond module
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def update(Module module) {
        if (module == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (module.hasErrors()) {
            transactionStatus.setRollbackOnly()
            //respond module.errors, view:'edit'
            render view:'edit', model: [module: module, course: module.course]
            return
        }

        module.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'module.label', default: 'Module'), module.id])
                redirect module
            }
            '*'{ respond module, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def delete(Module module) {

        if (module == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        module.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'module.label', default: 'Module'), module.id])
                //redirect action:"index", method:"GET
                redirect controller: "course", action:"show", id: module.course.id, method:"GET"
            }
             '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'module.label', default: 'Module'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
