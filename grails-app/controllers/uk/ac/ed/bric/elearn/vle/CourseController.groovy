package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

import uk.ac.ed.bric.elearn.User

@Transactional(readOnly = true)
class CourseController {

    def springSecurityService
    def courseService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured(['ROLE_USER','ROLE_ADMIN']) 
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Course.list(params), model:[courseCount: Course.count()]
    }

    @Secured(['ROLE_USER','ROLE_ADMIN']) 
    def show(Course course) {

        //find if logged-in user is enrolled on selected course
        User user = springSecurityService.currentUser

        sec.ifAnyGranted(roles:'ROLE_USER')
        {
            CourseEnrolment enrolment = courseService.findCourseEnrolment(course, user)
            if(!enrolment && user) {
                //show no enrolment page
                render (view: "notEnrolled", model:[course:course])
                return
            }
            else {
                //show course content 
                respond course
            }
        }
        sec.ifAnyGranted(roles:'ROLE_ADMIN')
        {
                //show course content 
                respond course
        }
    }

    @Secured('ROLE_ADMIN') 
    def create() {
        respond new Course(params)
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def save(Course course) {
        if (course == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (course.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond course.errors, view:'create'
            return
        }

        course.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'course.label', default: 'Course'), course.id])
                redirect course
            }
            '*' { respond course, [status: CREATED] }
        }
    }

    @Secured('ROLE_ADMIN') 
    def edit(Course course) {
        respond course
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def update(Course course) {
        if (course == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (course.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond course.errors, view:'edit'
            return
        }

        course.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'course.label', default: 'Course'), course.id])
                redirect course
            }
            '*'{ respond course, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def delete(Course course) {

        if (course == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        course.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'course.label', default: 'Course'), course.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    /* Enrolment methods are used by REST client (User Admin app) */
    @Secured('ROLE_ADMIN') 
    def list() {
        respond Course.list(params)
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def enrol() {
        Course course = Course.get(params.courseId.toLong())
        User user = User.get(params.userId.toLong())
        
        CourseEnrolment enrolment = new CourseEnrolment(course:course, user:user)
        enrolment.validate()
        if(enrolment.hasErrors()) {
            log.error "Validating enrolment had errors: ${enrolment.errors}"
            render status: BAD_REQUEST
            return
        }

        enrolment.save flush:true
        if(enrolment.hasErrors()) {
            log.error "Saving enrolment had errors: ${enrolment.errors}"
            render status: BAD_REQUEST
            return
        }

        render status: OK
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def withdraw() {
        Course course = Course.get(params.courseId.toLong())
        User user = User.get(params.userId.toLong())

        log.info "withdraw $user from $course"

        CourseEnrolment enrolment = courseService.findCourseEnrolment(course, user)
        if(!enrolment) {
            render status: NOT_FOUND
            return
        }
        enrolment.delete flush:true
        
        render status: OK
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def findUserCourses() {
        User user = User.get(params.userId.toLong())
        def enrolments = courseService.findEnrolmentsByUser(user)
        render(contentType: "text/xml") {
            userEnrolments {
                for (e in enrolments) {
                    enrolment(user:e.user.id, courseId:e.course.id, courseName:e.course.name )
                }
            }
        }
    }


    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'course.label', default: 'Course'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def showWelcomePage(Course course)
    {
        respond course
    }

    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def showStaffPage(Course course)
    {
        respond course
    }
}
