package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class QuestionController {

    def messageSource
    def testService

    static allowedMethods = [save: "POST", update: "PUT", delete: ["GET","DELETE"]]

    @Secured('ROLE_ADMIN') 
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Question.list(params), model:[questionCount: Question.count()]
    }

    @Secured('ROLE_ADMIN') 
    def show(Question question) {
        respond question
    }

    @Secured('ROLE_ADMIN') 
    def create() {
        def test = Test.get(params.testId)
        if(test == null) {
            flash.error = message(code: 'cannot.find.test')
            return
        }
      
        respond new Question(params), model:[ test:test ]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def save(Question question) {
        if (question == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        def questionGroup = QuestionGroup.get(params.owner.id)
        def test = Test.get(params.ownerOwnerId)

        if(questionGroup == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return    
        }

        question.owner = questionGroup

        if (question.hasErrors()) {
            transactionStatus.setRollbackOnly()
            //respond question.errors, view:'create', model:[ test:test ]
            respond question.errors, view:'create', model:[ questionGroup:questionGroup, test:test, lecture:test.lecture, module: test.lecture.module, course:test.lecture.module.course ]
            return
        }

        question.save flush:true

        if (question.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond question.errors, view:'create', model:[ questionGroup:questionGroup, test:test ]
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'question.label', default: 'Question'), question.id])
        render view:"show", model:[ question:question, questionGroup:questionGroup, test:test, choiceList:[]]
        //render view:"show", model:[ question:question, test:test, choiceList:[]]
        //render view:"show", model:[ question:question ]
    }

    @Secured('ROLE_ADMIN') 
    def edit(Question question) {
        //respond question
        respond question, model:[ choiceList:question.choices.sort{it.choiceOrder} ]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def update(Question question) {
        if (question == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (question.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond question.errors, view:'edit'
            return
        }

        question.save flush:true

        if (question.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond question.errors, view:'edit', model:[ test:test ]
            return
        }


        flash.message = message(code: 'default.updated.message', args: [message(code: 'question.label', default: 'Question'), question.id])
        forward controller: "test", action: "show", id: question.owner.owner.id

   //     request.withFormat {
   //         form multipartForm {
   //             flash.message = message(code: 'default.updated.message', args: [message(code: 'question.label', default: 'Question'), question.id])
   //             redirect question
   //         }
   //         '*'{ respond question, [status: OK] }
   //     }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def delete(Question question) {

        QuestionGroup questionGroup = question.owner

        if (question == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

//        if(testService.isTargetQuestion(question)) {
//            flash.error = message(code:"cannot.delete.target.question")
//            forward controller:"test", action:"show", id: test.id
//            return
//        }

        questionGroup.removeFromQuestions(question)
        question.delete flush:true

        flash.message = message(code:"question.deleted")
        forward controller:"test", action:"show", id: questionGroup.owner.id

//        request.withFormat {
//            form multipartForm {
//                flash.message = message(code: 'default.deleted.message', args: [message(code: 'question.label', default: 'Question'), question.id])
//                redirect action:"index", method:"GET"
//            }
//            '*'{ render status: NO_CONTENT }
//        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'question.label', default: 'Question'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def addChoice() {
        
        def question = Question.get(params.questionId)
        if(question == null) {
           render status:500
           return 
        }
        
        def choiceOrder = params.int('choiceOrder')
        def choiceText = params.choiceText
        def choiceStatus = params.boolean('choiceStatus')
        def choice = new Choice(owner:question, choiceOrder:choiceOrder, choiceText:choiceText, choiceStatus:choiceStatus )

        def choiceList = testService.listChoicesForQuestion(question)

        if(choice.hasErrors()) {
            transactionStatus.setRollbackOnly()
            flash.ajaxerror = handlChoiceErrors(choice)
            render template:"choices", model:[choiceList:choiceList, question:question]
            return  
        } 

        choice.save flush:true
        if(choice.hasErrors()) {
            transactionStatus.setRollbackOnly()
            flash.ajaxerror = handlChoiceErrors(choice)
            render template:"choices", model:[choiceList:choiceList, question:question]
            return
        }

        choiceList = testService.listChoicesForQuestion(question)
        render template:"choices", model:[choiceList:choiceList, question:question]
    }

    @Secured('ROLE_ADMIN')
    @Transactional
    def deleteChoice() {
        def choice = Choice.get(params.choiceId)
        
        if(choice == null) {
            render status:500
            return
        }
        
        def question = Question.get(choice.owner.id)
        if(question == null) {
            render status:500
            return
        }
        def choiceList = testService.listChoicesForQuestion(question)
        
        choice.delete flush:true
        if(choice.hasErrors()) {
            transactionStatus.setRollbackOnly()
            flash.ajaxerror = handlChoiceErrors(choice)
            render template:"choices", model:[choiceList:choiceList, question:question]
            return
        }

        choiceList = testService.listChoicesForQuestion(question)
        render template:"choices", model:[choiceList:choiceList, question:question]
    }

    def handlChoiceErrors(choice) {
        def message = new StringBuilder()
        for(fieldErrors in choice.errors) {
            for(error in fieldErrors.allErrors) {
                message.append("\n\t").append(messageSource.getMessage(error, Locale.getDefault()))
            }
        }
        message.toString()   
    }

}
