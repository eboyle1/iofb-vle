package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import uk.ac.ed.bric.elearn.vle.FileCommand

@Transactional(readOnly = true)
class LectureController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE", uploadLectureFile: "POST", deleteLectureFile: "DELETE"]

    def fileService

    @Secured('ROLE_ADMIN') 
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Lecture.list(params), model:[lectureCount: Lecture.count()]
    }

    @Secured('ROLE_ADMIN') 
    def show(Lecture lecture) {
        respond lecture
    }

    @Secured('ROLE_ADMIN') 
    def create() {
        respond new Lecture(params)
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def save(Lecture lecture) {
        if (lecture == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (lecture.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond lecture.errors, view:'create', model: [module: lecture.module, course: lecture.module.course]
            return
        }

        lecture.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'lecture.label', default: 'Lecture'), lecture.id])
                redirect lecture
            }
            '*' { respond lecture, [status: CREATED] }
        }
    }

    @Secured('ROLE_ADMIN') 
    def edit(Lecture lecture) {
        respond lecture
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def update(Lecture lecture) {

        if (lecture == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (lecture.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render view:'edit', model: [lecture: lecture, module: lecture.module]
            return
        }

        lecture.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'lecture.label', default: 'Lecture'), lecture.id])
                redirect controller: "lecture", action:"edit", id: lecture.id, method:"GET"
            }
            '*'{ respond lecture, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def delete(Lecture lecture) {

        if (lecture == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        lecture.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'lecture.label', default: 'Lecture'), lecture.id])
                redirect controller: "module", action:"show", id: lecture.module.id, method:"GET"
            }
            '*'{ render status: NO_CONTENT }

        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'lecture.label', default: 'Lecture'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def addLectureFile(Lecture lecture) {
        FileCommand cmd = new FileCommand()
        render view:"addLectureFile", model:[lecture:lecture, fileCommand:cmd]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def addContentFile(Lecture lecture) {
        ContentFileCommand cmd = new ContentFileCommand()
        render view:"addContentFile", model:[lecture:lecture, contentFileCommand:cmd]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def uploadLectureFile(FileCommand command) {
        Lecture lecture = Lecture.get(params.lectureId)
        String name = params.uploadedLectureFileName
        if(command.hasErrors()) {
            render view: 'addLectureFile', model:[lecture:lecture, fileCommand:command]
            return
        }
 
        fileService.saveFile(command, lecture, name)
        render view:"show", model:[lecture:lecture]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def uploadContentFile(ContentFileCommand command) {
        Lecture lecture = Lecture.get(params.lectureId)
        String name = params.uploadedContentFileName
        int type = params.int('uploadedContentFileType')
        if(command.hasErrors()) {
            render view: 'addContentFile', model:[lecture:lecture, contentFileCommand:command]
            return
        }
 
        fileService.saveFile2(command, lecture, name, type)
        render view:"show", model:[lecture:lecture]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def uploadLectureFile2(FileCommand command) {
        Lecture lecture = Lecture.get(params.lectureId)
        String name = params.uploadedLectureFileName
        if(command.hasErrors()) {
            render view: 'edit', model:[lecture:lecture, fileCommand:command]
            return
        }
 
        fileService.saveFile(command, lecture, name)
        render view:"edit", model:[lecture:lecture]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def uploadContentFile2(ContentFileCommand command) {
        Lecture lecture = Lecture.get(params.lectureId)
        String name = params.uploadedContentFileName
        int type = params.int('uploadedContentFileType')
        if(command.hasErrors()) {
            render view: 'edit', model:[lecture:lecture, contentFileCommand:command]
            return
        }
 
        fileService.saveFile2(command, lecture, name, type)
        render view:"edit", model:[lecture:lecture]
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def updateLectureFileName(Lecture lecture) {

        if (lecture == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (lecture.hasErrors()) {
            transactionStatus.setRollbackOnly()
            render view:'edit', model: [lecture: lecture, lectureFile: lecture.lectureFile]
            return
        }

        fileService.updateFileName(lecture, params.lectureFileName)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'lecture.label', default: 'Lecture File Name')])
                redirect controller: "lecture", action:"edit", id: lecture.id, method:"GET"
            }
            '*'{ respond lecture, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def deleteLectureFile(Lecture lecture) {

        if (lecture == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        fileService.deleteFile(lecture)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'lecture.label', default: 'Lecture File')])
                redirect controller: "lecture", action:"addLectureFile", id: lecture.id, method:"GET"
            }
            '*'{ render status: NO_CONTENT }

        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def deleteLectureFile2(Lecture lecture) {

        if (lecture == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        fileService.deleteFile(lecture)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'lecture.label', default: 'Lecture File')])
                redirect controller: "lecture", action:"edit", id: lecture.id, method:"GET"
            }
            '*'{ render status: NO_CONTENT }

        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def deleteContentFile() {

        ContentFile contentFile = ContentFile.get(params.int('contentFileId'))
        
        if (contentFile == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        Lecture lecture = contentFile.owner
        fileService.deleteContentFile(lecture, contentFile)

        if(contentFileSaveFail(contentFile)) {
            render template:"content_files", model:[lecture:lecture]
            return     
        }

        render template:"content_files", model:[lecture:lecture]

    }

    @Transactional
    def contentFileSaveFail(domainObject) {
        def result = true
        if(domainObject.hasErrors()) {
            transactionStatus.setRollbackOnly()
            flash.ajaxerror = errorsToString(domainObject) 
            result = true
        } else {
            result = false
        }
        return result
    }

    def errorsToString(domainObject) {
        def message = new StringBuilder()
        for(fieldErrors in domainObject.errors) {
            for(error in fieldErrors.allErrors) {
                message.append("\n\t").append(messageSource.getMessage(error, Locale.getDefault()))
            }
        }
        message.toString()       
    }

    @Secured('ROLE_ADMIN') 
    def listTests(Lecture lecture) {
        respond lecture
    }
}
