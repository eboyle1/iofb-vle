package uk.ac.ed.bric.elearn.vle

import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

class ImageCommand implements Validateable {

    MultipartFile uploadedQuestionGroupImage

    static def questionGroupImageValidator = { val, obj ->
        if ( val == null ) {
            return false
        }
        if ( val.empty ) {
            return false
        }

        ['jpg', 'png'].any { extension -> 
            val.originalFilename?.toLowerCase()?.endsWith(extension)
        }
    }

    static constraints = {
        uploadedQuestionGroupImage validator: questionGroupImageValidator
    }
}