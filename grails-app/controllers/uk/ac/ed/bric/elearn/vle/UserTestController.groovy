package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import groovy.text.SimpleTemplateEngine

import uk.ac.ed.bric.elearn.User

class UserTestController {

    def springSecurityService
    def userTestSessionService
    def userTestNavigationService
    def userAdminService
    def mailService

    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def beginTest() {

    	//get current user object
        User user = springSecurityService.currentUser

        //get selected test
        def testId = params.long('testId')
        Test test = Test.get(testId)

        //create a session object for the test
        UserTestSession userTestSession = userTestSessionService.createUserTestSession(user, test)
		//userTestSession.save flush:true

        //show first question group page of test
        QuestionGroup questionGroup = userTestNavigationService.getCurrentQuestionGroup(userTestSession)
        render view:"questionGroupPage", model:[ userTestSession : userTestSession, questionGroup : questionGroup, test : test ]
    }

    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def displayQuestionGroup() {

        Test test = Test.get(params.long('testId'))
        def userTestSession = UserTestSession.get(params.long('userTestSessionId')) 
        QuestionGroup questionGroup = QuestionGroup.get(params.long('questionGroupId')) 
       	render template: 'questionGroup', model:[ userTestSession : userTestSession, questionGroup : questionGroup, test :test ]
	}

    @Transactional
    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def previousQuestionGroup() {

        Test test = Test.get(params.long('testId'))
        def userTestSession = UserTestSession.get(params.long('userTestSessionId')) 
        QuestionGroup questionGroup = QuestionGroup.get(params.long('questionGroupId'))

        QuestionGroup previousQuestionGroup = userTestNavigationService.getPrecedingQuestionGroup(userTestSession, questionGroup, test)
      
        render template: 'questionGroup', model:[ userTestSession : userTestSession, questionGroup : previousQuestionGroup, test : test ]
  
    }

    @Transactional
    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def nextQuestionGroup() {

        Test test = Test.get(params.long('testId'))
        def userTestSession = UserTestSession.get(params.long('userTestSessionId')) 
        QuestionGroup questionGroup = QuestionGroup.get(params.long('questionGroupId'))

        recordAnswers(params, questionGroup, userTestSession)

	    QuestionGroup nextQuestionGroup = userTestNavigationService.getNextQuestionGroup(userTestSession, questionGroup, test)

        render template: 'questionGroup', model:[ userTestSession : userTestSession, questionGroup : nextQuestionGroup, test : test ]
    }

    @Transactional
    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def submitTest() {

        Test test = Test.get(params.long('testId'))
        def userTestSession = UserTestSession.get(params.long('userTestSessionId')) 
        QuestionGroup questionGroup = QuestionGroup.get(params.long('questionGroupId'))

        recordAnswers(params, questionGroup, userTestSession)
        calculateMark (userTestSession, test)
        //check to see if all tests have been marked at 100%
        Boolean testsComplete = checkCompletion (userTestSession)

        render template: 'testResults', model:[ userTestSession:userTestSession, test:test, testsComplete:testsComplete ]
    }

    @Transactional
    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def recordAnswers(params, questionGroup, userTestSession) {
        
		for (choiceId in params.list('selectedChoices[]'))
		{

        	Choice selectedChoice = Choice.get(choiceId)

			Answer answer = new Answer (owner:selectedChoice.owner, choice:selectedChoice)

        	//if there is an existing answer to this question, remove it (needed for previous navigation)
        	Answer existingAnswer = userTestSession.answers.find { it.owner.id == answer.owner.id }
        	if(existingAnswer) {
                userTestSession.removeFromAnswers(existingAnswer)
                existingAnswer.delete flush:true
            	userTestSession.save flush:true
        	}

            //check if answer is correct
            answer.owner.choices.each {choice ->
                if (choice.choiceStatus)
                {
                    if (choice.id == selectedChoice.id)
                    {
                        answer.correct = true
                    }
                }
            }

    		answer.save flush:true
  			userTestSession.addToAnswers(answer)
        	userTestSession.save flush:true
		}
    }

    @Transactional
    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def calculateMark(userTestSession, test) {

        //count number of correct answers in this test session
        int correctTotal = userTestSession.answers.correct.count(true)
        //count total number of questions in this test
        int testQuestionsTotal = 0
        test.questionGroups.each {questionGroup ->
            int currQuestionCount = 0
            currQuestionCount = questionGroup.questions.size()
            testQuestionsTotal = testQuestionsTotal + currQuestionCount
        }

        float calculatedMark

        if (correctTotal > 0)
        {
            calculatedMark = ( correctTotal / testQuestionsTotal ) * 100
        }
        else
        {
            calculatedMark = 0
        }

        userTestSession.totalQuestions = testQuestionsTotal
        userTestSession.mark = calculatedMark.round(1)
                
        userTestSession.save flush:true

    }

    @Transactional
    @Secured(['ROLE_USER','ROLE_ADMIN'])
    def checkCompletion(userTestSession) {

        //get current user object
        User user = springSecurityService.currentUser
   
        //use hql to retrieve student's test results
        //using max() to aggregate some of the columns is a bit of a fudge, as any_value() is not available in hql
        String hql = " select max(a.userId), a.test.id, max(a.mark) from UserTestSession a where a.userId = " + user.id + " group by a.test.id "
        def result = UserTestSession.executeQuery(hql)
        int testCount = 0
        int testsNo = Test.count()  //this will need to be modified to take account of multiple courses
        Boolean complete = true
        for (testMark in result)
        {
            //count tests to see that all have been attempted
            testCount++;
            //check that all marks are 100%
            if (testMark[2] != 100)
            {
                complete = false
                break
            }
        }
        if ((testCount == testsNo) && (complete))
        {
            //if yes, update UserAdmin table
            UserAdmin userAdmin = userAdminService.createUserAdmin(user, complete)
            //if yes, email IOFB admin with notification
            def emailTo = grailsApplication.config.vleTests.email.to
            def emailFrom = grailsApplication.config.vleTests.email.from
            def emailSubject = grailsApplication.config.vleTests.email.subject
            def emailBody = grailsApplication.config.vleTests.email.body
            if(emailBody.contains('$')) 
            {
                emailBody = evaluate(emailBody, [ firstname: user.firstName, lastname: user.lastName ])
            }
            try 
            {
                sendMail(emailTo, emailFrom, emailSubject, emailBody.toString())
            } 
            catch (Exception ex) 
            {
                log.error(ex.getMessage())  
            }
            //if yes, notify user on test results page
            return true
        }
        else
        {
            return false
        }
    }

    protected String evaluate(s, binding) {
        new SimpleTemplateEngine().createTemplate(s).make(binding)
    }

    def sendMail(emailTo, emailFrom, emailSubject, emailBody) throws Exception {
        mailService.sendMail {
            to emailTo
            from emailFrom
            subject emailSubject
            html emailBody
        }   
    }

}