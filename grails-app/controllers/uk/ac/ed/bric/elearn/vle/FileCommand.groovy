package uk.ac.ed.bric.elearn.vle

import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

class FileCommand implements Validateable {

    MultipartFile uploadedFile

    static def fileValidator = { val, obj ->
        if ( val == null ) {
            return false
        }
        if ( val.empty ) {
            return false
        }

        ['zip'].any { extension -> 
            val.originalFilename?.toLowerCase()?.endsWith(extension)
        }
    }

    static constraints = {
        uploadedFile validator: fileValidator
    }
}