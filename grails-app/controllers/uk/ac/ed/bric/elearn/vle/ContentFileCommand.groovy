package uk.ac.ed.bric.elearn.vle

import grails.validation.Validateable
import org.springframework.web.multipart.MultipartFile

class ContentFileCommand implements Validateable {

    MultipartFile uploadedContentFile

    static def contentFileValidator = { val, obj ->
        if ( val == null ) {
            return false
        }
        if ( val.empty ) {
            return false
        }

        ['doc', 'docx', 'pdf', 'mp3'].any { extension -> 
            val.originalFilename?.toLowerCase()?.endsWith(extension)
        }
    }

    static constraints = {
        uploadedContentFile validator: contentFileValidator
    }
}