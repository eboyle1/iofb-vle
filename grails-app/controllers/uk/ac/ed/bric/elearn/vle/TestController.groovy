package uk.ac.ed.bric.elearn.vle

import grails.plugin.springsecurity.annotation.Secured
import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional

@Transactional(readOnly = true)
class TestController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    @Secured('ROLE_ADMIN') 
    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond Test.list(params), model:[testCount: Test.count()]
    }

    @Secured('ROLE_ADMIN') 
    def show(Test test) {
        respond test
    }

    @Secured('ROLE_ADMIN') 
    def create() {
        respond new Test(params)
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def save(Test test) {
        if (test == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (test.hasErrors()) {
            transactionStatus.setRollbackOnly()
            respond test.errors, view:'create', model: [lecture: test.lecture, module: test.lecture.module, course: test.lecture.module.course ]
            return
        }

        test.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'test.label', default: 'Test'), test.id])
                redirect test
            }
            '*' { respond test, [status: CREATED] }
        }
    }

    @Secured('ROLE_ADMIN') 
    def edit(Test test) {
        respond test
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def update(Test test) {
        if (test == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        if (test.hasErrors()) {
            transactionStatus.setRollbackOnly()
            //respond test.errors, view:'edit'
            render view:'edit', model: [test: test, lecture: test.lecture]
            return
        }

        test.save flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'test.label', default: 'Test'), test.id])
                redirect test
            }
            '*'{ respond test, [status: OK] }
        }
    }

    @Secured('ROLE_ADMIN') 
    @Transactional
    def delete(Test test) {

        if (test == null) {
            transactionStatus.setRollbackOnly()
            notFound()
            return
        }

        test.delete flush:true

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'test.label', default: 'Test'), test.id])
                //redirect action:"index", method:"GET"
                redirect controller: "lecture", action:"listTests", id: test.lecture.id, method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'test.label', default: 'Test'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

    @Secured('ROLE_ADMIN') 
    def userReport1()
    {
        //use hql to select the highest mark for each student in UserTestSession 
        //to make gsp sortable columns work use params to order/sort and select as map in hql
        //using max() to aggregate some of the columns is a bit of a fudge, as any_value() is not available in hql
        String hql = " select new map (a.userId as userId, max(a.userFirstName) as firstName, max(a.userLastName) as lastName, a.test.id as testId, max(a.test.name) as testName, max(a.test.lecture.name) as lectureName, max(a.test.lecture.module.name) as moduleName, max(a.mark) as mark) from UserTestSession a group by a.userId, a.test.id order by $params.sort $params.order "
        def result = UserTestSession.executeQuery(hql)
        [ userTestSessions : result ]
    }

    @Secured('ROLE_ADMIN') 
    def userReport2()
    {
        //use hql to select all students in UserAdmin 
        //to make gsp sortable columns work use params to order/sort and select as map in hql
        String hql = " select new map (a.userId as userId, a.userFirstName as firstName, a.userLastName as lastName) from UserAdmin a order by $params.sort $params.order "
        def result = UserAdmin.executeQuery(hql)
        [ userAdmins : result ]
    }

}
