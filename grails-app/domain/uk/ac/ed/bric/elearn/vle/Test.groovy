package uk.ac.ed.bric.elearn.vle

class Test {

	String name
  Integer orderNo
  Boolean isVisible
	String description
	Date dateCreated
	Date lastUpdated  

  String toString() {
        return name
  }
    
  static belongsTo = [ lecture : Lecture ]
	static hasMany = [ questionGroups: QuestionGroup ]

	static constraints = {
   		name blank:false, nullable:false
   		orderNo blank:false, nullable:false, unique:['lecture']
   		isVisible blank:false, nullable:false
		  description blank:false, nullable:false
	}

  def firstQuestionGroup() {
    this.questionGroups.find{it.orderNo == 1}
  }

}