package uk.ac.ed.bric.elearn.vle

class LectureFile {

    String name
    String fileName
    String filePath
	String fileType
    String baseFileName
    Date dateCreated
    Date lastUpdated

    static belongsTo = [ lecture : Lecture ]

    static constraints = {
        name blank:true, nullable:true
        fileName blank:true, nullable:true
    	filePath blank:true, nullable:true, unique: true
        fileType blank:true, nullable:true
        baseFileName blank:true, nullable:true
        //lecture blank:true, nullable:true
    }

}