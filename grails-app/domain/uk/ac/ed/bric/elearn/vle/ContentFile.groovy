package uk.ac.ed.bric.elearn.vle

class ContentFile {

    String name
    String fileName
    String filePath
	int fileType
    String baseFileName
    Date dateCreated
    Date lastUpdated
    Lecture owner

    //static belongsTo = [ lecture : Lecture ]
    static belongsTo = Lecture

    static constraints = {
        name blank:true, nullable:true
        fileName blank:true, nullable:true
    	filePath blank:true, nullable:true, unique: true
        fileType blank:true, nullable:true
        baseFileName blank:true, nullable:true
        //lecture blank:true, nullable:true
    }

}