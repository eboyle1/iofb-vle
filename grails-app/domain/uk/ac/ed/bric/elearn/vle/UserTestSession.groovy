package uk.ac.ed.bric.elearn.vle

import uk.ac.ed.bric.elearn.User

class UserTestSession {
	
	Long userId
	String userFirstName
	String userLastName
	float mark
	int totalQuestions
	Test test

	static hasMany = [ answers : Answer ]

	def userTestSessionService

	User getUser() {
		userId ? User.get(userId) : null
	}

	void setUser(User user) {
		userId = user.id
		userFirstName = user.firstName
		userLastName = user.lastName
	}
	
	static transients = ['user']

	static constraints = {
		answers nullable:true
		mark blank:true, nullable:true
		totalQuestions blank:true, nullable:true
	}
}