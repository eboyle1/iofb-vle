package uk.ac.ed.bric.elearn.vle

class Lecture {

    String name
    String code
    Integer orderNo
    Boolean isVisible
	String description
	String authors 
	String editors
	String learningObjectives
    Date dateCreated
    Date lastUpdated

    String toString() {
        return name
    }
    
    static belongsTo = [ module : Module ]
    static hasOne = [ lectureFile : LectureFile ]
    static hasMany = [ contentFiles : ContentFile, tests : Test ]
 
    static constraints = {
        name blank:false, nullable:false
        code blank:true, nullable:true, unique:true
        orderNo blank:false, nullable:false, unique:['module']
        isVisible blank:false, nullable:false
    	description blank:false, nullable:false
    	authors blank:true, nullable:true
    	editors blank:true, nullable:true
    	learningObjectives blank:true, nullable:true
        lectureFile blank:true, nullable:true, unique:true
        contentFiles blank:true, nullable:true
    }

    static mapping = {
		learningObjectives type: "text"
        contentFiles lazy:false, fetch: "join"
	}
}
