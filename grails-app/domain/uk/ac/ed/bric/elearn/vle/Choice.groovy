package uk.ac.ed.bric.elearn.vle

class Choice {

    Integer choiceOrder
	String choiceText
    Question owner
    Boolean choiceStatus 

	static belongsTo =  Question

	static constraints = {
    	choiceOrder blank:false, nullable:false, unique:['owner']
    	choiceText blank:false, nullable:false
        choiceStatus blank:true, nullable:true
    }

    static mapping = {
		choiceText type: "text"
	}

}
