package uk.ac.ed.bric.elearn.vle

import uk.ac.ed.bric.elearn.User

class CourseEnrolment {

	Course course
	Long userId
	
    Date dateCreated
    Date lastUpdated

    User getUser() {
        userId ? User.get(userId) : null
    }

    void setUser(User user) {
        userId = user.id
    }

    static transients = ['user']

    static constraints = {
    }
}