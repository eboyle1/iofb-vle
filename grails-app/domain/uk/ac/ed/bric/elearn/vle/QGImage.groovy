package uk.ac.ed.bric.elearn.vle

class QGImage {

    String name
    String fileName
    String filePath
	String fileType
    String baseFileName
    Date dateCreated
    Date lastUpdated

    static belongsTo = [ questionGroup:QuestionGroup ]

    static constraints = {
        name blank:true, nullable:true
        fileName blank:true, nullable:true
    	filePath blank:true, nullable:true, unique: true
        fileType blank:true, nullable:true
        baseFileName blank:true, nullable:true
    }

}