package uk.ac.ed.bric.elearn.vle

class Module {

	String name
	String code
	Integer orderNo
	Boolean isVisible
	String moduleResource
	Date dateCreated
    Date lastUpdated

    String toString() {
        return name
    }

	static hasMany = [ lectures : Lecture ]
  	static belongsTo = [ course : Course ]

    static constraints = {
    	 name blank:false, nullable:false
    	 code blank:true, nullable:true, unique:true
    	 orderNo blank:false, nullable:false, unique:['course']
    	 isVisible blank:false, nullable:false
    	 moduleResource blank:true, nullable:true
    }

    static mapping = {
		moduleResource type: "text"
	}

}
