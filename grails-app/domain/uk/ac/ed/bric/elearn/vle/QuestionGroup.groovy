package uk.ac.ed.bric.elearn.vle

class QuestionGroup {

    Integer orderNo
    Boolean isVisible
	String questionGroupText

    static belongsTo =  [ owner : Test ]
    static hasMany = [ questions : Question, qGImages : QGImage ]

    static constraints = {
    	orderNo blank:false, nullable:false, unique:['owner']
   		isVisible blank:true, nullable:true
    	questionGroupText blank:false, nullable:false
    }

    static mapping = {
		questionGroupText type: "text"
	}

    String toString() {
        if (owner)
        {
            String qgname = owner.name + ": " + orderNo
            return qgname
        }
    }

}