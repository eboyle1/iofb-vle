package uk.ac.ed.bric.elearn.vle

class Course {

	String name
	String code
	Integer orderNo
	Boolean isVisible
	String welcomePage
	String staffPage
	Date dateCreated
    Date lastUpdated

    String toString() {
        return name
    }

 	static hasMany = [ modules : Module ]

    static constraints = {
    	 name blank:false, nullable:false
    	 code blank:true, nullable:true, unique:true
    	 orderNo blank:false, nullable:false, unique:true
    	 isVisible blank:false, nullable:false
    	 welcomePage blank:true, nullable:true
    	 staffPage blank:true, nullable:true
    }

    static mapping = {
        welcomePage type: "text"
        staffPage type: "text"
    }
}
