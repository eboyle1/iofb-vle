package uk.ac.ed.bric.elearn.vle

import uk.ac.ed.bric.elearn.User

class UserAdmin {
	
	Long userId
	String userFirstName
	String userLastName
	Boolean testsSuccess

	def userAdminService

	User getUser() {
		userId ? User.get(userId) : null
	}

	void setUser(User user) {
		userId = user.id
		userFirstName = user.firstName
		userLastName = user.lastName
	}
	
	static transients = ['user']

	static constraints = {
		testsSuccess blank:true, nullable:true
	}
}