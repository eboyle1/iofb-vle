package uk.ac.ed.bric.elearn.vle
import grails.util.Environment

class Answer {
	
	Choice choice
	Boolean correct

	static belongsTo = [ owner : Question, userTestSession : UserTestSession ]

	static constraints = {
	 	choice nullable:false
	 	correct blank:true, nullable:true
	}

	Boolean isChosen(choice) {
		this.choice == choice
	}
}