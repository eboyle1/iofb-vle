package uk.ac.ed.bric.elearn.vle

class Question {

    Integer orderNo
    Boolean isVisible
	String questionText

    static belongsTo = [ owner : QuestionGroup ]
    static hasMany = [ choices: Choice ]

    static constraints = {
    	orderNo blank:false, nullable:false, unique:['owner']
   		isVisible blank:true, nullable:true
    	questionText blank:false, nullable:false
    }

    static mapping = {
		questionText type: "text"
	}
}