
import uk.ac.ed.bric.elearn.vle.Course
import uk.ac.ed.bric.elearn.vle.Module
import uk.ac.ed.bric.elearn.vle.Lecture
import uk.ac.ed.bric.elearn.vle.Test
import uk.ac.ed.bric.elearn.vle.QuestionGroup
import uk.ac.ed.bric.elearn.vle.Question
import uk.ac.ed.bric.elearn.vle.Choice

import uk.ac.ed.bric.elearn.Role
import uk.ac.ed.bric.elearn.RoleGroup
import uk.ac.ed.bric.elearn.RoleGroupRole
import uk.ac.ed.bric.elearn.User
import uk.ac.ed.bric.elearn.UserRoleGroup

class BootStrap {

    def init = { servletContext ->
    	environments {
            development {
                createTestContent()
                setupUser('admin', 'admin', 'user', 'tu4acc£55', 'ROLE_ADMIN', 'ADMIN_GROUP')
                setupUser('student', 'student', 'user', 'tu4acc£55', 'ROLE_USER', 'USER_GROUP')
            }
            test {
            	
            }
            production {
                 
            }
        }
    }

    def createTestContent() {

        def course1 = new Course(name:'Intraorbital Foreign Body Reviewing',
                                  code:'C001',
                                  orderNo: 1,
                                  isVisible: true,
                                  staffPage: '<p><u><strong>Course Staff</strong></u></p><p><strong>Andrew Farrall</strong><br />Honorary Professor of Neuroimaging &amp; Education and Consultant Neuroradiologist</p><ul><li>Programme director, MSc Neuroimaging for Research &amp; Imaging MSc</li><li>Neuroradiologist, 2002-present. Brain Research Imaging Centre, University of Edinburgh &amp; Division of Clinical Neurosciences, Lothian NHS</li><li>Neuroradiology Fellow, 2004-2007. University of Edinburgh. Supervisor: Prof. JM Wardlaw</li>  <li>Neuroradiology Clinical Trainee, 1997-2002. QEII Health Sciences Centre, Halifax, Canada</li><li>Fellow of the Royal College of Physicians of Canada (Diagnostic Radiology), 2002</li>  <li>American Board of Radiology Certification (Diagnostic Imaging), 2002</li><li>Licentiate of the Medical Council of Canada, 1998</li>  <li>Medicinae Doctor (MBChB equivalent), 1997. University of Calgary</li><li>Master of Science (Medical Biophysics), 1995. University of Western Ontario</li><li>Bachelor of Science (Honours, First Class, Physics &amp; Chemistry), 1990. University of British Columbia</li></ul></p>',
                                  welcomePage: '<p><strong>Welcome to the IOFB course homepage</strong></p>'
                                  )
        def course2 = new Course(name:'Course 2',
                                  code:'C002',
                                  orderNo: 2,
                                  isVisible: true
                                  )
        def course3 = new Course(name:'Course 3',
                                  code:'C003',
                                  orderNo: 3,
                                  isVisible: true
                                  )
        def module1 = new Module(name:'Background and Technique',
                                  code:'M001',
                                  orderNo: 1,
                                  isVisible: true
                                  )
        def module2 = new Module(name:'Practicalities',
                                  code:'M002',
                                  orderNo: 2,
                                  isVisible: true
                                  )
        def module3 = new Module(name:'Test module 3',
                                  code:'M003',
                                  orderNo: 3,
                                  isVisible: false
                                  )
        def lecture1 = new Lecture(name:'Lecture 1 - Overview',
                                  code:'L001',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'Case studies, current advice, screening process',
                                  authors:'Elaine Sandeman',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><li>Explain screening for IOFB</li><li>Define extended role for the radiographer</li><li>Give an overview of some case histories</li><li>Analyse&nbsp;whether X-ray radiography is necessary in screening process</li><li>Debate the problems around screening for IOFB</li></ul>'
                                  )
        def lecture2 = new Lecture(name:'Technique',
                                  code:'L002',
                                  orderNo: 2,
                                  isVisible: true,
                                  description:'Radiography & reporting techniques; example images',
                                  authors:'Elaine Sandeman, Andrew Farrall',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'TODO'
                                  )
        def lecture3 = new Lecture(name:'Orbital anatomy',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'Bony anatomy of the orbits and related imaging; relevant soft tissue anatomy',
                                  authors:'Jennifer Ellison, Andrew Farrall',
                                  learningObjectives:'TODO'
                                  )
        def test1 = new Test(name:'Module 1 Lecture 1 Test 1',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M1_L1_Assessment_AF'
                                  )
        def test2 = new Test(name:'Module 1 Lecture 1 Test 2',
                                  orderNo: 2,
                                  isVisible: true,
                                  description:'IOFB_M1_L1_Assessment_AF (2)'
                                  )
        def test3 = new Test(name:'Module 2 Lecture 1 Part 1 Test',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M2_L1_Assessment1_AF'
                                  )
        def questionGroup1 = new QuestionGroup(orderNo: 1,
                                  questionGroupText:'With regards to principles behind screening for metallic IOFBs, answer the following as TRUE or FALSE:'
                                  )
        def question1 = new Question(orderNo: 1,
                                  questionText:'A. Orbital tissues fix metallic IOFBs in place by fibrosis'
                                  )
        def choice1 = new Choice(choiceOrder: 1,
                                  choiceText: 'TRUE',
                                  choiceStatus: false
                                  )
        def choice2 = new Choice(choiceOrder: 2,
                                  choiceText: 'FALSE',
                                  choiceStatus: true
                                  )
        def question2 = new Question(orderNo: 2,
                                  questionText:'B. Magnetic torque may cause a metallic IOFBs to move'
                                  )
        def choice3 = new Choice(choiceOrder: 1,
                                  choiceText: 'TRUE',
                                  choiceStatus: true
                                  )
        def choice4 = new Choice(choiceOrder: 2,
                                  choiceText: 'FALSE',
                                  choiceStatus: false
                                  )
        def questionGroup2 = new QuestionGroup(orderNo: 2,
                                  questionGroupText:'With regards to consideration of radiographer led metallic IOFB screening &amp; orbital radiograph reporting, which of the following are likely to be INCREASED or DECREASED:'
                                  )
        def question3 = new Question(orderNo: 1,
                                  questionText:'A. MR patient list efficiency'
                                  )
        def choice5 = new Choice(choiceOrder: 1,
                                  choiceText: 'INCREASED',
                                  choiceStatus: false
                                  )
        def choice6 = new Choice(choiceOrder: 2,
                                  choiceText: 'DECREASED',
                                  choiceStatus: true
                                  )
        def questionGroup3 = new QuestionGroup(orderNo: 3,
                                  questionGroupText:'With regards to establishing a specialist radiographer metallic IOFB reporting service, answer the following as TRUE or FALSE:'
                                  )
        def question4 = new Question(orderNo: 1,
                                  questionText:'A. Radiographers must attend and pass approved training'
                                  )
        def choice7 = new Choice(choiceOrder: 1,
                                  choiceText: 'TRUE',
                                  choiceStatus: true
                                  )
        def choice8 = new Choice(choiceOrder: 2,
                                  choiceText: 'FALSE',
                                  choiceStatus: false
                                  )
        def question5 = new Question(orderNo: 2,
                                  questionText:'B. Radiographers may work outwith an approved framework'
                                  )
        def choice9 = new Choice(choiceOrder: 1,
                                  choiceText: 'FALSE',
                                  choiceStatus: true
                                  )
        def choice10 = new Choice(choiceOrder: 2,
                                  choiceText: 'TRUE',
                                  choiceStatus: false
                                  )
        def questionGroup4 = new QuestionGroup(orderNo: 1,
                                  questionGroupText:'Regarding the bones which comprise the orbit, answer the following TRUE or FALSE:'
                                  )
        def question6 = new Question(orderNo: 1,
                                  questionText:'A. The orbit is bordered by seven bones'
                                  )
        def choice11 = new Choice(choiceOrder: 1,
                                  choiceText: 'TRUE',
                                  choiceStatus: true
                                  )
        def choice12 = new Choice(choiceOrder: 2,
                                  choiceText: 'FALSE',
                                  choiceStatus: false
                                  )  
        def questionGroup5 = new QuestionGroup(orderNo: 2,
                                  questionGroupText:'Identify the structures labelled in this orbital radiograph'
                                  )
        def question7 = new Question(orderNo: 1,
                                  questionText:'A.'
                                  ) 
        def choice13 = new Choice(choiceOrder: 1,
                                  choiceText: 'Cribriform plate',
                                  choiceStatus: false
                                  )
        def choice14 = new Choice(choiceOrder: 2,
                                  choiceText: 'Foramen rotundum',
                                  choiceStatus: true
                                  )
        def choice15 = new Choice(choiceOrder: 3,
                                  choiceText: 'Infra-orbital foramen',
                                  choiceStatus: false
                                  )
        def choice16 = new Choice(choiceOrder: 4,
                                  choiceText: 'Inominate line',
                                  choiceStatus: false
                                  )
        def choice17 = new Choice(choiceOrder: 5,
                                  choiceText: 'Lamina papyracea',
                                  choiceStatus: false
                                  )
        def choice18 = new Choice(choiceOrder: 6,
                                  choiceText: 'Maxillary bone',
                                  choiceStatus: false
                                  )
        def choice19 = new Choice(choiceOrder: 7,
                                  choiceText: 'Nasal bone',
                                  choiceStatus: false
                                  )
        def choice20 = new Choice(choiceOrder: 8,
                                  choiceText: 'Nasal septum',
                                  choiceStatus: false
                                  )
        def choice21 = new Choice(choiceOrder: 9,
                                  choiceText: 'Palatine bone',
                                  choiceStatus: false
                                  )
        def choice22 = new Choice(choiceOrder: 10,
                                  choiceText: 'Zygoma',
                                  choiceStatus: false
                                  )
        course1.addToModules(module1)
        course1.addToModules(module2)
        course1.addToModules(module3)
        module1.addToLectures(lecture1)
        module1.addToLectures(lecture2)
        module2.addToLectures(lecture3)
        lecture1.addToTests(test1)
        lecture1.addToTests(test2)
        lecture3.addToTests(test3)
        test1.addToQuestionGroups(questionGroup1)
        test1.addToQuestionGroups(questionGroup2)
        test1.addToQuestionGroups(questionGroup3)
        test3.addToQuestionGroups(questionGroup4)
        test3.addToQuestionGroups(questionGroup5)
        questionGroup1.addToQuestions(question1)
        questionGroup1.addToQuestions(question2)
        questionGroup2.addToQuestions(question3)
        questionGroup3.addToQuestions(question4)
        questionGroup3.addToQuestions(question5)
        questionGroup4.addToQuestions(question6)
        questionGroup5.addToQuestions(question7)
        question1.addToChoices(choice1)
        question1.addToChoices(choice2)
        question2.addToChoices(choice3)
        question2.addToChoices(choice4)
        question3.addToChoices(choice5)
        question3.addToChoices(choice6)
        question4.addToChoices(choice7)
        question4.addToChoices(choice8)
        question5.addToChoices(choice9)
        question5.addToChoices(choice10)
        question6.addToChoices(choice11)
        question6.addToChoices(choice12)
        question7.addToChoices(choice13)
        question7.addToChoices(choice14)
        question7.addToChoices(choice15)
        question7.addToChoices(choice16)
        question7.addToChoices(choice17)
        question7.addToChoices(choice18)
        question7.addToChoices(choice19)
        question7.addToChoices(choice20)
        question7.addToChoices(choice21)
        question7.addToChoices(choice22)

        course1.save (failOnError:true, flush:true)
        course2.save (failOnError:true, flush:true)
        course3.save (failOnError:true, flush:true)
    }

 
    def setupUser(username, forename, surname, password, roleName, groupName) {
        // create role
        def role = Role.findByAuthority(roleName) ?: new Role(authority: roleName).save(flush: true) 
        // create group
        def group = RoleGroup.findByName(groupName) ?: new RoleGroup(name: groupName).save(flush:true)

        // associate user role with user group
        def roleGroupRole = new RoleGroupRole()
        RoleGroupRole.create group, role

        // create user
        def user = User.findByUsername(username) ?: new User(
                username: username,
                firstName: forename,
                emailAddress: username,
                lastName: surname,
                password: password,
                enabled: true).save(flush: true)
        
        // associate  user with  group
        UserRoleGroup.create user, group
        
        return user
    }

    def destroy = {
    }
}
