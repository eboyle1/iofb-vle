
import uk.ac.ed.bric.elearn.vle.Course
import uk.ac.ed.bric.elearn.vle.Module
import uk.ac.ed.bric.elearn.vle.Lecture
import uk.ac.ed.bric.elearn.vle.LectureFile
import uk.ac.ed.bric.elearn.vle.ContentFile
import uk.ac.ed.bric.elearn.vle.Test
import uk.ac.ed.bric.elearn.vle.QuestionGroup
import uk.ac.ed.bric.elearn.vle.QGImage
import uk.ac.ed.bric.elearn.vle.Question
import uk.ac.ed.bric.elearn.vle.Choice
import uk.ac.ed.bric.elearn.vle.UserTestSession
import uk.ac.ed.bric.elearn.vle.CourseEnrolment

import uk.ac.ed.bric.elearn.Role
import uk.ac.ed.bric.elearn.RoleGroup
import uk.ac.ed.bric.elearn.RoleGroupRole
import uk.ac.ed.bric.elearn.User
import uk.ac.ed.bric.elearn.UserRoleGroup

class BootStrap {

    def init = { servletContext ->
    	environments {
            development {
                createTestContent()
                setupUser('admin', 'admin', 'user', 'tu4acc£55', 'ROLE_ADMIN', 'ADMIN_GROUP')
                setupUser('student1', 'student1', 'user', 'tu4acc£55', 'ROLE_USER', 'USER_GROUP')
                setupUser('student2', 'student2', 'user', 'tu4acc£55', 'ROLE_USER', 'USER_GROUP')
            }
            test {
            	
            }
            production {
             //createTestContent()
            }
        }
    }

    def createTestContent() {

        //create dev courses, modules, lectures and content

        def course1 = new Course(name:'Intraorbital Foreign Body Review',
                                  code:'C001',
                                  orderNo: 1,
                                  isVisible: true,
                                  staffPage: '<p><u><strong>Course Staff</strong></u></p><p><strong>Andrew Farrall</strong><br />Honorary Professor of Neuroimaging &amp; Education and Consultant Neuroradiologist</p><ul><li>Programme director, MSc Neuroimaging for Research &amp; Imaging MSc</li><li>Neuroradiologist, 2002-present. Brain Research Imaging Centre, University of Edinburgh &amp; Division of Clinical Neurosciences, Lothian NHS</li><li>Neuroradiology Fellow, 2004-2007. University of Edinburgh. Supervisor: Prof. JM Wardlaw</li>  <li>Neuroradiology Clinical Trainee, 1997-2002. QEII Health Sciences Centre, Halifax, Canada</li><li>Fellow of the Royal College of Physicians of Canada (Diagnostic Radiology), 2002</li>  <li>American Board of Radiology Certification (Diagnostic Imaging), 2002</li><li>Licentiate of the Medical Council of Canada, 1998</li>  <li>Medicinae Doctor (MBChB equivalent), 1997. University of Calgary</li><li>Master of Science (Medical Biophysics), 1995. University of Western Ontario</li><li>Bachelor of Science (Honours, First Class, Physics &amp; Chemistry), 1990. University of British Columbia</li></ul></p>',
                                  welcomePage: '<p><strong>Welcome to the IOFB course homepage</strong></p>'
                                  )
        def course2 = new Course(name:'Course 2',
                                  code:'C002',
                                  orderNo: 2,
                                  isVisible: true
                                  )
        def course3 = new Course(name:'Course 3',
                                  code:'C003',
                                  orderNo: 3,
                                  isVisible: true
                                  )
        def module1 = new Module(name:'Background and Technique',
                                  code:'M001',
                                  orderNo: 1,
                                  isVisible: true
                                  )
        def module2 = new Module(name:'Practicalities',
                                  code:'M002',
                                  orderNo: 2,
                                  isVisible: true
                                  )
        def module3 = new Module(name:'Test module 3',
                                  code:'M003',
                                  orderNo: 3,
                                  isVisible: false
                                  )
        def lecture1 = new Lecture(name:'Lecture 1 - Overview',
                                  code:'L001',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'Case studies, current advice, screening process',
                                  authors:'Elaine Sandeman',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><li>Explain screening for IOFB</li><li>Define extended role for the radiographer</li><li>Give an overview of some case histories</li><li>Analyse&nbsp;whether X-ray radiography is necessary in screening process</li><li>Debate the problems around screening for IOFB</li></ul>'
                                  )
        def lecture2 = new Lecture(name:'Lecture 2 - Technique',
                                  code:'L002',
                                  orderNo: 2,
                                  isVisible: true,
                                  description:'Radiography & reporting techniques; example images',
                                  authors:'Elaine Sandeman, Andrew Farrall',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><li>State the requirements of x-ray projection</li><li>Explain the importance of reporting technique</li><li>Analyse good and bad technique</li><li>Describe practical procedures used in Edinburgh</li></ul>'
                                  )
        def lecture3 = new Lecture(name:'Lecture 3 - IOFB reporting - how to',
                                  code:'L003',
                                  orderNo: 3,
                                  isVisible: true,
                                  description:'Practical guide to evaluating & assessing orbital radiographs for intra-orbital foreign bodies',
                                  authors:'Dr Grant Mair',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><ul><li>Define foreign body</li><li>Discuss radiographic appearances of foreign bodies</li><li>State how to know a foreign body is definitely in the orbit</li><li>State how to know a foreign body is non-deliberate</li><li>Recognize foreign body false positives</li><li>Apply strategies to handle poor quality radiographs</li></ul>'
                                  ) 
        def lecture4 = new Lecture(name:'Lecture 1 - Orbital anatomy',
                                  code:'L004',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'Bony anatomy of the orbits and related imaging; relevant soft tissue anatomy',
                                  authors:'Jennifer Ellison, Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><ul><li>Identify on imaging<ul><li>Bony orbital anatomy</li><li>Bony structures adjacent to and related to the orbit</li><li>Other structures which may be visible on orbital radiography</li><li>Soft tissue structures in and adjacent to the orbit</li></ul></li><li>Describe those structures which contribute to the appearances of an orbital radiograph</li><li>Locate approximately those structures which lie in and about the orbit, but which are not always visible on orbital radiography</li></ul>'
                                  )
        def lecture5 = new Lecture(name:'Lecture 2 - IRMER 2000',
                                  code:'L005',
                                  orderNo: 2,
                                  isVisible: true,
                                  description:'Ionising radiation (medical exposure) regulations 2000 for radiographers',
                                  authors:'Jennifer Ellison',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><ul><li>Explain the purpose, application and responsibility of IR(ME)R 2000</li><li>Give an overview of the responsibilities of all duty holders under IR(ME)R 2000:</li><ul><li>Employer</li><li>Practitioner</li><li>Operator</li><li>Referrer</li><li>Medical physics expert</li></ul></li><li>Interpret the appropriate dosage of exposure to radiation</li></ul>'
                                  )
        def lecture6 = new Lecture(name:'Lecture 3 - Policies and Protocols',
                                  code:'L006',
                                  orderNo: 3,
                                  isVisible: true,
                                  description:'Policies and protocols at work',
                                  authors:'Jennifer Ellison',
                                  editors:'Andrew Farrall',
                                  learningObjectives:'<p>On completion of this lecture, you should be able to:</p><ul><li>Define policy and protocol</li><li>Explain why protocol is needed</li><li>Describe how a protocol is developed &amp; changed</li></ul>'
                                  )
        def lectureFile1 = new LectureFile(name: 'View lecture',
                                  fileName: '1_main_lecture_1512384512791_IOFB M1L1 Lecture 20170626ca forLearn both.zi<ul><li>Define policy and protocol</li><li>Explain why protocol is needed</li><li>Describe how a protocol is developed &amp; changed</li></ul>p',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\1_main_lecture_1512384512791_IOFB M1L1 Lecture 20170626ca forLearn both.zip',
                                  baseFileName: '1_main_lecture_1512384512791_IOFB M1L1 Lecture 20170626ca forLearn both'
                                  )
        def contentFile1 = new ContentFile(name: 'Download lecture audio file (mp3)',
                                  fileName: '1_content_1512382046115_IOFB_M1L1_(overview).mp3',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\1\\1_content_1512382046115_IOFB_M1L1_(overview).mp3',
                                  baseFileName: '1_content_1512382046115_IOFB_M1L1_(overview)',
                                  fileType: 1
                                  )
        def contentFile2 = new ContentFile(name: 'Download lecture resources (pdf)',
                                  fileName: '1_content_1512382187921_Lecture_1_Resources.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\1\\1_content_1512382187921_Lecture_1_Resources.pdf',
                                  baseFileName: '1_content_1512382187921_Lecture_1_Resources',
                                  fileType: 2
                                  )
        def contentFile3 = new ContentFile(name: 'Download lecture slides (pdf)',
                                  fileName: '1_content_1512562802595_IOFB_M1L1_Lecture_20170626ca_forLearn.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\1\\1_content_1512562802595_IOFB_M1L1_Lecture_20170626ca_forLearn.pdf',
                                  baseFileName: '1_content_1512562802595_IOFB_M1L1_Lecture_20170626ca_forLearn',
                                  fileType: 0
                                  )
        def lectureFile2 = new LectureFile(name: 'View lecture',
                                  fileName: '2_main_lecture_1512568586634_IOFB_M1L2_Technique_20170630ca_forLearn.zip',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\2_main_lecture_1512568586634_IOFB_M1L2_Technique_20170630ca_forLearn.zip',
                                  baseFileName: '2_main_lecture_1512568586634_IOFB_M1L2_Technique_20170630ca_forLearn'
                                  )
        def contentFile4 = new ContentFile(name: 'Download lecture audio file (mp3)',
                                  fileName: '2_content_1512570927978_IOFB_M1L2_(Technique).mp3',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\2\\2_content_1512570927978_IOFB_M1L2_(Technique).mp3',
                                  baseFileName: '2_content_1512570927978_IOFB_M1L2_(Technique)',
                                  fileType: 1
                                  )
        def contentFile5 = new ContentFile(name: 'Download lecture resources (pdf)',
                                  fileName: '2_content_1512572293687_Lecture_2_Resources.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\2\\2_content_1512572293687_Lecture_2_Resources.pdf',
                                  baseFileName: '2_content_1512572293687_Lecture_2_Resources',
                                  fileType: 2
                                  )
        def contentFile6 = new ContentFile(name: 'Download lecture slides (pdf)',
                                  fileName: '2_content_1512573060960_IOFB_M1L2_(Technique).pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\2\\2_content_1512573060960_IOFB_M1L2_(Technique).pdf',
                                  baseFileName: '2_content_1512573060960_IOFB_M1L2_(Technique)',
                                  fileType: 0                                  
                                  )
        def lectureFile3 = new LectureFile(name: 'View lecture',
                                  fileName: '3_main_lecture_1512575050454_IOFB_M2_L3_reporting_-_how_to_0170630ca_forLearn.zip',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\3_main_lecture_1512575050454_IOFB_M2_L3_reporting_-_how_to_0170630ca_forLearn.zip',
                                  baseFileName: '3_main_lecture_1512575050454_IOFB_M2_L3_reporting_-_how_to_0170630ca_forLearn'
                                  )
        def contentFile7 = new ContentFile(name: 'Download lecture audio file (mp3)',
                                  fileName: '3_content_1512576203263_IOFB_M1L3_IOFB_reporting_-_how_to.mp3',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\3\\3_content_1512576203263_IOFB_M1L3_IOFB_reporting_-_how_to.mp3',
                                  baseFileName: '3_content_1512576203263_IOFB_M1L3_IOFB_reporting_-_how_to',
                                  fileType: 1
                                  )
        def contentFile8 = new ContentFile(name: 'Download lecture slides (pdf)',
                                  fileName: '3_content_1512576845730_IOFB_M1L3_reporting_-_how_to.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\3\\3_content_1512576845730_IOFB_M1L3_reporting_-_how_to.pdf',
                                  baseFileName: '3_content_1512576845730_IOFB_M1L3_reporting_-_how_to',
                                  fileType: 0
                                  )
        def lectureFile4 = new LectureFile(name: 'View lecture',
                                  fileName: '4_main_lecture_1512660121932_IOFB_M2L1_Anatomy_20170630ca.zip',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\4_main_lecture_1512660121932_IOFB_M2L1_Anatomy_20170630ca.zip',
                                  baseFileName: '4_main_lecture_1512660121932_IOFB_M2L1_Anatomy_20170630ca'
                                  )
        def contentFile9 = new ContentFile(name: 'Download lecture audio file (mp3)',
                                  fileName: '4_content_1512661020536_IOFB_M2L1_Anatomy.mp3',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\4\\4_content_1512661020536_IOFB_M2L1_Anatomy.mp3',
                                  baseFileName: '4_content_1512661020536_IOFB_M2L1_Anatomy',
                                  fileType: 1
                                  )
        def contentFile10 = new ContentFile(name: 'Download lecture slides (pdf)',
                                  fileName: '4_content_1512661554908_IOFB_M2L1_Anatomy.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\4\\4_content_1512661554908_IOFB_M2L1_Anatomy.pdf',
                                  baseFileName: '4_content_1512661554908_IOFB_M2L1_Anatomy',
                                  fileType: 0
                                  )
        def lectureFile5 = new LectureFile(name: 'View lecture',
                                  fileName: '5_main_lecture_1512654222209_IOFB_M2L2_Regulations_20170630ca_forLearn.zip',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\5_main_lecture_1512654222209_IOFB_M2L2_Regulations_20170630ca_forLearn.zip',
                                  baseFileName: '5_main_lecture_1512654222209_IOFB_M2L2_Regulations_20170630ca_forLearn'
                                  )
        def contentFile11 = new ContentFile(name: 'Download lecture resources (pdf)',
                                  fileName: '5_content_1512655111969_M2L2_Resources.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\5\\5_content_1512655111969_M2L2_Resources.pdf',
                                  baseFileName: '5_content_1512655111969_M2L2_Resources',
                                  fileType: 2
                                  )
        def contentFile12 = new ContentFile(name: 'Download lecture audio file (mp3)',
                                  fileName: '5_content_1512656026416_IOFB_M2L2_(IRMER 2000).mp3',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\5\\5_content_1512656026416_IOFB_M2L2_(IRMER 2000).mp3',
                                  baseFileName: '5_content_1512656026416_IOFB_M2L2_(IRMER 2000)',
                                  fileType: 1
                                  )
        def contentFile13 = new ContentFile(name: 'Download lecture slides (pdf)',
                                  fileName: '5_content_1512656722439_IOFB_M2L2_(IRMER_2000).pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\5\\5_content_1512656722439_IOFB_M2L2_(IRMER_2000).pdf',
                                  baseFileName: '5_content_1512656722439_IOFB_M2L2_(IRMER_2000)',
                                  fileType: 0
                                  )
        def contentFile14 = new ContentFile(name: 'Protocol for radiographers reporting IOFB radiographs (pdf)',
                                  fileName: '5_content_1512657484802_IOFB_Reporting_Protocol_DCN_X-ray.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\5\\5_content_1512657484802_IOFB_Reporting_Protocol_DCN_X-ray.pdf',
                                  baseFileName: '5_content_1512657484802_IOFB_Reporting_Protocol_DCN_X-ray',
                                  fileType: 4
                                  )
        def lectureFile6 = new LectureFile(name: 'View lecture',
                                  fileName: '6_main_lecture_1512658639955_IOFB_M2L3_PolsProts_20170630ca_forLearn.zip',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\6_main_lecture_1512658639955_IOFB_M2L3_PolsProts_20170630ca_forLearn.zip',
                                  baseFileName: '6_main_lecture_1512658639955_IOFB_M2L3_PolsProts_20170630ca_forLearn'
                                  )
        def contentFile15 = new ContentFile(name: 'Download lecture resources (pdf)',
                                  fileName: '6_content_1512659292368_IOFB_M2L3_Resources.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\6\\6_content_1512659292368_IOFB_M2L3_Resources.pdf',
                                  baseFileName: '6_content_1512659292368_IOFB_M2L3_Resources',
                                  fileType: 2
                                  )
        def contentFile16 = new ContentFile(name: 'Download lecture audio file (mp3)',
                                  fileName: '6_content_1513245068916_IOFB_M2L3_PolsProts.mp3',
                                  filePath: ' C:\\Apache24\\htdocs\\lectureFiles\\6\\6_content_1513245068916_IOFB_M2L3_PolsProts.mp3',
                                  baseFileName: '6_content_1513245068916_IOFB_M2L3_PolsProts',
                                  fileType: 1
                                  )
        def contentFile17 = new ContentFile(name: 'Download lecture slides (pdf)',
                                  fileName: '6_content_1513245043445_IOFB_M2L3_PolsProts.pdf',
                                  filePath: 'C:\\Apache24\\htdocs\\lectureFiles\\6\\6_content_1513245043445_IOFB_M2L3_PolsProts.pdf',
                                  baseFileName: '6_content_1513245043445_IOFB_M2L3_PolsProts',
                                  fileType: 0
                                  )

        //create dev tests

        def test1 = new Test(name:'Module 1 Lecture 1 Test 1',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M1_L1_Assessment_AF'
                                  )
        def test2 = new Test(name:'Module 1 Lecture 2 Test 1',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M1_L2_Assessment_AF'
                                  )
        def test3 = new Test(name:'Module 2 Lecture 1 Test 1',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M2_L1_Assessment1_AF'
                                  )
        def test4 = new Test(name:'Module 2 Lecture 1 Test 2',
                                  orderNo: 2,
                                  isVisible: true,
                                  description:'IOFB_M2_L1_Assessment2_AF'
                                  )
        def test5 = new Test(name:'Module 2 Lecture 1 Test 3',
                                  orderNo: 3,
                                  isVisible: true,
                                  description:'IOFB_M2_L1_Assessment3_AF'
                                  )
        def test6 = new Test(name:'Module 2 Lecture 1 Test 4',
                                  orderNo: 4,
                                  isVisible: true,
                                  description:'IOFB_M2_L1_Assessment4_AF'
                                  )
        def test7 = new Test(name:'Module 2 Lecture 2 Test 1',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M2_L2_Assessment_AF'
                                  )
        def test8 = new Test(name:'Module 2 Lecture 3 Test 1',
                                  orderNo: 1,
                                  isVisible: true,
                                  description:'IOFB_M2_L3_Assessment_AF'
                                  )

        course1.addToModules(module1)
        course1.addToModules(module2)
        course1.addToModules(module3)
        course1.save (failOnError:true, flush:true)
        module1.addToLectures(lecture1)
        module1.addToLectures(lecture2)
        module1.addToLectures(lecture3)
        module1.save (failOnError:true, flush:true)
        module2.addToLectures(lecture4)
        module2.addToLectures(lecture5)
        module2.addToLectures(lecture6)
        module2.save (failOnError:true, flush:true)
        lecture1.setLectureFile(lectureFile1)
        lecture1.addToContentFiles(contentFile1)
        lecture1.addToContentFiles(contentFile2)
        lecture1.addToContentFiles(contentFile3)
        loadTestXML('IOFB_M1_L1.qml', test1)
        lecture1.addToTests(test1)
        lecture1.save (failOnError:true, flush:true)
        lecture2.setLectureFile(lectureFile2)
        lecture2.addToContentFiles(contentFile4)
        lecture2.addToContentFiles(contentFile5)
        lecture2.addToContentFiles(contentFile6)
        loadTestXML('IOFB_M1_L2.qml', test2)
        lecture2.addToTests(test2)
        lecture2.save (failOnError:true, flush:true)
        lecture3.setLectureFile(lectureFile3)
        lecture3.addToContentFiles(contentFile7)
        lecture3.addToContentFiles(contentFile8)
        lecture3.save (failOnError:true, flush:true)
        lecture4.setLectureFile(lectureFile4)
        lecture4.addToContentFiles(contentFile9)
        lecture4.addToContentFiles(contentFile10)
        loadTestXML('IOFB_M2_L1_T1.qml', test3)
        loadTestXML('IOFB_M2_L1_T2.qml', test4)
        loadTestXML('IOFB_M2_L1_T3.qml', test5)
        loadTestXML('IOFB_M2_L1_T4.qml', test6)    
        lecture4.addToTests(test3)
        lecture4.addToTests(test4)
        lecture4.addToTests(test5)
        lecture4.addToTests(test6)
        lecture4.save (failOnError:true, flush:true)
        lecture5.setLectureFile(lectureFile5)
        lecture5.addToContentFiles(contentFile11)
        lecture5.addToContentFiles(contentFile12)
        lecture5.addToContentFiles(contentFile13)
        lecture5.addToContentFiles(contentFile14)
        loadTestXML('IOFB_M2_L2.qml', test7) 
        lecture5.addToTests(test7)
        lecture5.save (failOnError:true, flush:true)
        lecture6.setLectureFile(lectureFile6)
        lecture6.addToContentFiles(contentFile15)
        lecture6.addToContentFiles(contentFile16)
        lecture6.addToContentFiles(contentFile17)
        loadTestXML('IOFB_M2_L3.qml', test8) 
        lecture6.addToTests(test8)
        lecture6.save (failOnError:true, flush:true)

        //create dev images for tests

        def testImage1 = new QGImage(fileName: '9_image_1512392513276_M1L2_Q4.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\9_image_1512392513276_M1L2_Q4.jpg',
                                  baseFileName: '9_image_1512392513276_M1L2_Q4'
                                  )
        def testImage2 = new QGImage(fileName: '12_image_1512402532446_M2L1_Q2.png',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\12_image_1512402532446_M2L1_Q2.png',
                                  baseFileName: '12_image_1512402532446_M2L1_Q2'
                                  )
        def testImage3 = new QGImage(fileName: '13_image_1512559011722_M2L1_Q3.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\13_image_1512559011722_M2L1_Q3.jpg',
                                  baseFileName: '13_image_1512559011722_M2L1_Q3'
                                  )
        def testImage4 = new QGImage(fileName: '14_image_1512561176222_M2L1_Q4.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\14_image_1512561176222_M2L1_Q4.jpg',
                                  baseFileName: '14_image_1512561176222_M2L1_Q4'
                                  )
        def testImage5 = new QGImage(fileName: '16_image_1513164513314_M2L1_Q6.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\16_image_1513164513314_M2L1_Q6.jpg',
                                  baseFileName: '16_image_1513164513314_M2L1_Q6'
                                  )
        def testImage6 = new QGImage(fileName: '17_image_1513165129098_M2L1_Q7.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\17_image_1513165129098_M2L1_Q7.jpg',
                                  baseFileName: '17_image_1513165129098_M2L1_Q7'
                                  )
        def testImage7 = new QGImage(fileName: '18_image_1513166066078_M2L1_Q8.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\18_image_1513166066078_M2L1_Q8.jpg',
                                  baseFileName: '18_image_1513166066078_M2L1_Q8'
                                  )
        def testImage8 = new QGImage(fileName: '20_image_1513166408197_M2L1_Q10.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\20_image_1513166408197_M2L1_Q10.jpg',
                                  baseFileName: '20_image_1513166408197_M2L1_Q10'
                                  )
        def testImage9 = new QGImage(fileName: '21_image_1513171605021_M2L1_Q11.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\21_image_1513171605021_M2L1_Q11.jpg',
                                  baseFileName: '21_image_1513171605021_M2L1_Q11'
                                  )
        def testImage10 = new QGImage(fileName: '22_image_1513171816519_M2L1_Q12.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\22_image_1513171816519_M2L1_Q12.jpg',
                                  baseFileName: '22_image_1513171816519_M2L1_Q12'
                                  )
        def testImage11 = new QGImage(fileName: '24_image_1513172030916_M2L1_Q14.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\24_image_1513172030916_M2L1_Q14.jpg',
                                  baseFileName: '24_image_1513172030916_M2L1_Q14'
                                  )
        def testImage12 = new QGImage(fileName: '25_image_1513172263597_M2L1_Q15.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\25_image_1513172263597_M2L1_Q15.jpg',
                                  baseFileName: '25_image_1513172263597_M2L1_Q15'
                                  )
        def testImage13 = new QGImage(fileName: '26_image_1513174601623_M2L1_Q16.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\26_image_1513174601623_M2L1_Q16.jpg',
                                  baseFileName: '26_image_1513174601623_M2L1_Q16'
                                  )
        def testImage14 = new QGImage(fileName: '28_image_1513174848725_M2L1_Q18.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\28_image_1513174848725_M2L1_Q18.jpg',
                                  baseFileName: '28_image_1513174848725_M2L1_Q18'
                                  )
        def testImage15 = new QGImage(fileName: '29_image_1513175024047_M2L1_Q19.jpg',
                                  filePath: 'C:\\Apache24\\htdocs\\images\\29_image_1513175024047_M2L1_Q19.jpg',
                                  baseFileName: '29_image_1513175024047_M2L1_Q19'
                                  )

        def questionGroup9 = QuestionGroup.findById(9)
        questionGroup9.addToqGImages(testImage1)
        questionGroup9.save (failOnError:true, flush:true)

        def questionGroup12 = QuestionGroup.findById(12)
        questionGroup12.addToqGImages(testImage2)
        questionGroup12.save (failOnError:true, flush:true)

        def questionGroup13 = QuestionGroup.findById(13)
        questionGroup13.addToqGImages(testImage3)
        questionGroup13.save (failOnError:true, flush:true)

        def questionGroup14 = QuestionGroup.findById(14)
        questionGroup14.addToqGImages(testImage4)
        questionGroup14.save (failOnError:true, flush:true)

        def questionGroup16 = QuestionGroup.findById(16)
        questionGroup16.addToqGImages(testImage5)
        questionGroup16.save (failOnError:true, flush:true)

        def questionGroup17 = QuestionGroup.findById(17)
        questionGroup17.addToqGImages(testImage6)
        questionGroup17.save (failOnError:true, flush:true)

        def questionGroup18 = QuestionGroup.findById(18)
        questionGroup18.addToqGImages(testImage7)
        questionGroup18.save (failOnError:true, flush:true)

        def questionGroup20 = QuestionGroup.findById(20)
        questionGroup20.addToqGImages(testImage8)
        questionGroup20.save (failOnError:true, flush:true)

        def questionGroup21 = QuestionGroup.findById(21)
        questionGroup21.addToqGImages(testImage9)
        questionGroup21.save (failOnError:true, flush:true)

        def questionGroup22 = QuestionGroup.findById(22)
        questionGroup22.addToqGImages(testImage10)
        questionGroup22.save (failOnError:true, flush:true)

        def questionGroup24 = QuestionGroup.findById(24)
        questionGroup24.addToqGImages(testImage11)
        questionGroup24.save (failOnError:true, flush:true)

        def questionGroup25 = QuestionGroup.findById(25)
        questionGroup25.addToqGImages(testImage12)
        questionGroup25.save (failOnError:true, flush:true)

        def questionGroup26 = QuestionGroup.findById(26)
        questionGroup26.addToqGImages(testImage13)
        questionGroup26.save (failOnError:true, flush:true)

        def questionGroup28 = QuestionGroup.findById(28)
        questionGroup28.addToqGImages(testImage14)
        questionGroup28.save (failOnError:true, flush:true)

        def questionGroup29 = QuestionGroup.findById(29)
        questionGroup29.addToqGImages(testImage15)
        questionGroup29.save (failOnError:true, flush:true)

        course2.save (failOnError:true, flush:true)
        course3.save (failOnError:true, flush:true)

        //create dev user test sessions

        def session1 = new UserTestSession(mark: 90,
                                  userId: 28, 
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test1
                                  )
        def session2 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test2
                                  )
        def session3 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test3
                                  )
        def session4 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test4
                                  )
        def session5 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test5
                                  )
        def session6 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test6
                                  )
        def session7 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test7
                                  )
        def session8 = new UserTestSession(mark: 100,
                                  userId: 28,
                                  userFirstName: 'admin',
                                  userLastName: 'user',
                                  test: test8
                                  )

        session1.save (failOnError:true, flush:true)
        session2.save (failOnError:true, flush:true)
        session3.save (failOnError:true, flush:true)
        session4.save (failOnError:true, flush:true)
        session5.save (failOnError:true, flush:true)
        session6.save (failOnError:true, flush:true)
        session7.save (failOnError:true, flush:true)
        session8.save (failOnError:true, flush:true)

        //create dev user course enrolments

        def enrol1 = new CourseEnrolment(userId: 4,
                                  course: course1
                                  )
        def enrol2 = new CourseEnrolment(userId: 3,
                                  course: course2
                                  )

        enrol1.save (failOnError:true, flush:true)
        enrol2.save (failOnError:true, flush:true)
    }

    def toLowerCaseXmlTags(xmlText) {
      xmlText.replaceAll(/<[^<>]+>/) { it.toLowerCase() }
    }

    def loadTestXML(String filename, Test test) {
        InputStream testFile = this.class.classLoader.getResourceAsStream(filename)
        String stringXML = testFile.text
        def testAsXML = new XmlSlurper().parseText(toLowerCaseXmlTags(stringXML))

        int countQuestionGroup = 1
        testAsXML.question.each { questionGroupNode ->

            QuestionGroup questionGroup = new QuestionGroup(owner:test,
                                                          orderNo:countQuestionGroup,
                                                          questionGroupText:questionGroupNode.content.text())
            test.addToQuestionGroups(questionGroup)
            
            int countQuestion = 1
            questionGroupNode.answer.choice.each {
                String curID = it.@id.text() 
                Question question = new Question(owner:questionGroup,
                                                orderNo:countQuestion,
                                                questionText:it.content.text())
                questionGroup.addToQuestions(question)
                int countChoice = 1
                it.option.each{
                    
                    String curChoiceString = it.text()                            
                    Boolean curChoiceStatus = false
             
                    questionGroupNode.outcome.each {
                        String correctID = it.@id.text()
                        it.content.each{
                            String correctAnswer = it.text().drop(4)
                            if ((curChoiceString == correctAnswer) && (correctID == curID))
                            {
                                curChoiceStatus = true
                            }
                        }
                    }

                    Choice choice = new Choice(owner:question,
                                              choiceOrder:countChoice,
                                              choiceStatus:curChoiceStatus,
                                              choiceText:curChoiceString)
                    question.addToChoices(choice)
                    countChoice++
                }
                countQuestion++
            }
            countQuestionGroup++
          }
    }
 
    def setupUser(username, forename, surname, password, roleName, groupName) {
        // create role
        def role = Role.findByAuthority(roleName) ?: new Role(authority: roleName).save(flush: true) 
        // create group
        def group = RoleGroup.findByName(groupName) ?: new RoleGroup(name: groupName).save(flush:true)

        // associate user role with user group
        def roleGroupRole = new RoleGroupRole()
        RoleGroupRole.create group, role

        // create user
        def user = User.findByUsername(username) ?: new User(
                username: username,
                firstName: forename,
                emailAddress: username,
                lastName: surname,
                password: password,
                enabled: true).save(flush: true)
        
        // associate  user with  group
        UserRoleGroup.create user, group
        
        return user
    }

    def destroy = {
    }
}
