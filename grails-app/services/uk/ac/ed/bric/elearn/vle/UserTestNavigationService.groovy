package uk.ac.ed.bric.elearn.vle

import grails.transaction.Transactional

@Transactional
class UserTestNavigationService {

    def getCurrentQuestionGroup(userTestSession) {

        if(userTestSession.answers)
        {
        	def latestAnswer = userTestSession.answers.max{it.owner.owner.orderNo}	
        	return latestAnswer.owner.owner
        }
        else
        {
        	return userTestSession.test.firstQuestionGroup()
        }
    }

    def getNextQuestionGroup(userTestSession, questionGroup, test) {

        int newOrderNo = questionGroup.orderNo+1
        return QuestionGroup.findWhere( orderNo:newOrderNo, owner:test ) 
    }

    def getPrecedingQuestionGroup(userTestSession, questionGroup, test) {

        int newOrderNo = questionGroup.orderNo-1
        return QuestionGroup.findWhere( orderNo:newOrderNo, owner:test ) 
    }

}