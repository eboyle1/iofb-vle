package uk.ac.ed.bric.elearn.vle

import grails.transaction.Transactional

import uk.ac.ed.bric.elearn.User

class CourseService {

	def findEnrolmentsByUser(user) {
		StringBuilder query = new StringBuilder()
        query << " FROM CourseEnrolment "
        query << " WHERE user_id = :userId"
        def params = ['userId':user.id]
        CourseEnrolment.findAll(query.toString(), params)
	}

	def findCourseEnrolment(course, user) {
		StringBuilder query = new StringBuilder()
        query << " FROM CourseEnrolment "
        query << " WHERE user_id = :userId "
        query << " AND course_id = :courseId "
        Map params = ['userId':user.id, 'courseId':course.id]
        CourseEnrolment.find(query.toString(), params)	
	}
}