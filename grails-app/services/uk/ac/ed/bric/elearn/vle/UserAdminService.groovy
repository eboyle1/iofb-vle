package uk.ac.ed.bric.elearn.vle

import grails.transaction.Transactional

import uk.ac.ed.bric.elearn.User

@Transactional
class UserAdminService {

    def createUserAdmin(user, flag)
    {
    	if (!UserAdmin.findByUserId(user.id))
    	{
        	UserAdmin newUserAdmin = new UserAdmin( user:user, testsSuccess:flag )
        	newUserAdmin.save flush:true
        	return newUserAdmin
        } 
    }
}