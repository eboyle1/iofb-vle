package uk.ac.ed.bric.elearn.vle

import grails.transaction.Transactional

import uk.ac.ed.bric.elearn.User

@Transactional
class UserTestSessionService {

	def getTestSession(user, test) {
        //Map params = [ 'user':user, 'test':test ]
        //UserTestSession userTestSession = findUserTestSession(params)
        //if(userTestSession) {
        //    return userTestSession
        //}
        //createUserTestSession(user, test)
    }

    def createUserTestSession(user, test) {
        UserTestSession newUserTestSession = new UserTestSession( user:user, test:test )
        newUserTestSession.save flush:true
        return newUserTestSession  
    }
}