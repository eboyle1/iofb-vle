package uk.ac.ed.bric.elearn.vle

import grails.transaction.Transactional
import org.apache.commons.lang.StringUtils
import java.util.zip.*
import java.nio.file.*
import java.nio.file.attribute.*

@Transactional
class FileService {

    def grailsApplication

    private void saveFile(FileCommand cmd, Lecture lecture, String name) {
        saveLectureFile(cmd.uploadedFile, lecture, name)
    }

    private void saveFile2(ContentFileCommand cmd, Lecture lecture, String name, int type) {
        saveContentFile(cmd.uploadedContentFile, lecture, name, type)
    }

    private void saveFile3(ImageCommand cmd, QuestionGroup questionGroup) {
        saveQuestionGroupImage(cmd.uploadedQuestionGroupImage, questionGroup)
    }

    private void saveLectureFile(uploadedFile, Lecture lecture, String name) {
        long timestamp = new Date().getTime() //to record upload time and ensure uniqueness of file on server 
        if(StringUtils.isNotBlank(uploadedFile.originalFilename)) {
            File targetFile = saveLectureFileStore(uploadedFile,
                "${grailsApplication.config.file.upload.dir}",
                "${lecture.id}_main_lecture_${timestamp}", lecture)
            linkFile(targetFile, lecture, name)
            String newFileName = "${lecture.id}_main_lecture_${timestamp}_${uploadedFile.originalFilename}"
            unzipFile (newFileName, lecture)
        }
    }

    private void saveContentFile(uploadedContentFile, Lecture lecture, String name, int type) {
        long timestamp = new Date().getTime() //to record upload time and ensure uniqueness of file on server 
        if(StringUtils.isNotBlank(uploadedContentFile.originalFilename)) {
            File targetFile = saveContentFileStore(uploadedContentFile,
                "${grailsApplication.config.file.upload.dir}/${lecture.id}",
                "${lecture.id}_content_${timestamp}", lecture)
            linkContentFile(targetFile, lecture, name, type)
        }
    }

    private void saveQuestionGroupImage(uploadedQuestionGroupImage, QuestionGroup questionGroup) {
        long timestamp = new Date().getTime() //to record upload time and ensure uniqueness of file on server 
        if(StringUtils.isNotBlank(uploadedQuestionGroupImage.originalFilename)) {
            File targetFile = saveQuestionGroupImageStore(uploadedQuestionGroupImage,
                "${grailsApplication.config.image.upload.dir}",
                "${questionGroup.id}_image_${timestamp}", questionGroup)
            linkQuestionGroupImage(targetFile, questionGroup)
        }
    }

    private File saveLectureFileStore(file, dir, id, Lecture lecture) {
        String filename = "${dir}/${id}_${file.originalFilename}"
        log.debug "filename: ${filename}"
        File targetFile = new File(filename)
        if(targetFile.exists()) {
            throw new IOException("${filename} already exists")
        }
        file.transferTo(targetFile)
        return targetFile
    }

    private File saveContentFileStore(file, dir, id, Lecture lecture) {
        String filename = "${dir}/${id}_${file.originalFilename}"
        log.debug "filename: ${filename}"
        File targetFile = new File(filename)
        if(targetFile.exists()) {
            throw new IOException("${filename} already exists")
        }
        file.transferTo(targetFile)
        //need to set the group of the uploaded file so that httpd on the production VM can read it
        //note this is specific to Linux
        Path p = targetFile.toPath()
        String str_group = "www-data"
        FileSystem fileSystem = p.getFileSystem()
        UserPrincipalLookupService lookupService = fileSystem.getUserPrincipalLookupService()
        GroupPrincipal group = lookupService.lookupPrincipalByGroupName(str_group)
        Files.setAttribute(p, "posix:group", group, LinkOption.NOFOLLOW_LINKS)
        return targetFile
    }

    private File saveQuestionGroupImageStore(file, dir, id, QuestionGroup questionGroup) {
        String filename = "${dir}/${id}_${file.originalFilename}"
        log.debug "filename: ${filename}"
        File targetFile = new File(filename)
        if(targetFile.exists()) {
            throw new IOException("${filename} already exists")
        }
        file.transferTo(targetFile)
        //need to set the group of the uploaded file so that httpd on the production VM can read it
        //note this is specific to Linux
        Path p = targetFile.toPath()
        String str_group = "www-data"
        FileSystem fileSystem = p.getFileSystem()
        UserPrincipalLookupService lookupService = fileSystem.getUserPrincipalLookupService()
        GroupPrincipal group = lookupService.lookupPrincipalByGroupName(str_group)
        Files.setAttribute(p, "posix:group", group, LinkOption.NOFOLLOW_LINKS)
        return targetFile
    }

    private void linkFile(File targetFile, Lecture lecture, String name) {
        String baseFileName = targetFile.name.substring(0, targetFile.name.lastIndexOf('.'))
        LectureFile lectureFile = new LectureFile(lecture:lecture, filePath:targetFile.absolutePath, name:name, fileName:targetFile.name, baseFileName:baseFileName)
        lectureFile.save flush:true
        lecture.setLectureFile(lectureFile)
        lecture.save flush:true 
    }

    private void linkContentFile(File targetFile, Lecture lecture, String name, int type) {
        String baseFileName = targetFile.name.substring(0, targetFile.name.lastIndexOf('.'))
        ContentFile contentFile = new ContentFile(lecture:lecture, filePath:targetFile.absolutePath, name:name, fileType:type, fileName:targetFile.name, baseFileName:baseFileName)
        contentFile.save flush:true
        lecture.addToContentFiles(contentFile)
        lecture.save flush:true 
    }

    private void linkQuestionGroupImage(File targetFile, QuestionGroup questionGroup) {
        String baseFileName = targetFile.name.substring(0, targetFile.name.lastIndexOf('.'))
        QGImage qGImage = new QGImage(questionGroup:questionGroup, filePath:targetFile.absolutePath, fileName:targetFile.name, baseFileName:baseFileName)
        qGImage.save flush:true
        questionGroup.addToqGImages(qGImage)
        questionGroup.save flush:true 
    }

    private void updateFileName (Lecture lecture, String newFileName) {
        def lectureFile = lecture.lectureFile
        lectureFile.setName (newFileName) 
        lectureFile.save flush:true 
    }

    private void updateContentFileName (Lecture lecture, String newFileName) {
        def contentFile = lecture.contentFile
        contentFile.setName (newFileName) 
        contentFile.save flush:true 
    }

    private void deleteFile(Lecture lecture) {
        def lectureFile = lecture.lectureFile
        lecture.lectureFile = null
        lectureFile.delete(flush:true)
    }

    private void deleteContentFile(Lecture lecture, ContentFile contentFile) {
        lecture.removeFromContentFiles(contentFile)
        contentFile.delete(flush:true)
    }

   private void deleteQuestionGroupImage(QuestionGroup questionGroup, QGImage qGImage) {
        questionGroup.removeFromqGImages(qGImage)
        qGImage.delete(flush:true)
    }

    private void unzipFile(String zipFileName, Lecture lecture) {

        String zipFilePathName = "${grailsApplication.config.file.upload.dir}/${zipFileName}"
        String zipFileNameBase = zipFileName.substring(0, zipFileName.lastIndexOf('.'))
        def outputDir = "${grailsApplication.config.file.upload.dir}/${lecture.id}/${zipFileNameBase}"

        def zip = new ZipFile(new File(zipFilePathName))
        def buf = new byte[4096]; //optimum value may be dependent on local environment
        zip.entries().each{  
            if (!it.isDirectory()){
                def fOut = new File(outputDir+ File.separator + it.name)

                new File(fOut.parent).mkdirs() //create output dir if not exists
                
                def len = zip.getInputStream(it); // get the input stream
                def fos = new FileOutputStream(fOut);

                int r;
                while ((r = len.read(buf)) != -1) {
                    fos.write(buf, 0, r);
                }

                len.close()
                fos.close()     
            }
        }
        zip.close()

    }
}
