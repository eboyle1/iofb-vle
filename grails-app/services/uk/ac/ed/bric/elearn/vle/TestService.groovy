package uk.ac.ed.bric.elearn.vle

import grails.transaction.Transactional

@Transactional
class TestService {
	def listChoicesForQuestion(Question question) {
    	def choiceListQuery = "FROM Choice WHERE owner = :owner ORDER BY choiceOrder"
        def choiceList = Choice.findAll(choiceListQuery,[owner:question])
		choiceList ?: []
    }
}