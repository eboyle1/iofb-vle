<QML>

<QUESTION ID="3584226943812243" DESCRIPTION="IOFB_M2L1_Q1_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Regarding the bones which comprise the orbit, answer the following TRUE or FALSE:</P></CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. The orbit is bordered by seven bones.</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. The orbital medial wall is primarily the cribriform plate.</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. The orbital inferior wall is primarily the zygoma.</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. The orbital superior wall is primarily the frontal bone.</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. The orbital posterior wall is primarily the sphenoid bone.</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">B = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">C = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="7223405793761541" DESCRIPTION="IOFB_M2L1_Q2_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the structures labelled in this orbital radiograph:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Infra-orbital foramen</OPTION>
      <OPTION>Inominate line</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Infra-orbital foramen</OPTION>
      <OPTION>Inominate line</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Infra-orbital foramen</OPTION>
      <OPTION>Inominate line</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Infra-orbital foramen</OPTION>
      <OPTION>Inominate line</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Infra-orbital foramen</OPTION>
      <OPTION>Inominate line</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Inominate line"</CONDITION>
    <CONTENT TYPE="text/html">A = Inominate line</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Zygoma"</CONDITION>
    <CONTENT TYPE="text/html">B = Zygoma</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Lamina papyracea"</CONDITION>
    <CONTENT TYPE="text/html">C = Lamina papyracea</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Nasal bone"</CONDITION>
    <CONTENT TYPE="text/html">D = Nasal bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Nasal septum"</CONDITION>
    <CONTENT TYPE="text/html">E = Nasal septum</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="1761521852351684" DESCRIPTION="IOFB_M2L1_Q3_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the labelled structures in this coronal CT section through the orbits:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Superior rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">A = Superior rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Optic nerve (CN II)"</CONDITION>
    <CONTENT TYPE="text/html">B = Optic nerve (CN II)</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Medial rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">C = Medial rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Inferior rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">D = Inferior rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Lateral rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">E = Lateral rectus muscle</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="4741120583598690" DESCRIPTION="IOFB_M2L1_Q4_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the labelled structures in this axial MR section through the orbits:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Basilar artery</OPTION>
      <OPTION>Carotid artery</OPTION>
      <OPTION>Ethmoid air cells</OPTION>
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic chiasm</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Basilar artery</OPTION>
      <OPTION>Carotid artery</OPTION>
      <OPTION>Ethmoid air cells</OPTION>
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic chiasm</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Basilar artery</OPTION>
      <OPTION>Carotid artery</OPTION>
      <OPTION>Ethmoid air cells</OPTION>
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic chiasm</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Basilar artery</OPTION>
      <OPTION>Carotid artery</OPTION>
      <OPTION>Ethmoid air cells</OPTION>
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic chiasm</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Basilar artery</OPTION>
      <OPTION>Carotid artery</OPTION>
      <OPTION>Ethmoid air cells</OPTION>
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic chiasm</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Lateral rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">A = Lateral rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Extra conal fat"</CONDITION>
    <CONTENT TYPE="text/html">B = Extra conal fat</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Ethmoid air cells"</CONDITION>
    <CONTENT TYPE="text/html">C = Ethmoid air cells</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Basilar artery"</CONDITION>
    <CONTENT TYPE="text/html">D = Basilar artery</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Carotid artery"</CONDITION>
    <CONTENT TYPE="text/html">E = Carotid artery</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="3005363694252084" DESCRIPTION="IOFB_M2L1_Q5_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Match the following:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Parietal bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">A. Infra orbital foramen</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Parietal bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">B. Foramen rotundum</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Parietal bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">C. Lamina papyracea</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Parietal bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">D. Optic foramen</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Parietal bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">E. Cribriform plate</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Maxillary bone"</CONDITION>
    <CONTENT TYPE="text/html">A = Maxillary bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Sphenoid bone"</CONDITION>
    <CONTENT TYPE="text/html">B = Sphenoid bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Ethmoid bone"</CONDITION>
    <CONTENT TYPE="text/html">C = Ethmoid bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Sphenoid bone"</CONDITION>
    <CONTENT TYPE="text/html">D = Sphenoid bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Ethmoid bone"</CONDITION>
    <CONTENT TYPE="text/html">E = Ethmoid bone</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
