

grails.plugin.springsecurity.userLookup.userDomainClassName = 'uk.ac.ed.bric.elearn.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'uk.ac.ed.bric.elearn.UserRole'
grails.plugin.springsecurity.authority.className = 'uk.ac.ed.bric.elearn.Role'
grails.plugin.springsecurity.authority.groupAuthorityNameField = 'authorities'
grails.plugin.springsecurity.useRoleGroups = true

grails.logging.jul.usebridge = true
grails.plugin.springsecurity.logout.postOnly = false

grails.plugin.springsecurity.useBasicAuth = true
grails.plugin.springsecurity.basic.realmName = "Edinburgh Imaging Academy"

grails.plugin.springsecurity.providerNames = ['elearnAuthenticationProvider']

grails.plugin.springsecurity.filterChain.filterNames = [
   'securityContextPersistenceFilter', 'logoutFilter', 'basicAuthenticationFilter',
   'elearnSSOAuthenticationFilter', 'anonymousAuthenticationFilter',
   'exceptionTranslationFilter', 'filterInvocationInterceptor'
]

grails.plugin.springsecurity.debug.useFilter = true
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
	[pattern: '/',               access: ['permitAll']],
	[pattern: '/error',          access: ['permitAll']],
	[pattern: '/index',          access: ['permitAll']],
	[pattern: '/index.gsp',      access: ['permitAll']],
	[pattern: '/shutdown',       access: ['permitAll']],
	[pattern: '/assets/**',      access: ['permitAll']],
	[pattern: '/**/js/**',       access: ['permitAll']],
	[pattern: '/**/css/**',      access: ['permitAll']],
	[pattern: '/**/images/**',   access: ['permitAll']],
	[pattern: '/static/**',   	 access: ['permitAll']],
	[pattern: '/**/favicon.ico', access: ['permitAll']],
]

grails.plugin.springsecurity.filterChain.chainMap = [
	[pattern: '/assets/**',      filters: 'none'],
	[pattern: '/**/js/**',       filters: 'none'],
	[pattern: '/**/css/**',      filters: 'none'],
	[pattern: '/**/images/**',   filters: 'none'],
	[pattern: '/**/favicon.ico', filters: 'none'],
	[pattern: '/rest/**', filters: 'basicAuthenticationFilter'],
	[pattern: '/**', filters: 'JOINED_FILTERS,-basicAuthenticationFilter'],
	
]




