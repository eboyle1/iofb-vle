<QML>

<QUESTION ID="9110906925062032" DESCRIPTION="IOFB_M2L1_Q11_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Identify the labelled structures in this coronal CT section through the orbits:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Crista galli</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Crista galli</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Crista galli</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Crista galli</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Cribriform plate</OPTION>
      <OPTION>Crista galli</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lamina papyracea</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Lamina papyracea"</CONDITION>
    <CONTENT TYPE="text/html">A = Lamina papyracea</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Crista galli"</CONDITION>
    <CONTENT TYPE="text/html">B = Crista galli</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Cribriform plate"</CONDITION>
    <CONTENT TYPE="text/html">C = Cribriform plate</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Maxillary bone"</CONDITION>
    <CONTENT TYPE="text/html">D = Maxillary bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Frontal bone"</CONDITION>
    <CONTENT TYPE="text/html">E = Frontal bone</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="0217713732265157" DESCRIPTION="IOFB_M2L1_Q12_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the labelled structures in this coronal MR section through the orbits:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Mastoid air cells</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Lacrimal gland</OPTION>
      <OPTION>Nasal fossa</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Mastoid air cells</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Lacrimal gland</OPTION>
      <OPTION>Nasal fossa</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Mastoid air cells</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Lacrimal gland</OPTION>
      <OPTION>Nasal fossa</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Mastoid air cells</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Lacrimal gland</OPTION>
      <OPTION>Nasal fossa</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Mastoid air cells</OPTION>
      <OPTION>Maxillary sinus</OPTION>
      <OPTION>Lacrimal gland</OPTION>
      <OPTION>Nasal fossa</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Nasal septum</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Globe"</CONDITION>
    <CONTENT TYPE="text/html">A = Globe</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Frontal sinus"</CONDITION>
    <CONTENT TYPE="text/html">B = Frontal sinus</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Ethmoid air cell"</CONDITION>
    <CONTENT TYPE="text/html">C = Ethmoid air cell</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Nasal fossa"</CONDITION>
    <CONTENT TYPE="text/html">D = Nasal fossa</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Maxillary sinus"</CONDITION>
    <CONTENT TYPE="text/html">E = Maxillary sinus</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="8797882036744442" DESCRIPTION="IOFB_M2L1_Q13_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Answer the following TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. The foramen ovale lies in the sphenoid bone</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. The foramen rotundum lies in the sphenoid bone</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. The vidian canal lies in the maxillary bone</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. The infra orbital canal lies in the maxillary bone</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. The inferior orbital fissure lies in the maxillary bone</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">C = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">E = FALSE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="7393752258597074" DESCRIPTION="IOFB_M2L1_Q14_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the bones which give rise to the labelled linear shadows:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Ethmoid bone</OPTION>
      <OPTION>Frontal bone</OPTION>
      <OPTION>Lacrimal bone</OPTION>
      <OPTION>Maxillary bone</OPTION>
      <OPTION>Nasal bone</OPTION>
      <OPTION>Occipital bone</OPTION>
      <OPTION>Palatine bone</OPTION>
      <OPTION>Sphenoid bone</OPTION>
      <OPTION>Temporal bone</OPTION>
      <OPTION>Zygoma</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Frontal bone"</CONDITION>
    <CONTENT TYPE="text/html">A = Frontal bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Sphenoid bone"</CONDITION>
    <CONTENT TYPE="text/html">B = Sphenoid bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Zygoma"</CONDITION>
    <CONTENT TYPE="text/html">C = Zygoma</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Maxillary bone"</CONDITION>
    <CONTENT TYPE="text/html">D = Maxillary bone</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Ethmoid bone"</CONDITION>
    <CONTENT TYPE="text/html">E = Ethmoid bone</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="8578021013502182" DESCRIPTION="IOFB_M2L1_Q15_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the labelled structures in this coronal CT section through the orbits:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Anterior clinoid process</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Foramen ovale</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Inferior orbital fissure</OPTION>
      <OPTION>Optic foramen</OPTION>
      <OPTION>Posterior clinoid process</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior orbital fissure</OPTION>
      <OPTION>Vidian canal</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Anterior clinoid process</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Foramen ovale</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Inferior orbital fissure</OPTION>
      <OPTION>Optic foramen</OPTION>
      <OPTION>Posterior clinoid process</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior orbital fissure</OPTION>
      <OPTION>Vidian canal</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Anterior clinoid process</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Foramen ovale</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Inferior orbital fissure</OPTION>
      <OPTION>Optic foramen</OPTION>
      <OPTION>Posterior clinoid process</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior orbital fissure</OPTION>
      <OPTION>Vidian canal</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Anterior clinoid process</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Foramen ovale</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Inferior orbital fissure</OPTION>
      <OPTION>Optic foramen</OPTION>
      <OPTION>Posterior clinoid process</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior orbital fissure</OPTION>
      <OPTION>Vidian canal</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Anterior clinoid process</OPTION>
      <OPTION>Foramen rotundum</OPTION>
      <OPTION>Foramen ovale</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Inferior orbital fissure</OPTION>
      <OPTION>Optic foramen</OPTION>
      <OPTION>Posterior clinoid process</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior orbital fissure</OPTION>
      <OPTION>Vidian canal</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Foramen rotundum"</CONDITION>
    <CONTENT TYPE="text/html">A = Foramen rotundum</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Anterior clinoid process"</CONDITION>
    <CONTENT TYPE="text/html">B = Anterior clinoid process</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Optic foramen"</CONDITION>
    <CONTENT TYPE="text/html">C = Optic foramen</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Vidian canal"</CONDITION>
    <CONTENT TYPE="text/html">D = Vidian canal</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Sphenoid sinus"</CONDITION>
    <CONTENT TYPE="text/html">E = Sphenoid sinus</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
