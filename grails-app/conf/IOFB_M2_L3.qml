<QML>

<QUESTION ID="4170434418657179" DESCRIPTION="IOFB_M2L2_Q6_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L2 IRMER" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When considering documentation, training and enforcement, make the best match:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Clinical audit</OPTION>
      <OPTION>Diagnostic reference levels</OPTION>
      <OPTION>Medical physics</OPTION>
      <OPTION>Standard operating procedures</OPTION>
      <OPTION>Training</OPTION>
      <CONTENT TYPE="text/html">A. Documents ongoing radiography competency</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Clinical audit</OPTION>
      <OPTION>Diagnostic reference levels</OPTION>
      <OPTION>Medical physics</OPTION>
      <OPTION>Standard operating procedures</OPTION>
      <OPTION>Training</OPTION>
      <CONTENT TYPE="text/html">B. Entitles radiographer to obtain radiographs</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Clinical audit</OPTION>
      <OPTION>Diagnostic reference levels</OPTION>
      <OPTION>Medical physics</OPTION>
      <OPTION>Standard operating procedures</OPTION>
      <OPTION>Training</OPTION>
      <CONTENT TYPE="text/html">C. Sets boundaries on scope of entitlement</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Clinical audit</OPTION>
      <OPTION>Diagnostic reference levels</OPTION>
      <OPTION>Medical physics</OPTION>
      <OPTION>Standard operating procedures</OPTION>
      <OPTION>Training</OPTION>
      <CONTENT TYPE="text/html">D. Record which allows monitoring of patient radiation</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Clinical audit</OPTION>
      <OPTION>Diagnostic reference levels</OPTION>
      <OPTION>Medical physics</OPTION>
      <OPTION>Standard operating procedures</OPTION>
      <OPTION>Training</OPTION>
      <CONTENT TYPE="text/html">E. Admissable as evidence of due diligence</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Clinical audit"</CONDITION>
    <CONTENT TYPE="text/html">A = Clinical audit</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Training"</CONDITION>
    <CONTENT TYPE="text/html">B = Training</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Training"</CONDITION>
    <CONTENT TYPE="text/html">C = Training</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Clinical audit"</CONDITION>
    <CONTENT TYPE="text/html">D = Clinical audit</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Training"</CONDITION>
    <CONTENT TYPE="text/html">E = Training</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="2741321358829565" DESCRIPTION="IOFB_M2L3_Q1_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L3 Policies" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Regarding policies and protocols, make the best match for the following:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>Policy</OPTION>
      <OPTION>Protocol</OPTION>
      <CONTENT TYPE="text/html">A. Definition of who will do what</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Policy</OPTION>
      <OPTION>Protocol</OPTION>
      <CONTENT TYPE="text/html">B. Statement of intention</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Policy</OPTION>
      <OPTION>Protocol</OPTION>
      <CONTENT TYPE="text/html">C. Guidance on decision making</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Policy</OPTION>
      <OPTION>Protocol</OPTION>
      <CONTENT TYPE="text/html">D. Listing of forms to fill out</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Policy</OPTION>
      <OPTION>Protocol</OPTION>
      <CONTENT TYPE="text/html">E. Step by step instruction</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Protocol"</CONDITION>
    <CONTENT TYPE="text/html">A = Protocol</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Policy"</CONDITION>
    <CONTENT TYPE="text/html">B = Policy</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Policy"</CONDITION>
    <CONTENT TYPE="text/html">C = Policy</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Protocol"</CONDITION>
    <CONTENT TYPE="text/html">D = Protocol</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Protocol"</CONDITION>
    <CONTENT TYPE="text/html">E = Protocol</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="6102958452608647" DESCRIPTION="IOFB_M2L3_Q2_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L3 Policies" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When setting up a protocol for radiographer led reporting of orbital radiographs for metallic IOFB detection, what is the recommended optimum order for the following steps to be carried out?</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">A. Set review date</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">B. Outline relevant training</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">C. Engage with senior management</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">D. Identify risks</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">E. Review feedback</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "5"</CONDITION>
    <CONTENT TYPE="text/html">A = 5</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "1"</CONDITION>
    <CONTENT TYPE="text/html">B = 1</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "3"</CONDITION>
    <CONTENT TYPE="text/html">C = 3</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "2"</CONDITION>
    <CONTENT TYPE="text/html">D = 2</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "4"</CONDITION>
    <CONTENT TYPE="text/html">E = 4</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="9527884812228065" DESCRIPTION="IOFB_M2L3_Q3_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L3 Policies" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Engagement of senior managers in setting up a radiographer led orbital reporting programme for metallic IOFB detection is important for which of the following reasons? Answer using TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Approval of role extension</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Support additional training</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Record which radiographers are appropriately trained</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Feedback on protocol development</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Examination of competencies</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">E = FALSE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="3653427240239649" DESCRIPTION="IOFB_M2L3_Q4_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L3 Policies" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">A protocol for radiographers reporting orbital radiographs for metallic IOFBs has been provided as an example from Lothian. Which of the following in this document are included in the definition of an appropriately trained radiographer? Answer using TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Hold a Diploma or BSc in diagnostic radiography</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Designated as a non-specialist radiographer</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Have valid professional registration</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Experienced clinical radiographer trained in MR</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Have completed twenty (20) supervised orbital radiograph assessments</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">B = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">E = FALSE</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
