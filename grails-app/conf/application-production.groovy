vle.url="http://129.215.119.67/vle"
elearn.apps.home = "http://129.215.119.67"

file.upload.dir="/var/www/html/lectureFiles"
image.upload.dir="/var/www/html/images"

external.webserver="http://129.215.119.67"

sso.client.home = "http://129.215.119.67/vle/login/sso"
grails.plugin.springsecurity.sso.authenticationPoint = 'http://129.215.119.67/ssoserver/login/auth'

grails {
   mail {
    host = "smtp.gmail.com"
    port = "465"
    username = "edward.boyle7@gmail.com"
    password = ""
    props = ["mail.smtp.auth":"true",              
              "mail.smtp.socketFactory.port":"465",
              "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
              "mail.smtp.socketFactory.fallback":"false"]
   }
}

//vleTests.email.to = 'Imaging.Academy@ed.ac.uk'
vleTests.email.to = 'edward.boyle@ed.ac.uk'
vleTests.email.from = 'Imaging.Academy@ed.ac.uk'
vleTests.email.subject = 'Notification of IOFB review course successful lecture test suite completion'
vleTests.email.body = '''\
<p>This student on the IOFB review course has now achieved 100% in all of the lecture tests:</p>
<p>$lastname, $firstname</p> 
'''