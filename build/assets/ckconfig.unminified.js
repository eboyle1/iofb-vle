

CKEDITOR.editorConfig = function( config )
{
     
   config.toolbar_custom = [
       ['Styles','Format','Font','FontSize','TextColor','BGColor','Maximize','Image'],
       ['Bold','Italic','Underline','Strike','-','Subscript','Superscript','-','SpellChecker','Scayt'],
       ['Link'],
       ['Table','HorizontalRule'],
       ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
   ]
};
