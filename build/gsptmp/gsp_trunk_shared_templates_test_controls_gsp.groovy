import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_shared_templates_test_controls_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/shared-templates/_test_controls.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
invokeTag('display','f',2,['bean':("test"),'property':("name")],-1)
printHtmlPart(1)
createTagBody(1, {->
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('link','g',9,['controller':("userTest"),'params':([testId:test.id]),'action':("beginTest"),'class':("btn btn-nav pull-right")],2)
printHtmlPart(4)
createClosureForHtmlPart(5, 2)
invokeTag('link','g',11,['controller':("question"),'action':("create"),'params':([testId:test.id]),'class':("btn btn-primary pull-right")],2)
printHtmlPart(4)
createClosureForHtmlPart(6, 2)
invokeTag('link','g',13,['controller':("questionGroup"),'action':("create"),'params':([testId:test.id,owner:test.id]),'class':("btn btn-primary pull-right")],2)
printHtmlPart(4)
invokeTag('link','p',15,['controller':("test"),'action':("edit"),'resource':(test),'class':("btn btn-primary pull-right"),'actionType':("edit test")],-1)
printHtmlPart(7)
invokeTag('link','p',19,['controller':("lecture"),'action':("listTests"),'id':(test.lecture.id),'class':("btn btn-default pull-right"),'actionType':("cancel")],-1)
printHtmlPart(8)
})
invokeTag('form','g',23,['url':([resource:test, action:'delete']),'method':("DELETE"),'class':("form-horizontal")],1)
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636587L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
