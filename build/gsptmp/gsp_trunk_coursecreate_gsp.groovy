import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_coursecreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/course/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'course.label', default: 'Course'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.create.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(1)
invokeTag('resources','ckeditor',7,[:],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',11,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
invokeTag('message','g',14,['code':("default.create.label"),'args':([entityName])],-1)
printHtmlPart(6)
createTagBody(2, {->
printHtmlPart(7)
createTagBody(3, {->
printHtmlPart(8)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(9)
expressionOut.print(error.field)
printHtmlPart(10)
}
printHtmlPart(11)
invokeTag('message','g',22,['error':(error)],-1)
printHtmlPart(12)
})
invokeTag('eachError','g',23,['bean':(course),'var':("error")],3)
printHtmlPart(13)
})
invokeTag('hasErrors','g',25,['bean':(course)],2)
printHtmlPart(14)
createTagBody(2, {->
printHtmlPart(15)
invokeTag('hiddenField','g',30,['name':("version"),'value':(course?.version)],-1)
printHtmlPart(16)
invokeTag('render','g',33,['template':("form_fields_create")],-1)
printHtmlPart(17)
expressionOut.print(message(code: 'default.button.create.label', default: 'Create'))
printHtmlPart(18)
createClosureForHtmlPart(19, 3)
invokeTag('link','g',37,['controller':("course"),'action':("index"),'class':("btn btn-default pull-right")],3)
printHtmlPart(20)
})
invokeTag('form','g',39,['url':([resource:course, action:'save']),'class':("form-horizontal"),'method':("POST")],2)
printHtmlPart(21)
})
invokeTag('captureBody','sitemesh',43,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636330L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
