import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_layouts_toolbar_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/layouts/_toolbar.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
createClosureForHtmlPart(2, 2)
invokeTag('link','g',12,['controller':("course"),'action':("index")],2)
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',14,['controller':("course"),'action':("create")],2)
printHtmlPart(5)
createClosureForHtmlPart(6, 2)
invokeTag('link','g',23,['controller':("test"),'action':("userReport1"),'params':([sort:'userId', order:'asc'])],2)
printHtmlPart(7)
createClosureForHtmlPart(8, 2)
invokeTag('link','g',24,['controller':("test"),'action':("userReport2"),'params':([sort:'userId', order:'asc'])],2)
printHtmlPart(9)
})
invokeTag('access','sec',24,['expression':("hasRole('ROLE_ADMIN')")],1)
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636488L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
