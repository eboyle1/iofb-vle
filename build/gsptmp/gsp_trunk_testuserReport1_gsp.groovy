import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_testuserReport1_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/test/userReport1.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
invokeTag('sortableColumn','g',17,['property':("userId"),'title':("User ID")],-1)
printHtmlPart(4)
invokeTag('sortableColumn','g',18,['property':("lastName"),'title':("Name")],-1)
printHtmlPart(4)
invokeTag('sortableColumn','g',19,['property':("testName"),'title':("Test")],-1)
printHtmlPart(4)
invokeTag('sortableColumn','g',20,['property':("mark"),'title':("Highest mark (%)")],-1)
printHtmlPart(5)
loop:{
int i = 0
for( userTestSessionInstance in (userTestSessions) ) {
printHtmlPart(6)
expressionOut.print(userTestSessionInstance.userId)
printHtmlPart(7)
expressionOut.print(userTestSessionInstance.firstName)
printHtmlPart(8)
expressionOut.print(userTestSessionInstance.lastName)
printHtmlPart(9)
expressionOut.print(userTestSessionInstance.testName)
printHtmlPart(10)
expressionOut.print(userTestSessionInstance.lectureName)
printHtmlPart(11)
expressionOut.print(userTestSessionInstance.moduleName)
printHtmlPart(7)
expressionOut.print(userTestSessionInstance.mark)
printHtmlPart(12)
i++
}
}
printHtmlPart(13)
})
invokeTag('captureBody','sitemesh',35,[:],1)
printHtmlPart(14)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636096L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
