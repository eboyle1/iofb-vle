import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lectureaddContentFile_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/addContentFile.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.addFile.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',27,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
createClosureForHtmlPart(5, 2)
invokeTag('link','g',30,['controller':("course"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('display','f',30,['bean':("lecture"),'property':("module.course.name")],-1)
})
invokeTag('link','g',30,['controller':("course"),'action':("show"),'id':(lecture.module.course.id)],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('display','f',30,['bean':("lecture"),'property':("module.name")],-1)
})
invokeTag('link','g',30,['controller':("module"),'action':("show"),'id':(lecture.module.id)],2)
printHtmlPart(7)
invokeTag('message','g',33,['code':("default.addContentFiles.label"),'args':([entityName])],-1)
printHtmlPart(8)
createTagBody(2, {->
printHtmlPart(9)
createTagBody(3, {->
printHtmlPart(10)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(11)
expressionOut.print(error.field)
printHtmlPart(12)
}
printHtmlPart(13)
invokeTag('message','g',41,['error':(error)],-1)
printHtmlPart(14)
})
invokeTag('eachError','g',42,['bean':(contentFileCommand),'var':("error")],3)
printHtmlPart(15)
})
invokeTag('hasErrors','g',44,['bean':(contentFileCommand)],2)
printHtmlPart(16)
expressionOut.print(lecture.name)
printHtmlPart(17)
createTagBody(2, {->
printHtmlPart(18)
invokeTag('hiddenField','g',59,['name':("lectureId"),'value':(lecture.id)],-1)
printHtmlPart(19)
expressionOut.print(message(code: 'lecture.lectureFile.upload.label', default: 'Upload'))
printHtmlPart(20)
createClosureForHtmlPart(21, 3)
invokeTag('link','g',77,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default pull-right")],3)
printHtmlPart(22)
})
invokeTag('uploadForm','g',80,['name':("upload"),'action':("uploadContentFile"),'method':("post"),'controller':("Lecture")],2)
printHtmlPart(23)
invokeTag('render','g',82,['template':("content_files"),'bean':(lecture)],-1)
printHtmlPart(24)
})
invokeTag('captureBody','sitemesh',89,[:],1)
printHtmlPart(25)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636939L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
