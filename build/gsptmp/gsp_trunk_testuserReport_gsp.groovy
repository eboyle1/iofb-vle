import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_testuserReport_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/test/userReport.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
loop:{
int i = 0
for( userAdminInstance in (userAdmins) ) {
printHtmlPart(4)
expressionOut.print(userAdminInstance[0])
printHtmlPart(5)
expressionOut.print(userAdminInstance[1])
printHtmlPart(6)
expressionOut.print(userAdminInstance[2])
printHtmlPart(7)
i++
}
}
printHtmlPart(8)
loop:{
int i = 0
for( userTestSessionInstance in (userTestSessions) ) {
printHtmlPart(9)
expressionOut.print(userTestSessionInstance[0])
printHtmlPart(10)
expressionOut.print(userTestSessionInstance[1])
printHtmlPart(6)
expressionOut.print(userTestSessionInstance[2])
printHtmlPart(11)
expressionOut.print(userTestSessionInstance[3])
printHtmlPart(12)
expressionOut.print(userTestSessionInstance[4])
printHtmlPart(13)
expressionOut.print(userTestSessionInstance[5])
printHtmlPart(14)
expressionOut.print(userTestSessionInstance[6])
printHtmlPart(15)
i++
}
}
printHtmlPart(16)
})
invokeTag('captureBody','sitemesh',82,[:],1)
printHtmlPart(17)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636085L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
