import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_courseshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/course/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'course.label', default: 'Course'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',11,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
invokeTag('display','f',11,['bean':("course"),'property':("name")],-1)
printHtmlPart(6)
invokeTag('display','f',13,['bean':("course"),'property':("name")],-1)
printHtmlPart(7)
createClosureForHtmlPart(8, 2)
invokeTag('link','g',15,['controller':("course"),'action':("showWelcomePage"),'id':(course.id)],2)
printHtmlPart(9)
createClosureForHtmlPart(10, 2)
invokeTag('link','g',16,['controller':("course"),'action':("showStaffPage"),'id':(course.id)],2)
printHtmlPart(11)
createTagBody(2, {->
printHtmlPart(12)
createClosureForHtmlPart(13, 3)
invokeTag('link','g',24,['controller':("module"),'class':("btn btn-nav"),'action':("create"),'params':([course:course.id])],3)
printHtmlPart(14)
})
invokeTag('ifAnyGranted','sec',24,['roles':("ROLE_ADMIN")],2)
printHtmlPart(15)
createClosureForHtmlPart(16, 2)
invokeTag('ifAnyGranted','sec',34,['roles':("ROLE_ADMIN")],2)
printHtmlPart(17)
loop:{
int i = 0
for( moduleInstance in (course.modules.sort{it.orderNo}) ) {
printHtmlPart(18)
if(true && (moduleInstance.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]')) {
printHtmlPart(19)
createTagBody(4, {->
printHtmlPart(20)
expressionOut.print(moduleInstance.orderNo)
printHtmlPart(21)
createTagBody(5, {->
expressionOut.print(moduleInstance.name)
})
invokeTag('link','g',46,['controller':("module"),'action':("show"),'id':(moduleInstance.id)],5)
printHtmlPart(22)
})
invokeTag('ifAnyGranted','sec',46,['roles':("ROLE_ADMIN")],4)
printHtmlPart(22)
createTagBody(4, {->
printHtmlPart(23)
expressionOut.print(moduleInstance.orderNo)
printHtmlPart(21)
createTagBody(5, {->
expressionOut.print(moduleInstance.name)
})
invokeTag('link','g',50,['controller':("module"),'action':("studentView"),'id':(moduleInstance.id)],5)
printHtmlPart(22)
})
invokeTag('ifAnyGranted','sec',50,['roles':("ROLE_USER")],4)
printHtmlPart(24)
createTagBody(4, {->
printHtmlPart(25)
createClosureForHtmlPart(26, 5)
invokeTag('link','g',58,['controller':("module"),'action':("show"),'id':(moduleInstance.id)],5)
printHtmlPart(27)
createClosureForHtmlPart(28, 5)
invokeTag('link','g',60,['controller':("lecture"),'class':("create"),'action':("create"),'params':([module:moduleInstance.id])],5)
printHtmlPart(27)
createClosureForHtmlPart(29, 5)
invokeTag('link','g',62,['controller':("module"),'action':("studentView"),'id':(moduleInstance.id)],5)
printHtmlPart(27)
createClosureForHtmlPart(30, 5)
invokeTag('link','g',63,['controller':("module"),'action':("edit"),'id':(moduleInstance.id)],5)
printHtmlPart(27)
createTagBody(5, {->
printHtmlPart(31)
invokeTag('actionSubmit','g',65,['class':("delete-button"),'action':("delete"),'value':("Delete module"),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(32)
})
invokeTag('form','g',65,['controller':("module"),'id':(moduleInstance.id),'method':("DELETE")],5)
printHtmlPart(33)
})
invokeTag('ifAnyGranted','sec',67,['roles':("ROLE_ADMIN")],4)
printHtmlPart(34)
}
printHtmlPart(18)
i++
}
}
printHtmlPart(35)
if(true && (course.id == 1)) {
printHtmlPart(36)
}
printHtmlPart(37)
})
invokeTag('captureBody','sitemesh',84,[:],1)
printHtmlPart(38)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636320L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
