import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_questionGroup_images_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/questionGroup/_images.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (flash.ajaxerror)) {
printHtmlPart(1)
expressionOut.print(flash.ajaxerror)
printHtmlPart(2)
}
printHtmlPart(3)
if(true && (questionGroup.qGImages)) {
printHtmlPart(4)
for( qGImage in (questionGroup.qGImages) ) {
printHtmlPart(5)
invokeTag('resource','g',24,['base':(grailsApplication.config.external.webserver),'dir':("images"),'file':(qGImage.fileName)],-1)
printHtmlPart(6)
expressionOut.print(qGImage?.id)
printHtmlPart(7)
}
printHtmlPart(8)
}
else {
printHtmlPart(9)
}
printHtmlPart(10)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637304L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
