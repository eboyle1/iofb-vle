import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_questionGroupedit_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/questionGroup/edit.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'questionGroup.label', default: 'QuestionGroup'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.edit.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(1)
invokeTag('resources','ckeditor',7,[:],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',27,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
invokeTag('render','g',30,['template':("/shared-templates/test_head"),'model':(['test':questionGroup.owner])],-1)
printHtmlPart(5)
createTagBody(2, {->
printHtmlPart(6)
createTagBody(3, {->
printHtmlPart(7)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(8)
expressionOut.print(error.field)
printHtmlPart(9)
}
printHtmlPart(10)
invokeTag('message','g',40,['error':(error)],-1)
printHtmlPart(11)
})
invokeTag('eachError','g',41,['bean':(questionGroup),'var':("error")],3)
printHtmlPart(12)
})
invokeTag('hasErrors','g',43,['bean':(questionGroup)],2)
printHtmlPart(13)
createTagBody(2, {->
printHtmlPart(14)
invokeTag('render','g',49,['template':("form")],-1)
printHtmlPart(15)
createClosureForHtmlPart(16, 3)
invokeTag('link','g',54,['controller':("test"),'action':("show"),'id':(questionGroup.owner.id),'class':("btn btn-default")],3)
printHtmlPart(17)
})
invokeTag('form','g',59,['role':("form"),'class':("form-horizontal"),'url':([resource:questionGroup, action:'update']),'method':("PUT")],2)
printHtmlPart(18)
createTagBody(2, {->
printHtmlPart(6)
createTagBody(3, {->
printHtmlPart(19)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(8)
expressionOut.print(error.field)
printHtmlPart(9)
}
printHtmlPart(10)
invokeTag('message','g',69,['error':(error)],-1)
printHtmlPart(20)
})
invokeTag('eachError','g',70,['bean':(imageCommand),'var':("error")],3)
printHtmlPart(12)
})
invokeTag('hasErrors','g',72,['bean':(imageCommand)],2)
printHtmlPart(21)
createTagBody(2, {->
printHtmlPart(22)
invokeTag('hiddenField','g',82,['name':("questionGroupId"),'value':(questionGroup.id)],-1)
printHtmlPart(23)
})
invokeTag('uploadForm','g',83,['action':("uploadQuestionGroupImage")],2)
printHtmlPart(24)
createClosureForHtmlPart(25, 2)
invokeTag('link','g',86,['controller':("test"),'id':(questionGroup.owner.id),'class':("btn btn-nav"),'action':("show")],2)
printHtmlPart(26)
invokeTag('render','g',92,['template':("images"),'bean':(questionGroup)],-1)
printHtmlPart(27)
})
invokeTag('captureBody','sitemesh',95,[:],1)
printHtmlPart(28)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637330L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
