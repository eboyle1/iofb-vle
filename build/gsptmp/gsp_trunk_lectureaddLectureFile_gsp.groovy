import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lectureaddLectureFile_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/addLectureFile.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.addFile.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
createClosureForHtmlPart(5, 2)
invokeTag('link','g',11,['controller':("course"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('display','f',11,['bean':("lecture"),'property':("module.course.name")],-1)
})
invokeTag('link','g',11,['controller':("course"),'action':("show"),'id':(lecture.module.course.id)],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('display','f',11,['bean':("lecture"),'property':("module.name")],-1)
})
invokeTag('link','g',11,['controller':("module"),'action':("show"),'id':(lecture.module.id)],2)
printHtmlPart(7)
invokeTag('message','g',14,['code':("default.addLectureFile.label"),'args':([entityName])],-1)
printHtmlPart(8)
createTagBody(2, {->
printHtmlPart(9)
createTagBody(3, {->
printHtmlPart(10)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(11)
expressionOut.print(error.field)
printHtmlPart(12)
}
printHtmlPart(13)
invokeTag('message','g',22,['error':(error)],-1)
printHtmlPart(14)
})
invokeTag('eachError','g',23,['bean':(fileCommand),'var':("error")],3)
printHtmlPart(15)
})
invokeTag('hasErrors','g',25,['bean':(fileCommand)],2)
printHtmlPart(16)
expressionOut.print(lecture.name)
printHtmlPart(17)
if(true && (lecture.lectureFile)) {
printHtmlPart(18)
expressionOut.print(lecture.lectureFile.fileName)
printHtmlPart(19)
createTagBody(3, {->
printHtmlPart(20)
invokeTag('actionSubmit','g',42,['action':("deleteLectureFile"),'value':("Delete lecture file"),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(20)
})
invokeTag('form','g',43,['controller':("lecture"),'id':(lecture.id),'method':("DELETE")],3)
printHtmlPart(21)
createClosureForHtmlPart(22, 3)
invokeTag('link','g',45,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default pull-right")],3)
printHtmlPart(23)
}
else {
printHtmlPart(24)
createTagBody(3, {->
printHtmlPart(25)
invokeTag('hiddenField','g',56,['name':("lectureId"),'value':(lecture.id)],-1)
printHtmlPart(26)
expressionOut.print(message(code: 'lecture.lectureFile.upload.label', default: 'Upload'))
printHtmlPart(27)
createClosureForHtmlPart(22, 4)
invokeTag('link','g',65,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default pull-right")],4)
printHtmlPart(28)
})
invokeTag('uploadForm','g',68,['name':("upload"),'action':("uploadLectureFile"),'method':("post"),'controller':("Lecture")],3)
printHtmlPart(29)
}
printHtmlPart(30)
})
invokeTag('captureBody','sitemesh',73,[:],1)
printHtmlPart(31)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636907L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
