import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lecture_form_fields_create_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/_form_fields_create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean:lecture, field:'name', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',6,['type':("text"),'name':("name"),'bean':(lecture),'value':(""),'class':("form-control")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean:lecture, field:'module', 'has-warning'))
printHtmlPart(3)
invokeTag('widget','f',13,['bean':(lecture),'property':("module"),'class':("form-check-input")],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean:lecture, field:'orderNo', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',20,['type':("number"),'min':("1"),'max':("100"),'name':("orderNo"),'bean':(lecture),'value':(""),'class':("form-control")],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean:lecture,field:'isVisible', 'has-warning'))
printHtmlPart(6)
invokeTag('widget','f',28,['bean':(lecture),'property':("isVisible"),'class':("form-check-input")],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean:lecture,field:'description', 'has-warning'))
printHtmlPart(8)
invokeTag('field','g',37,['type':("text"),'name':("description"),'bean':(lecture),'value':(""),'class':("form-control")],-1)
printHtmlPart(9)
expressionOut.print(hasErrors(bean:lecture,field:'authors', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',42,['type':("text"),'name':("authors"),'bean':(lecture),'value':(""),'class':("form-control")],-1)
printHtmlPart(10)
expressionOut.print(hasErrors(bean:lecture,field:'editors', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',50,['type':("text"),'name':("editors"),'bean':(lecture),'value':(""),'class':("form-control")],-1)
printHtmlPart(11)
createClosureForHtmlPart(12, 1)
invokeTag('editor','ckeditor',58,['name':("learningObjectives"),'toolbar':("custom"),'height':("300px"),'width':("100%")],1)
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636901L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
