import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_moduleshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/module/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'module.label', default: 'Module'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',11,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',11,['bean':("module"),'property':("course.name")],-1)
})
invokeTag('link','g',11,['controller':("course"),'action':("show"),'id':(module.course.id)],2)
printHtmlPart(5)
invokeTag('display','f',11,['bean':("module"),'property':("name")],-1)
printHtmlPart(6)
invokeTag('display','f',13,['bean':("module"),'property':("name")],-1)
printHtmlPart(7)
createClosureForHtmlPart(8, 2)
invokeTag('link','g',16,['controller':("module"),'id':(module.id),'action':("studentView"),'class':("btn btn-nav")],2)
printHtmlPart(9)
createClosureForHtmlPart(10, 2)
invokeTag('link','g',17,['controller':("lecture"),'action':("create"),'params':([module:module.id]),'class':("btn btn-nav")],2)
printHtmlPart(11)
loop:{
int i = 0
for( lectureInstance in (module.lectures.sort{it.orderNo}) ) {
printHtmlPart(12)
createTagBody(3, {->
expressionOut.print(lectureInstance.name)
})
invokeTag('link','g',32,['controller':("lecture"),'action':("show"),'id':(lectureInstance.id)],3)
printHtmlPart(13)
createClosureForHtmlPart(14, 3)
invokeTag('link','g',38,['controller':("lecture"),'action':("edit"),'id':(lectureInstance.id)],3)
printHtmlPart(15)
createClosureForHtmlPart(16, 3)
invokeTag('link','g',39,['controller':("lecture"),'action':("addLectureFile"),'id':(lectureInstance.id)],3)
printHtmlPart(15)
createClosureForHtmlPart(17, 3)
invokeTag('link','g',40,['controller':("lecture"),'action':("addContentFile"),'id':(lectureInstance.id)],3)
printHtmlPart(15)
createClosureForHtmlPart(18, 3)
invokeTag('link','g',41,['controller':("test"),'action':("create"),'params':([lecture:lectureInstance.id])],3)
printHtmlPart(15)
createClosureForHtmlPart(19, 3)
invokeTag('link','g',42,['controller':("lecture"),'action':("listTests"),'id':(lectureInstance.id)],3)
printHtmlPart(20)
createTagBody(3, {->
printHtmlPart(21)
invokeTag('actionSubmit','g',45,['class':("delete-button"),'action':("delete"),'value':("Delete lecture"),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(21)
})
invokeTag('form','g',45,['controller':("lecture"),'id':(lectureInstance.id),'method':("DELETE")],3)
printHtmlPart(22)
i++
}
}
printHtmlPart(23)
})
invokeTag('captureBody','sitemesh',54,[:],1)
printHtmlPart(24)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637648L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
