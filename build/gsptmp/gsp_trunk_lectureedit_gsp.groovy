import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lectureedit_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/edit.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.edit.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(1)
invokeTag('resources','ckeditor',7,[:],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',29,[:],1)
printHtmlPart(3)
createTagBody(1, {->
printHtmlPart(4)
createClosureForHtmlPart(5, 2)
invokeTag('link','g',32,['controller':("course"),'action':("index")],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('display','f',32,['bean':("lecture"),'property':("module.course.name")],-1)
})
invokeTag('link','g',32,['controller':("course"),'action':("show"),'id':(lecture.module.course.id)],2)
printHtmlPart(6)
createTagBody(2, {->
invokeTag('display','f',32,['bean':("lecture"),'property':("module.name")],-1)
})
invokeTag('link','g',32,['controller':("module"),'action':("show"),'id':(lecture.module.id)],2)
printHtmlPart(7)
invokeTag('message','g',35,['code':("default.edit.label"),'args':([entityName])],-1)
printHtmlPart(8)
createTagBody(2, {->
printHtmlPart(9)
createTagBody(3, {->
printHtmlPart(10)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(11)
expressionOut.print(error.field)
printHtmlPart(12)
}
printHtmlPart(13)
invokeTag('message','g',43,['error':(error)],-1)
printHtmlPart(14)
})
invokeTag('eachError','g',44,['bean':(lecture),'var':("error")],3)
printHtmlPart(15)
})
invokeTag('hasErrors','g',46,['bean':(lecture)],2)
printHtmlPart(16)
createTagBody(2, {->
printHtmlPart(17)
invokeTag('hiddenField','g',50,['name':("version"),'value':(lecture?.version)],-1)
printHtmlPart(18)
invokeTag('render','g',53,['template':("form_fields")],-1)
printHtmlPart(19)
createClosureForHtmlPart(20, 3)
invokeTag('link','g',59,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default")],3)
printHtmlPart(21)
expressionOut.print(message(code: 'default.button.update.label', default: 'Update'))
printHtmlPart(22)
createClosureForHtmlPart(23, 3)
invokeTag('link','g',63,['controller':("lecture"),'action':("show"),'id':(lecture.id),'class':("btn btn-nav")],3)
printHtmlPart(24)
})
invokeTag('form','g',67,['url':([resource:lecture, action:'update']),'class':("form-horizontal"),'method':("PUT")],2)
printHtmlPart(25)
if(true && (lecture.lectureFile)) {
printHtmlPart(26)
invokeTag('message','g',75,['code':("default.editLectureFile.label"),'args':([entityName])],-1)
printHtmlPart(27)
createTagBody(3, {->
printHtmlPart(28)
invokeTag('render','g',81,['template':("form_fields_file")],-1)
printHtmlPart(29)
createClosureForHtmlPart(20, 4)
invokeTag('link','g',87,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default")],4)
printHtmlPart(21)
expressionOut.print(message(code: 'default.button.update.label', default: 'Update'))
printHtmlPart(22)
createClosureForHtmlPart(23, 4)
invokeTag('link','g',91,['controller':("lecture"),'action':("show"),'id':(lecture.id),'class':("btn btn-nav")],4)
printHtmlPart(30)
})
invokeTag('form','g',96,['url':([resource:lecture, action:'updateLectureFileName']),'class':("form-horizontal"),'method':("PUT")],3)
printHtmlPart(31)
createClosureForHtmlPart(32, 3)
invokeTag('form','g',105,['controller':("lecture"),'action':("deleteLectureFile2"),'id':(lecture.id),'method':("DELETE")],3)
printHtmlPart(33)
}
else {
printHtmlPart(34)
invokeTag('message','g',115,['code':("default.addLectureFile.label"),'args':([entityName])],-1)
printHtmlPart(35)
createTagBody(3, {->
printHtmlPart(9)
createTagBody(4, {->
printHtmlPart(36)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(11)
expressionOut.print(error.field)
printHtmlPart(12)
}
printHtmlPart(13)
invokeTag('message','g',124,['error':(error)],-1)
printHtmlPart(37)
})
invokeTag('eachError','g',125,['bean':(fileCommand),'var':("error")],4)
printHtmlPart(15)
})
invokeTag('hasErrors','g',127,['bean':(fileCommand)],3)
printHtmlPart(38)
createTagBody(3, {->
printHtmlPart(39)
invokeTag('hiddenField','g',137,['name':("lectureId"),'value':(lecture.id)],-1)
printHtmlPart(40)
expressionOut.print(message(code: 'lecture.lectureFile.upload.label', default: 'Upload'))
printHtmlPart(41)
createClosureForHtmlPart(20, 4)
invokeTag('link','g',146,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default pull-right")],4)
printHtmlPart(42)
})
invokeTag('uploadForm','g',148,['name':("upload"),'action':("uploadLectureFile2"),'method':("post"),'controller':("Lecture")],3)
printHtmlPart(43)
}
printHtmlPart(26)
invokeTag('message','g',155,['code':("default.editContentFiles.label"),'args':([entityName])],-1)
printHtmlPart(44)
createTagBody(2, {->
printHtmlPart(9)
createTagBody(3, {->
printHtmlPart(10)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(11)
expressionOut.print(error.field)
printHtmlPart(12)
}
printHtmlPart(13)
invokeTag('message','g',164,['error':(error)],-1)
printHtmlPart(14)
})
invokeTag('eachError','g',165,['bean':(contentFileCommand),'var':("error")],3)
printHtmlPart(15)
})
invokeTag('hasErrors','g',167,['bean':(contentFileCommand)],2)
printHtmlPart(45)
createTagBody(2, {->
printHtmlPart(39)
invokeTag('hiddenField','g',177,['name':("lectureId"),'value':(lecture.id)],-1)
printHtmlPart(46)
createClosureForHtmlPart(20, 3)
invokeTag('link','g',196,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-default")],3)
printHtmlPart(47)
expressionOut.print(message(code: 'lecture.lectureFile.upload.label', default: 'Upload'))
printHtmlPart(48)
createClosureForHtmlPart(23, 3)
invokeTag('link','g',200,['controller':("lecture"),'action':("show"),'id':(lecture.id),'class':("btn btn-nav")],3)
printHtmlPart(49)
})
invokeTag('uploadForm','g',204,['name':("upload"),'action':("uploadContentFile2"),'method':("post"),'controller':("Lecture")],2)
printHtmlPart(50)
invokeTag('render','g',207,['template':("content_files"),'bean':(lecture)],-1)
printHtmlPart(51)
})
invokeTag('captureBody','sitemesh',212,[:],1)
printHtmlPart(52)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636994L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
