import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_modulestudentView_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/module/studentView.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'module.label', default: 'Module'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',11,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',11,['bean':("module"),'property':("course.name")],-1)
})
invokeTag('link','g',11,['controller':("course"),'action':("show"),'id':(module.course.id)],2)
printHtmlPart(5)
invokeTag('display','f',11,['bean':("module"),'property':("name")],-1)
printHtmlPart(6)
invokeTag('display','f',13,['bean':("module"),'property':("name")],-1)
printHtmlPart(7)
loop:{
int i = 0
for( lectureInstance in (module.lectures.sort{it.orderNo}) ) {
printHtmlPart(8)
invokeTag('display','f',23,['bean':(lectureInstance),'property':("orderNo")],-1)
printHtmlPart(9)
expressionOut.print(lectureInstance.id)
printHtmlPart(10)
invokeTag('display','f',24,['bean':(lectureInstance),'property':("name")],-1)
printHtmlPart(11)
i++
}
}
printHtmlPart(12)
loop:{
int i = 0
for( lectureInstance in (module.lectures.sort{it.orderNo}) ) {
printHtmlPart(1)
if(true && (lectureInstance.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]')) {
printHtmlPart(13)
expressionOut.print(lectureInstance.id)
printHtmlPart(14)
invokeTag('display','f',35,['bean':(lectureInstance),'property':("name")],-1)
printHtmlPart(15)
if(true && (lectureInstance.lectureFile)) {
printHtmlPart(16)
invokeTag('resource','g',40,['base':(grailsApplication.config.external.webserver),'dir':("lectureFiles"),'file':("${lectureInstance.id}/${lectureInstance.lectureFile.baseFileName}/index.htm")],-1)
printHtmlPart(17)
invokeTag('display','f',41,['bean':(lectureInstance),'property':("lectureFile.name")],-1)
printHtmlPart(18)
}
printHtmlPart(19)
if(true && (lectureInstance.contentFiles)) {
printHtmlPart(20)
for( contentFile in (lectureInstance.contentFiles.sort{it.fileType}) ) {
printHtmlPart(21)
invokeTag('resource','g',49,['base':(grailsApplication.config.external.webserver),'dir':("lectureFiles"),'file':("${lectureInstance.id}/${contentFile.fileName}")],-1)
printHtmlPart(10)
expressionOut.print(contentFile.name)
printHtmlPart(22)
}
printHtmlPart(23)
}
printHtmlPart(19)
if(true && (lectureInstance.tests)) {
printHtmlPart(20)
for( test in (lectureInstance.tests.sort{it.orderNo}) ) {
printHtmlPart(24)
if(true && (test.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]')) {
printHtmlPart(25)
createTagBody(7, {->
printHtmlPart(26)
expressionOut.print(test.name)
})
invokeTag('link','g',57,['controller':("userTest"),'action':("beginTest"),'params':([testId:test.id])],7)
printHtmlPart(27)
invokeTag('set','g',60,['var':("testTaken"),'value':(false)],-1)
printHtmlPart(27)
loop:{
int j = 0
for( userTestSessionInstance in (userTestSessions) ) {
printHtmlPart(28)
if(true && (userTestSessionInstance[1] == test.id)) {
printHtmlPart(29)
expressionOut.print(userTestSessionInstance[2])
printHtmlPart(30)
invokeTag('set','g',65,['var':("testTaken"),'value':(true)],-1)
printHtmlPart(28)
}
printHtmlPart(27)
j++
}
}
printHtmlPart(27)
if(true && (!testTaken)) {
printHtmlPart(31)
}
printHtmlPart(32)
}
printHtmlPart(33)
}
printHtmlPart(34)
}
else {
printHtmlPart(35)
}
printHtmlPart(36)
}
printHtmlPart(37)
i++
}
}
printHtmlPart(38)
})
invokeTag('captureBody','sitemesh',78,[:],1)
printHtmlPart(39)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637654L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
