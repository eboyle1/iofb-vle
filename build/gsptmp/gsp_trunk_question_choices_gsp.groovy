import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_question_choices_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/question/_choices.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (flash.ajaxerror)) {
printHtmlPart(1)
expressionOut.print(flash.ajaxerror)
printHtmlPart(2)
}
printHtmlPart(3)
createClosureForHtmlPart(4, 1)
invokeTag('link','g',16,['controller':("test"),'id':(question.owner.owner.id),'class':("btn btn-nav"),'action':("show")],1)
printHtmlPart(5)
for( choice in (choiceList) ) {
printHtmlPart(6)
expressionOut.print(choice?.id)
printHtmlPart(7)
invokeTag('renderChoice','p',27,['choice':(choice)],-1)
printHtmlPart(8)
}
printHtmlPart(9)
if(true && (choiceList.size() > 0)) {
printHtmlPart(10)
invokeTag('set','g',35,['var':("choiceOrder"),'value':(choiceList.size()+1)],-1)
printHtmlPart(11)
}
else {
printHtmlPart(10)
invokeTag('set','g',38,['var':("choiceOrder"),'value':("1")],-1)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('field','g',44,['name':("choiceOrderField"),'type':("number"),'min':("1"),'max':("10"),'size':("2"),'value':(choiceOrder),'class':("form-control")],-1)
printHtmlPart(13)
invokeTag('field','g',49,['name':("choiceTextField"),'type':("text"),'size':("50"),'class':("form-control")],-1)
printHtmlPart(14)
invokeTag('checkBox','g',54,['name':("choiceStatusField"),'class':("form-check-input")],-1)
printHtmlPart(15)
expressionOut.print(question?.id)
printHtmlPart(16)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637835L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
