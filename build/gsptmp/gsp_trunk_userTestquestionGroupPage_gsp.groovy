import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_userTestquestionGroupPage_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/userTest/questionGroupPage.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'course.label', default: 'Course'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.edit.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
expressionOut.print(grailsApplication.config.vle.url)
printHtmlPart(3)
expressionOut.print(test.id)
printHtmlPart(4)
expressionOut.print(questionGroup.id)
printHtmlPart(5)
expressionOut.print(userTestSession.id)
printHtmlPart(6)
})
invokeTag('captureHead','sitemesh',43,[:],1)
printHtmlPart(7)
createTagBody(1, {->
printHtmlPart(8)
createClosureForHtmlPart(9, 2)
invokeTag('link','g',47,['controller':("course"),'action':("index")],2)
printHtmlPart(10)
createTagBody(2, {->
invokeTag('display','f',47,['bean':("test"),'property':("lecture.module.course.name")],-1)
})
invokeTag('link','g',47,['controller':("course"),'action':("show"),'id':(test.lecture.module.course.id)],2)
printHtmlPart(10)
createTagBody(2, {->
invokeTag('display','f',47,['bean':("test"),'property':("lecture.module.name")],-1)
})
invokeTag('link','g',47,['controller':("module"),'action':("studentView"),'id':(test.lecture.module.id)],2)
printHtmlPart(10)
invokeTag('display','f',47,['bean':("test"),'property':("name")],-1)
printHtmlPart(11)
invokeTag('display','f',49,['bean':("test"),'property':("name")],-1)
printHtmlPart(12)
createClosureForHtmlPart(13, 2)
invokeTag('link','g',52,['controller':("test.lecture.module"),'id':(test.lecture.module.id),'class':("btn btn-nav"),'action':("studentView")],2)
printHtmlPart(14)
})
invokeTag('captureBody','sitemesh',59,[:],1)
printHtmlPart(15)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164638206L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
