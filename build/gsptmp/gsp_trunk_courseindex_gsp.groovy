import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_courseindex_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/course/index.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'course.label', default: 'Course'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.list.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createTagBody(2, {->
printHtmlPart(4)
createClosureForHtmlPart(5, 3)
invokeTag('link','g',17,['class':("btn btn-nav"),'action':("create")],3)
printHtmlPart(4)
})
invokeTag('ifAnyGranted','sec',18,['roles':("ROLE_ADMIN")],2)
printHtmlPart(6)
createClosureForHtmlPart(7, 2)
invokeTag('ifAnyGranted','sec',28,['roles':("ROLE_ADMIN")],2)
printHtmlPart(8)
loop:{
int i = 0
for( courseInstance in (courseList.sort{it.orderNo}) ) {
printHtmlPart(9)
if(true && (courseInstance.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]')) {
printHtmlPart(10)
createTagBody(4, {->
expressionOut.print(fieldValue(bean: courseInstance, field: "name"))
})
invokeTag('link','g',36,['action':("show"),'id':(courseInstance.id)],4)
printHtmlPart(11)
createTagBody(4, {->
printHtmlPart(12)
createClosureForHtmlPart(13, 5)
invokeTag('link','g',43,['action':("show"),'id':(courseInstance.id)],5)
printHtmlPart(14)
createClosureForHtmlPart(15, 5)
invokeTag('link','g',44,['controller':("module"),'class':("create"),'action':("create"),'params':([course:courseInstance.id])],5)
printHtmlPart(14)
createClosureForHtmlPart(16, 5)
invokeTag('link','g',45,['action':("edit"),'id':(courseInstance.id)],5)
printHtmlPart(17)
createTagBody(5, {->
printHtmlPart(18)
invokeTag('actionSubmit','g',48,['class':("delete-button"),'action':("delete"),'value':("Delete course"),'onclick':("return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');")],-1)
printHtmlPart(18)
})
invokeTag('form','g',48,['id':(courseInstance.id),'method':("DELETE")],5)
printHtmlPart(19)
})
invokeTag('ifAnyGranted','sec',53,['roles':("ROLE_ADMIN")],4)
printHtmlPart(20)
}
printHtmlPart(21)
i++
}
}
printHtmlPart(22)
})
invokeTag('captureBody','sitemesh',58,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636299L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
