import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_questionGroup_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/questionGroup/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: questionGroup, field: 'owner', 'error'))
printHtmlPart(1)
invokeTag('widget','f',9,['bean':(questionGroup),'property':("owner"),'class':("form-check-input")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean: questionGroup, field: 'orderNo', 'error'))
printHtmlPart(3)
invokeTag('field','g',21,['name':("orderNo"),'type':("number"),'min':("1"),'max':("100"),'value':(questionGroup?.orderNo),'required':(""),'class':("form-control")],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean: questionGroup, field: 'questionGroupText', 'error'))
printHtmlPart(5)
invokeTag('message','g',30,['code':("question.questionGroupText.label"),'default':("Question Group Text ")],-1)
printHtmlPart(6)
createTagBody(1, {->
printHtmlPart(7)
expressionOut.print(questionGroup?.questionGroupText)
printHtmlPart(8)
})
invokeTag('editor','ckeditor',37,['name':("questionGroupText"),'toolbar':("custom"),'height':("200px"),'width':("100%")],1)
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637290L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
