import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_questionGroupcreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/questionGroup/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'questionGroup.label', default: 'QuestionGroup'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.create.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(1)
invokeTag('resources','ckeditor',7,[:],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
invokeTag('render','g',11,['template':("/shared-templates/test_head")],-1)
printHtmlPart(4)
createTagBody(2, {->
printHtmlPart(5)
createTagBody(3, {->
printHtmlPart(6)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(7)
expressionOut.print(error.field)
printHtmlPart(8)
}
printHtmlPart(9)
invokeTag('message','g',22,['error':(error)],-1)
printHtmlPart(10)
})
invokeTag('eachError','g',23,['bean':(this.questionGroup),'var':("error")],3)
printHtmlPart(11)
})
invokeTag('hasErrors','g',26,['bean':(this.questionGroup)],2)
printHtmlPart(12)
createTagBody(2, {->
printHtmlPart(13)
invokeTag('hiddenField','g',30,['name':("ownerId"),'value':(test?.id)],-1)
printHtmlPart(14)
invokeTag('hiddenField','g',31,['name':("isVisible"),'value':("false")],-1)
printHtmlPart(15)
invokeTag('render','g',33,['template':("form")],-1)
printHtmlPart(16)
expressionOut.print(message(code: 'default.button.create.label', default: 'Create'))
printHtmlPart(17)
createClosureForHtmlPart(18, 3)
invokeTag('link','g',39,['controller':("test"),'action':("show"),'id':(test.id),'class':("btn btn-default pull-right")],3)
printHtmlPart(19)
})
invokeTag('form','g',42,['class':("form-horizontal"),'url':([resource:questionGroup, action:'save'])],2)
printHtmlPart(20)
})
invokeTag('captureBody','sitemesh',47,[:],1)
printHtmlPart(21)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637297L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
