import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lecture_content_files_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/_content_files.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (flash.ajaxerror)) {
printHtmlPart(1)
expressionOut.print(flash.ajaxerror)
printHtmlPart(2)
}
printHtmlPart(3)
if(true && (lecture.contentFiles)) {
printHtmlPart(4)
for( contentFile in (lecture.contentFiles) ) {
printHtmlPart(5)
expressionOut.print(contentFile.fileName)
printHtmlPart(6)
expressionOut.print(contentFile.name)
printHtmlPart(7)
expressionOut.print(contentFile?.id)
printHtmlPart(8)
}
printHtmlPart(9)
}
else {
printHtmlPart(10)
}
printHtmlPart(11)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636894L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
