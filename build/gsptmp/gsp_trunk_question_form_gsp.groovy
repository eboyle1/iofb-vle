import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_question_form_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/question/_form.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean: question, field: 'owner', 'error'))
printHtmlPart(1)
invokeTag('widget','f',9,['bean':(question),'property':("owner"),'class':("form-check-input")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean: question, field: 'orderNo', 'error'))
printHtmlPart(3)
invokeTag('field','g',21,['name':("orderNo"),'type':("number"),'min':("1"),'max':("100"),'value':(question?.orderNo),'required':(""),'class':("form-control")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean: question, field: 'questionText', 'error'))
printHtmlPart(4)
invokeTag('message','g',29,['code':("question.questionText.label"),'default':("Question Text ")],-1)
printHtmlPart(5)
createTagBody(1, {->
printHtmlPart(6)
expressionOut.print(question?.questionText)
printHtmlPart(7)
})
invokeTag('editor','ckeditor',34,['name':("questionText"),'toolbar':("custom"),'height':("200px"),'width':("100%")],1)
printHtmlPart(8)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637844L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
