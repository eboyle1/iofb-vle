import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_userTest_questionGroup_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/userTest/_questionGroup.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
if(true && (flash.message)) {
printHtmlPart(1)
expressionOut.print(flash.message)
printHtmlPart(2)
}
printHtmlPart(3)
if(true && (flash.error)) {
printHtmlPart(4)
expressionOut.print(flash.error)
printHtmlPart(2)
}
printHtmlPart(5)
expressionOut.print(questionGroup.orderNo)
printHtmlPart(6)
expressionOut.print(test.questionGroups.max{it.orderNo}.orderNo)
printHtmlPart(7)
expressionOut.print(raw(questionGroup.questionGroupText))
printHtmlPart(8)
for( image in (questionGroup.qGImages) ) {
printHtmlPart(9)
expressionOut.print(grailsApplication.config.external.webserver)
printHtmlPart(10)
expressionOut.print(image.fileName)
printHtmlPart(11)
expressionOut.print(grailsApplication.config.external.webserver)
printHtmlPart(10)
expressionOut.print(image.fileName)
printHtmlPart(12)
}
printHtmlPart(13)
for( question in (questionGroup.questions.sort{it.orderNo}) ) {
printHtmlPart(14)
if(true && (question.choices.size() >= 4)) {
printHtmlPart(15)
}
else {
printHtmlPart(16)
}
printHtmlPart(17)
expressionOut.print(raw(question.questionText))
printHtmlPart(18)
invokeTag('renderAnswer','p',49,['question':(question),'userTestSession':(userTestSession)],-1)
printHtmlPart(19)
}
printHtmlPart(20)
if(true && (questionGroup.orderNo != 1)) {
printHtmlPart(21)
expressionOut.print(questionGroup?.id)
printHtmlPart(22)
expressionOut.print(test?.id)
printHtmlPart(22)
expressionOut.print(userTestSession?.id)
printHtmlPart(23)
}
printHtmlPart(24)
if(true && (questionGroup.orderNo != test.questionGroups.max{it.orderNo}.orderNo)) {
printHtmlPart(25)
expressionOut.print(questionGroup?.id)
printHtmlPart(22)
expressionOut.print(test?.id)
printHtmlPart(22)
expressionOut.print(userTestSession?.id)
printHtmlPart(26)
}
else {
printHtmlPart(27)
expressionOut.print(questionGroup?.id)
printHtmlPart(22)
expressionOut.print(test?.id)
printHtmlPart(22)
expressionOut.print(userTestSession?.id)
printHtmlPart(28)
}
printHtmlPart(29)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164638212L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
