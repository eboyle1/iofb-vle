import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lectureshow_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/show.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',10,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',10,['bean':("lecture"),'property':("module.course.name")],-1)
})
invokeTag('link','g',10,['controller':("course"),'action':("show"),'id':(lecture.module.course.id)],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',10,['bean':("lecture"),'property':("module.name")],-1)
})
invokeTag('link','g',10,['controller':("module"),'action':("show"),'id':(lecture.module.id)],2)
printHtmlPart(5)
invokeTag('display','f',10,['bean':("lecture"),'property':("name")],-1)
printHtmlPart(6)
invokeTag('display','f',13,['bean':("lecture"),'property':("name")],-1)
printHtmlPart(7)
invokeTag('display','f',14,['bean':("lecture"),'property':("description")],-1)
printHtmlPart(8)
invokeTag('display','f',15,['bean':("lecture"),'property':("authors")],-1)
printHtmlPart(9)
invokeTag('display','f',15,['bean':("lecture"),'property':("editors")],-1)
printHtmlPart(10)
invokeTag('display','f',16,['bean':("lecture"),'property':("learningObjectives")],-1)
printHtmlPart(11)
if(true && (lecture.lectureFile)) {
printHtmlPart(12)
invokeTag('resource','g',21,['base':(grailsApplication.config.external.webserver),'dir':("lectureFiles"),'file':("${lecture.id}/${lecture.lectureFile.baseFileName}/index.htm")],-1)
printHtmlPart(13)
invokeTag('display','f',21,['bean':("lecture"),'property':("lectureFile.name")],-1)
printHtmlPart(14)
}
printHtmlPart(15)
if(true && (lecture.contentFiles)) {
printHtmlPart(16)
for( contentFile in (lecture.contentFiles) ) {
printHtmlPart(17)
invokeTag('resource','g',29,['base':(grailsApplication.config.external.webserver),'dir':("lectureFiles"),'file':("${lecture.id}/${contentFile.fileName}")],-1)
printHtmlPart(13)
expressionOut.print(contentFile.name)
printHtmlPart(18)
}
printHtmlPart(19)
}
printHtmlPart(20)
if(true && (lecture.tests)) {
printHtmlPart(16)
for( test in (lecture.tests.sort{it.orderNo}) ) {
printHtmlPart(21)
createTagBody(4, {->
expressionOut.print(test.name)
})
invokeTag('link','g',39,['controller':("userTest"),'action':("beginTest"),'params':([testId:test.id])],4)
printHtmlPart(22)
}
printHtmlPart(19)
}
printHtmlPart(23)
createTagBody(2, {->
printHtmlPart(24)
createClosureForHtmlPart(25, 3)
invokeTag('link','g',51,['controller':("lecture"),'action':("edit"),'id':(lecture.id),'class':("btn btn-primary")],3)
printHtmlPart(26)
createClosureForHtmlPart(27, 3)
invokeTag('link','g',55,['controller':("module"),'action':("show"),'id':(lecture.module.id),'class':("btn btn-nav")],3)
printHtmlPart(28)
})
invokeTag('form','g',58,['url':([resource:lecture, action:'delete']),'method':("DELETE")],2)
printHtmlPart(29)
})
invokeTag('captureBody','sitemesh',62,[:],1)
printHtmlPart(30)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636932L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
