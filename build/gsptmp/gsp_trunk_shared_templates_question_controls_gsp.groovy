import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_shared_templates_question_controls_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/shared-templates/_question_controls.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
for( questionGroup in (test.questionGroups.sort{it.orderNo}) ) {
printHtmlPart(1)
invokeTag('renderQuestionGroup','p',7,['questionGroup':(questionGroup)],-1)
printHtmlPart(2)
createClosureForHtmlPart(3, 2)
invokeTag('link','g',12,['controller':("questionGroup"),'action':("edit"),'id':(questionGroup.id),'data-toggle':("tooltip"),'title':("Edit question group")],2)
printHtmlPart(4)
createClosureForHtmlPart(5, 2)
invokeTag('link','g',13,['controller':("questionGroup"),'action':("delete"),'id':(questionGroup.id),'data-confirm':("Are you sure you want to delete this question group? This action cannot be undone."),'data-toggle':("tooltip"),'title':("Delete question group")],2)
printHtmlPart(6)
for( question in (questionGroup.questions.sort{it.orderNo}) ) {
printHtmlPart(7)
invokeTag('renderQuestion','p',22,['questionGroup':(questionGroup),'question':(question)],-1)
printHtmlPart(8)
createClosureForHtmlPart(3, 3)
invokeTag('link','g',26,['controller':("question"),'action':("edit"),'id':(question.id),'data-toggle':("tooltip"),'title':("Edit question")],3)
printHtmlPart(9)
createClosureForHtmlPart(5, 3)
invokeTag('link','g',27,['controller':("question"),'action':("delete"),'id':(question.id),'data-confirm':("Are you sure you want to delete this question? This action cannot be undone."),'data-toggle':("tooltip"),'title':("Delete question")],3)
printHtmlPart(10)
}
printHtmlPart(11)
}
printHtmlPart(12)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636576L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
