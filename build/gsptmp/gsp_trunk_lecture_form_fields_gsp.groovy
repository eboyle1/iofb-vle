import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lecture_form_fields_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/_form_fields.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean:lecture, field:'name', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',6,['type':("text"),'name':("name"),'bean':(lecture),'value':(lecture?.name),'class':("form-control")],-1)
printHtmlPart(2)
invokeTag('display','f',13,['bean':(lecture),'property':("module.name"),'class':("form-control")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean:lecture, field:'orderNo', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',20,['type':("number"),'min':("1"),'max':("100"),'name':("orderNo"),'bean':(lecture),'value':(lecture?.orderNo),'class':("form-control")],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean:lecture,field:'isVisible', 'has-warning'))
printHtmlPart(5)
invokeTag('widget','f',27,['bean':(lecture),'property':("isVisible"),'class':("form-check-input")],-1)
printHtmlPart(6)
expressionOut.print(hasErrors(bean:lecture,field:'description', 'has-warning'))
printHtmlPart(7)
invokeTag('field','g',38,['type':("text"),'name':("description"),'bean':(lecture),'value':(lecture?.description),'class':("form-control")],-1)
printHtmlPart(8)
expressionOut.print(hasErrors(bean:lecture,field:'authors', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',42,['type':("text"),'name':("authors"),'bean':(lecture),'value':(lecture?.authors),'class':("form-control")],-1)
printHtmlPart(9)
expressionOut.print(hasErrors(bean:lecture,field:'editors', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',50,['type':("text"),'name':("editors"),'bean':(lecture),'value':(lecture?.editors),'class':("form-control")],-1)
printHtmlPart(10)
createTagBody(1, {->
printHtmlPart(11)
expressionOut.print(lecture?.learningObjectives)
printHtmlPart(12)
})
invokeTag('editor','ckeditor',59,['name':("learningObjectives"),'toolbar':("custom"),'height':("300px"),'width':("100%")],1)
printHtmlPart(13)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636887L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
