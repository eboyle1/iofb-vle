import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_testcreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/test/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'test.label', default: 'Test'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.create.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',10,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',10,['bean':("test"),'property':("lecture.module.course.name")],-1)
})
invokeTag('link','g',10,['controller':("course"),'action':("show"),'id':(test.lecture.module.course.id)],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',10,['bean':("test"),'property':("lecture.module.name")],-1)
})
invokeTag('link','g',10,['controller':("module"),'action':("show"),'id':(test.lecture.module.id)],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',10,['bean':("test"),'property':("lecture.name")],-1)
})
invokeTag('link','g',10,['controller':("lecture"),'action':("listTests"),'id':(test.lecture.id)],2)
printHtmlPart(6)
invokeTag('message','g',13,['code':("default.create.label"),'args':([entityName])],-1)
printHtmlPart(7)
createTagBody(2, {->
printHtmlPart(8)
createTagBody(3, {->
printHtmlPart(9)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(10)
expressionOut.print(error.field)
printHtmlPart(11)
}
printHtmlPart(12)
invokeTag('message','g',21,['error':(error)],-1)
printHtmlPart(13)
})
invokeTag('eachError','g',22,['bean':(test),'var':("error")],3)
printHtmlPart(14)
})
invokeTag('hasErrors','g',24,['bean':(test)],2)
printHtmlPart(15)
createTagBody(2, {->
printHtmlPart(16)
invokeTag('hiddenField','g',29,['name':("version"),'value':(test?.version)],-1)
printHtmlPart(17)
invokeTag('render','g',32,['template':("form_fields_create")],-1)
printHtmlPart(18)
expressionOut.print(message(code: 'default.button.create.label', default: 'Create'))
printHtmlPart(19)
createClosureForHtmlPart(20, 3)
invokeTag('link','g',36,['controller':("lecture"),'action':("listTests"),'id':(test.lecture.id),'class':("btn btn-default pull-right")],3)
printHtmlPart(21)
})
invokeTag('form','g',38,['url':([resource:test, action:'save']),'class':("form-horizontal"),'method':("POST")],2)
printHtmlPart(22)
})
invokeTag('captureBody','sitemesh',42,[:],1)
printHtmlPart(23)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636112L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
