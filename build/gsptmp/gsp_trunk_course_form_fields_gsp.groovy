import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_course_form_fields_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/course/_form_fields.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean:course, field:'name', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',6,['type':("text"),'name':("name"),'bean':(course),'value':(course?.name),'class':("form-control")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean:course, field:'orderNo', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',13,['type':("number"),'min':("1"),'max':("100"),'name':("orderNo"),'bean':(course),'value':(course?.orderNo),'class':("form-control")],-1)
printHtmlPart(3)
expressionOut.print(hasErrors(bean:course,field:'isVisible', 'has-warning'))
printHtmlPart(4)
invokeTag('widget','f',20,['bean':(course),'property':("isVisible"),'class':("form-check-input")],-1)
printHtmlPart(5)
createTagBody(1, {->
printHtmlPart(6)
expressionOut.print(course?.welcomePage)
printHtmlPart(7)
})
invokeTag('editor','ckeditor',27,['name':("welcomePage"),'toolbar':("custom"),'height':("300px"),'width':("100%")],1)
printHtmlPart(8)
createTagBody(1, {->
printHtmlPart(6)
expressionOut.print(course?.staffPage)
printHtmlPart(7)
})
invokeTag('editor','ckeditor',36,['name':("staffPage"),'toolbar':("custom"),'height':("300px"),'width':("100%")],1)
printHtmlPart(9)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636304L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
