import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_shared_templates_test_head_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/shared-templates/_test_head.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createClosureForHtmlPart(1, 1)
invokeTag('link','g',1,['controller':("course"),'action':("index")],1)
printHtmlPart(2)
createTagBody(1, {->
invokeTag('display','f',1,['bean':("test"),'property':("lecture.module.course.name")],-1)
})
invokeTag('link','g',1,['controller':("course"),'action':("show"),'id':(test.lecture.module.course.id)],1)
printHtmlPart(2)
createTagBody(1, {->
invokeTag('display','f',1,['bean':("test"),'property':("lecture.module.name")],-1)
})
invokeTag('link','g',1,['controller':("module"),'action':("show"),'id':(test.lecture.module.id)],1)
printHtmlPart(2)
createTagBody(1, {->
invokeTag('display','f',1,['bean':("test"),'property':("lecture.name")],-1)
})
invokeTag('link','g',1,['controller':("lecture"),'action':("listTests"),'id':(test.lecture.id)],1)
printHtmlPart(2)
expressionOut.print(test?.name)
printHtmlPart(3)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636581L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
