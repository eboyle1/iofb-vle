import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_questioncreate_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/question/create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'question.label', default: 'Question'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.create.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(1)
invokeTag('resources','ckeditor',7,[:],-1)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',8,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
invokeTag('render','g',11,['template':("/shared-templates/test_head")],-1)
printHtmlPart(4)
invokeTag('message','g',14,['code':("default.create.label"),'args':([entityName])],-1)
printHtmlPart(5)
createTagBody(2, {->
printHtmlPart(6)
createTagBody(3, {->
printHtmlPart(7)
if(true && (error in org.springframework.validation.FieldError)) {
printHtmlPart(8)
expressionOut.print(error.field)
printHtmlPart(9)
}
printHtmlPart(10)
invokeTag('message','g',22,['error':(error)],-1)
printHtmlPart(11)
})
invokeTag('eachError','g',23,['bean':(this.question),'var':("error")],3)
printHtmlPart(12)
})
invokeTag('hasErrors','g',26,['bean':(this.question)],2)
printHtmlPart(13)
createTagBody(2, {->
printHtmlPart(14)
invokeTag('hiddenField','g',30,['name':("ownerOwnerId"),'value':(test?.id)],-1)
printHtmlPart(15)
invokeTag('hiddenField','g',31,['name':("isVisible"),'value':("false")],-1)
printHtmlPart(16)
invokeTag('render','g',33,['template':("form")],-1)
printHtmlPart(17)
expressionOut.print(message(code: 'default.button.create.label', default: 'Create'))
printHtmlPart(18)
createClosureForHtmlPart(19, 3)
invokeTag('link','g',39,['controller':("test"),'action':("show"),'id':(test.id),'class':("btn btn-default pull-right")],3)
printHtmlPart(20)
})
invokeTag('form','g',42,['class':("form-horizontal"),'url':([resource:question, action:'save'])],2)
printHtmlPart(21)
})
invokeTag('captureBody','sitemesh',47,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164637849L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
