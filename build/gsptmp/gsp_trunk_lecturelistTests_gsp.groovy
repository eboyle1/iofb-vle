import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_lecturelistTests_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/lecture/listTests.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
createTagBody(1, {->
printHtmlPart(1)
invokeTag('captureMeta','sitemesh',4,['gsp_sm_xmlClosingForEmptyTag':("/"),'name':("layout"),'content':("main")],-1)
printHtmlPart(1)
invokeTag('set','g',5,['var':("entityName"),'value':(message(code: 'lecture.label', default: 'Lecture'))],-1)
printHtmlPart(1)
createTagBody(2, {->
createTagBody(3, {->
invokeTag('message','g',6,['code':("default.show.label"),'args':([entityName])],-1)
})
invokeTag('captureTitle','sitemesh',6,[:],3)
})
invokeTag('wrapTitleTag','sitemesh',6,[:],2)
printHtmlPart(2)
})
invokeTag('captureHead','sitemesh',7,[:],1)
printHtmlPart(2)
createTagBody(1, {->
printHtmlPart(3)
createClosureForHtmlPart(4, 2)
invokeTag('link','g',12,['controller':("course"),'action':("index")],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',12,['bean':("lecture"),'property':("module.course.name")],-1)
})
invokeTag('link','g',12,['controller':("course"),'action':("show"),'id':(lecture.module.course.id)],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',12,['bean':("lecture"),'property':("module.name")],-1)
})
invokeTag('link','g',12,['controller':("module"),'action':("show"),'id':(lecture.module.id)],2)
printHtmlPart(5)
createTagBody(2, {->
invokeTag('display','f',12,['bean':("lecture"),'property':("name")],-1)
})
invokeTag('link','g',12,['controller':("lecture"),'action':("show"),'id':(lecture.id)],2)
printHtmlPart(6)
invokeTag('display','f',15,['bean':("lecture"),'property':("name")],-1)
printHtmlPart(7)
createClosureForHtmlPart(8, 2)
invokeTag('link','g',18,['controller':("test"),'class':("btn btn-nav"),'action':("create"),'params':([lecture:lecture.id])],2)
printHtmlPart(9)
loop:{
int i = 0
for( testInstance in (lecture.tests.sort{it.orderNo}) ) {
printHtmlPart(10)
expressionOut.print(testInstance.name)
printHtmlPart(11)
expressionOut.print(testInstance.description)
printHtmlPart(12)
createTagBody(3, {->
printHtmlPart(13)
createClosureForHtmlPart(14, 4)
invokeTag('link','g',51,['controller':("test"),'action':("edit"),'id':(testInstance.id),'class':("btn btn-primary")],4)
printHtmlPart(15)
createClosureForHtmlPart(16, 4)
invokeTag('link','g',55,['controller':("test"),'action':("show"),'resource':(testInstance),'class':("btn btn-primary")],4)
printHtmlPart(17)
createClosureForHtmlPart(18, 4)
invokeTag('link','g',56,['controller':("userTest"),'params':([testId:testInstance.id]),'action':("beginTest"),'class':("btn btn-nav pull-right")],4)
printHtmlPart(19)
})
invokeTag('form','g',60,['url':([resource:testInstance, action:'delete']),'method':("DELETE")],3)
printHtmlPart(20)
i++
}
}
printHtmlPart(21)
})
invokeTag('captureBody','sitemesh',60,[:],1)
printHtmlPart(22)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636913L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
