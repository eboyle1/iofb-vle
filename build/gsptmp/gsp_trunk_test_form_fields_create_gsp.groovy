import grails.plugins.metadata.GrailsPlugin
import org.grails.gsp.compiler.transform.LineNumber
import org.grails.gsp.GroovyPage
import org.grails.web.taglib.*
import org.grails.taglib.GrailsTagException
import org.springframework.web.util.*
import grails.util.GrailsUtil

class gsp_trunk_test_form_fields_create_gsp extends GroovyPage {
public String getGroovyPageFileName() { "/WEB-INF/grails-app/views/test/_form_fields_create.gsp" }
public Object run() {
Writer out = getOut()
Writer expressionOut = getExpressionOut()
registerSitemeshPreprocessMode()
printHtmlPart(0)
expressionOut.print(hasErrors(bean:test, field:'name', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',6,['type':("text"),'name':("name"),'bean':(test),'value':(""),'class':("form-control")],-1)
printHtmlPart(2)
expressionOut.print(hasErrors(bean:test, field:'lecture', 'has-warning'))
printHtmlPart(3)
invokeTag('widget','f',14,['bean':(test),'property':("lecture"),'class':("form-check-input")],-1)
printHtmlPart(4)
expressionOut.print(hasErrors(bean:test, field:'orderNo', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',22,['type':("number"),'min':("1"),'max':("100"),'name':("orderNo"),'bean':(test),'value':(""),'class':("form-control")],-1)
printHtmlPart(5)
expressionOut.print(hasErrors(bean:test,field:'isVisible', 'has-warning'))
printHtmlPart(6)
invokeTag('widget','f',29,['bean':(test),'property':("isVisible"),'class':("form-check-input")],-1)
printHtmlPart(7)
expressionOut.print(hasErrors(bean:test,field:'description', 'has-warning'))
printHtmlPart(1)
invokeTag('field','g',36,['type':("text"),'name':("description"),'bean':(test),'value':(""),'class':("form-control")],-1)
printHtmlPart(8)
}
public static final Map JSP_TAGS = new HashMap()
protected void init() {
	this.jspTags = JSP_TAGS
}
public static final String CONTENT_TYPE = 'text/html;charset=UTF-8'
public static final long LAST_MODIFIED = 1518164636090L
public static final String EXPRESSION_CODEC = 'html'
public static final String STATIC_CODEC = 'none'
public static final String OUT_CODEC = 'none'
public static final String TAGLIB_CODEC = 'none'
}
