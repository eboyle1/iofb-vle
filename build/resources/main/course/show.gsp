<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div role="main">
            <p><g:link controller="course" action="index">Courses</g:link> > <f:display bean="course" property="name" /></p>
        <div class = "title_left">
            <h3><%--Modules for course:<br/>--%> <f:display bean="course" property="name" /></h3>
            <ul>
            <li><g:link controller="course" action="showWelcomePage" id="${course.id}">Welcome</g:link></li>
            <li><g:link controller="course" action="showStaffPage" id="${course.id}">Course Staff</g:link></li>
            </ul>
        </div>
        <div class = "title_right">
            <div class ="course_pages">
            <%--
            <ul>
            <li><g:link controller="course" action="showWelcomePage" id="${course.id}">Welcome</g:link></li>
            <li><g:link controller="course" action="showStaffPage" id="${course.id}">Course Staff</g:link></li>
            </ul>
            --%>
            </div>
            <sec:ifAnyGranted roles="ROLE_ADMIN"> 
            <g:link controller="module" class="create" action="create" params="[course:course.id]" class="btn btn-nav">New module</g:link>
            </sec:ifAnyGranted>
        </div>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>    
                    <%--<g:sortableColumn property="name" title="Modules" />--%>
                    <th><div class="name-header">Modules</div></th>
                    <sec:ifAnyGranted roles="ROLE_ADMIN">   
                    <th><div class="action-header">Action</div></th>
                    </sec:ifAnyGranted>
                </tr>
            </thead>
            <tbody>
                <g:each in="${course.modules.sort{it.orderNo}}" status="i" var="moduleInstance">
                <g:if test="${moduleInstance.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]'}">
                    <tr>
                        <td>
                        <sec:ifAnyGranted roles="ROLE_ADMIN"> 
                            ${moduleInstance.orderNo}. <g:link controller="module" action="show" id="${moduleInstance.id}">${moduleInstance.name}</g:link>
                        </sec:ifAnyGranted>
                        <sec:ifAnyGranted roles="ROLE_USER">
                             ${moduleInstance.orderNo}. <g:link controller="module" action="studentView" id="${moduleInstance.id}">${moduleInstance.name}</g:link>
                        </sec:ifAnyGranted>
                        </td>
                        <td>
                        <sec:ifAnyGranted roles="ROLE_ADMIN"> 
                        <div class="action">
                        <div class="dropdown">
                        <button class="dropbtn" type="button" data-toggle="dropdown">Select <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><g:link controller="module" action="show" id="${moduleInstance.id}">List lectures</g:link></li>
                            <li><g:link controller="lecture" class="create" action="create" params="[module:moduleInstance.id]">New lecture</g:link></li>
                            <li><g:link controller="module" action="studentView" id="${moduleInstance.id}">View module</g:link></li>
                            <li><g:link controller="module" action="edit" id="${moduleInstance.id}">Edit module</g:link></li>
                            <li><g:form controller="module" id="${moduleInstance.id}" method="DELETE">
<%--
                                <g:actionSubmit class="delete-button" action="delete" value="Delete module" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
--%>
                                <p:renderModuleDeleteLink curModule="${moduleInstance}"/>
                            </g:form></li>
                        </ul>
                        </div>                            
                        </div>
                        </sec:ifAnyGranted>
                        </td>
                    </tr>
                </g:if>
                </g:each>
            </tbody>
        </table>
<%--
        <div class="pagination">
            <p:bootstrapPagination total="${course.modules?.size() ?: 0}" domainBean="uk.ac.ed.bric.elearn.vle.Course" />
        </div>
--%>
        <%--to do: make these active links at the appropriate times--%>
        <g:if test="${course.id == 1}">
        <ul>
        <li>Practice assessment (You can only take this assessment when you have achieved a 100% mark in all the course tests)</li>
        <li>Exit assessment (You can only take this assessment when you have completed the practice assessment)</li>
        <li>Exit survey (You must complete this before you can be given the course certificate)</li>
        <li>Course certificate</li>
        </ul>
        </g:if>
        <i class="fa fa-envelope"></i> <a href="mailto:Imaging.Academy@ed.ac.uk">Contact user support</a></li>
    </body>
</html>
