<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:course, field:'name', 'has-warning')}">
                    <g:field type="text" name="name" bean="${course}" value="" class="form-control" />
             </div>
        </div>

        <div class="form-group">
            <label for="orderNo" class="col-md-2 control-label">Order no. <span class="required-indicator">*</span></label>
             <div class="col-md-2 ${hasErrors(bean:course, field:'orderNo', 'has-warning')}">
                    <g:field type="number" min="1" max="100" name="orderNo" bean="${course}" value="" class="form-control" />
             </div>
        </div>
<%--
		<div class="form-group">
	        <label for="code" class="col-md-2 control-label">Code <span class="required-indicator">*</span></label>
	         <div class="col-md-10 ${hasErrors(bean:course,field:'code', 'has-warning')}">
	         		<g:field type="text" name="code" bean="${course}" value="" class="form-control" />
	         </div>
	    </div>
--%>
        <div class="form-group">
            <label for="isVisible" class="col-md-2 control-label">Published </label>
             <div class="col-md-10 ${hasErrors(bean:course,field:'isVisible', 'has-warning')}">
                    <div class="form-checkbox"><f:widget bean="${course}" property="isVisible" class="form-check-input" /></div>
             </div>
        </div>
       

        <div class="form-group">
            <label for="description" class="col-md-2 control-label">Welcome page </label>
             <div class="col-md-10">
                <ckeditor:editor name="welcomePage" toolbar="custom" height="300px" width="100%">

                </ckeditor:editor>
             </div>
        </div>

        <div class="form-group">
            <label for="description" class="col-md-2 control-label">Staff page </label>
             <div class="col-md-10">
                <ckeditor:editor name="staffPage" toolbar="custom" height="300px" width="100%">

                </ckeditor:editor>
             </div>
        </div>


    </div>