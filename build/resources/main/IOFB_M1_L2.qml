<QML>

<QUESTION ID="7475061269405970" DESCRIPTION="IOFB_M1L2_Q1_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L2 Technique" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">With regards to acquiring an orbital radiograph of good quality, which of the following are recommended steps in the acquisition technique? Answer the following as TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Antero-posterior projection</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Radiographic baseline of 45 degrees</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Straight tube</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Skull rotated to ensure projection along orbital axis</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Eyes up and open</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">A = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">D = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">E = FALSE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="3293926558764119" DESCRIPTION="IOFB_M1L2_Q2_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L2 Technique" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When assessing orbital radiograph quality, which of the following are indicators of good technique? Answer the following as TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Petrous ridges project superiorly to the inferior orbital rim</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Gridlines clearly visible</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Underpenetration of soft tissue structures</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Patient rotated 45 degrees to the right</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Mastoid sinuses project inferiorly to orbits</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">A = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">B = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">C = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">D = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="5043426426174718" DESCRIPTION="IOFB_M1L2_Q3_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L2 Technique" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Following the recommended referring / reporting procedure, put the following in order from first to last step:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">A. Orbital x-ray checked by competent IOFB reporting radiographer</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">B. Orbital x-ray performed</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">C. Orbital x-ray reported as per normal procedure</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">D. Orbits request made by appropriate referrer</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">E. Note and signature made on form</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "3"</CONDITION>
    <CONTENT TYPE="text/html">A = 3</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "2"</CONDITION>
    <CONTENT TYPE="text/html">B = 2</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "5"</CONDITION>
    <CONTENT TYPE="text/html">C = 5</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "1"</CONDITION>
    <CONTENT TYPE="text/html">D = 1</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "4"</CONDITION>
    <CONTENT TYPE="text/html">E = 4</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="7523001485727103" DESCRIPTION="IOFB_M1L2_Q4_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L2 Technique" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">On MR scanning, you obtain the image below. Put the following steps which you should follow into the recommended sequence order from start to finish:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">A. Remove the patient from the MR scanner</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">B. Remove the patient from the MR scanner room</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">C. Sit the patient up</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">D. Stop scanning</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>1</OPTION>
      <OPTION>2</OPTION>
      <OPTION>3</OPTION>
      <OPTION>4</OPTION>
      <OPTION>5</OPTION>
      <CONTENT TYPE="text/html">E. Alert the responsible physician</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "2"</CONDITION>
    <CONTENT TYPE="text/html">A = 2</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "3"</CONDITION>
    <CONTENT TYPE="text/html">B = 3</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "4"</CONDITION>
    <CONTENT TYPE="text/html">C = 4</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "1"</CONDITION>
    <CONTENT TYPE="text/html">D = 1</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "5"</CONDITION>
    <CONTENT TYPE="text/html">E = 5</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="8187600891251350" DESCRIPTION="IOFB_M1L2_Q5_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L2 Technique" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Which of the following will exacerbate (make worse) a situation where a patient is discovered to have metallic signal drop out over an orbit during a MR scan:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Performing a set of T2* sequences to make sure</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Shouting to the patient to get out of the scanner quickly</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Sitting the patient up on the scanner table as soon as the patient is out of the scanner bore</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Keeping the patient supine / horizontal until out of the MR scanner room</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Quenching the magnet</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">D = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
