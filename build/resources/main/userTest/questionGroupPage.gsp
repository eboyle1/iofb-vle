<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>

        <script type="text/javascript">
	    $(function() {
	            $('#questiondiv').load('${grailsApplication.config.vle.url}/userTest/displayQuestionGroup?testId=${test.id}&questionGroupId=${questionGroup.id}&userTestSessionId=${userTestSession.id}',{nocache: new Date().getTime()});        
	    });

		function postAnswer(action, params) {
			var data = $.param(params)
			var response
			$.post(action, data, function(response) {
				$( "#questiondiv" ).html( response );
			})
			.fail(function() {
				$( "#questiondiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
			})		
		};

	    function previous(questionGroupId, testId, userTestSessionId, action) {
        	var params = {questionGroupId:questionGroupId,testId:testId,userTestSessionId:userTestSessionId}
        	postAnswer(action, params)

    	};

    	function next(questionGroupId, testId, userTestSessionId, action) {
    		var params
    		var selectedChoices = [] //needs to be an array for collecting up to 5 choice answers per question group
    		$(":radio").each(function () {
				if(this.checked) {
					selectedChoices.push(this.id);
				}
			});
			params = {questionGroupId:questionGroupId,testId:testId,userTestSessionId:userTestSessionId,selectedChoices:selectedChoices}
   			postAnswer(action, params)
    	};
    	</script>

    </head>
    <body>

    <div role="main">
        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${test.lecture.module.course.id}"><f:display bean="test" property="lecture.module.course.name" /></g:link> > <g:link controller="module" action="studentView" id="${test.lecture.module.id}"><f:display bean="test" property="lecture.module.name" /></g:link> > <f:display bean="test" property="name" /></p>
    <div class = "title_left">
          <h3>Test:<br/><f:display bean="test" property="name" /></h3>
    </div>
    <div class = "title_right">
        <g:link controller="test.lecture.module" id="${test.lecture.module.id}" class="create" action="studentView" class="btn btn-nav">View module</g:link>
    </div>

    </div>

	<div id="questiondiv"></div>

    </body>
</html>
