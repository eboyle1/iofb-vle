<div id="questiondiv">

  	<g:if test="${flash.message}">
		<div class="alert alert-dismissable alert-info">
			<button type="button" class="close" data-dismiss="alert">×</button>
			${flash.message}
		</div>
	</g:if>
	<g:if test="${flash.error}">
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			${flash.error}
		</div>
	</g:if>

	<div class="listQuestions">

	<p>You answered ${userTestSession.answers.size()} of ${userTestSession.totalQuestions} questions.</p>

	<p>Result: ${userTestSession.mark} %</p>

	<g:if test="${testsComplete}">
	<p>You have successfully achieved a mark of 100% in all tests for this course.</p>
	</g:if>

	</div>

</div>