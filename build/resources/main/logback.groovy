import grails.util.BuildSettings
import grails.util.Environment

def TOMCAT_HOME = System.getProperty("catalina.base") ?: BuildSettings.TARGET_DIR

// See http://logback.qos.ch/manual/groovy.html for details on configuration
appender('STDOUT', ConsoleAppender) {
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }
}

root(ERROR, ['STDOUT'])

appender("FULL_STACKTRACE", FileAppender) {
    file = "${TOMCAT_HOME}/logs/stacktrace.log"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }
}
appender("VLE_DEBUG", FileAppender) {
    file = "${TOMCAT_HOME}/logs/vle.log"
    append = true
    encoder(PatternLayoutEncoder) {
        pattern = "%level %logger - %msg%n"
    }    
}
logger("StackTrace", ERROR, ['FULL_STACKTRACE'], false)
logger 'grails.app.controllers', DEBUG, ['VLE_DEBUG']
