<QML>

<QUESTION ID="7513435330202934" DESCRIPTION="IOFB_M2L1_Q16_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the labelled structures in this coronal MR section through the orbits:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic nerve</OPTION>
      <OPTION>Superior oblique muscle</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic nerve</OPTION>
      <OPTION>Superior oblique muscle</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic nerve</OPTION>
      <OPTION>Superior oblique muscle</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic nerve</OPTION>
      <OPTION>Superior oblique muscle</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Extra conal fat</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Intra conal fat</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Ophthalmic artery</OPTION>
      <OPTION>Optic nerve</OPTION>
      <OPTION>Superior oblique muscle</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Superior rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">A = Superior rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Superior oblique muscle"</CONDITION>
    <CONTENT TYPE="text/html">B = Superior oblique muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Medial rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">C = Medial rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Inferior rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">D = Inferior rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Optic nerve"</CONDITION>
    <CONTENT TYPE="text/html">E = Optic nerve</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="5980405217792593" DESCRIPTION="IOFB_M2L1_Q17_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Regarding the function of the muscles associated with the orbit, match the following:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Inferior oblique</OPTION>
      <OPTION>Inferior rectus</OPTION>
      <OPTION>Lateral rectus</OPTION>
      <OPTION>Levator palpebrae superioris</OPTION>
      <OPTION>Medial rectus</OPTION>
      <OPTION>Orbicularis oculi</OPTION>
      <OPTION>Superior oblique</OPTION>
      <OPTION>Superior rectus</OPTION>
      <CONTENT TYPE="text/html">A. Eyes roll, look down and to the side</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Inferior oblique</OPTION>
      <OPTION>Inferior rectus</OPTION>
      <OPTION>Lateral rectus</OPTION>
      <OPTION>Levator palpebrae superioris</OPTION>
      <OPTION>Medial rectus</OPTION>
      <OPTION>Orbicularis oculi</OPTION>
      <OPTION>Superior oblique</OPTION>
      <OPTION>Superior rectus</OPTION>
      <CONTENT TYPE="text/html">B. Closes eye</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Inferior oblique</OPTION>
      <OPTION>Inferior rectus</OPTION>
      <OPTION>Lateral rectus</OPTION>
      <OPTION>Levator palpebrae superioris</OPTION>
      <OPTION>Medial rectus</OPTION>
      <OPTION>Orbicularis oculi</OPTION>
      <OPTION>Superior oblique</OPTION>
      <OPTION>Superior rectus</OPTION>
      <CONTENT TYPE="text/html">C. Eyes look down</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Inferior oblique</OPTION>
      <OPTION>Inferior rectus</OPTION>
      <OPTION>Lateral rectus</OPTION>
      <OPTION>Levator palpebrae superioris</OPTION>
      <OPTION>Medial rectus</OPTION>
      <OPTION>Orbicularis oculi</OPTION>
      <OPTION>Superior oblique</OPTION>
      <OPTION>Superior rectus</OPTION>
      <CONTENT TYPE="text/html">D. Eyes rotate laterally</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Inferior oblique</OPTION>
      <OPTION>Inferior rectus</OPTION>
      <OPTION>Lateral rectus</OPTION>
      <OPTION>Levator palpebrae superioris</OPTION>
      <OPTION>Medial rectus</OPTION>
      <OPTION>Orbicularis oculi</OPTION>
      <OPTION>Superior oblique</OPTION>
      <OPTION>Superior rectus</OPTION>
      <CONTENT TYPE="text/html">E. Eyes look up</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Superior oblique"</CONDITION>
    <CONTENT TYPE="text/html">A = Superior oblique</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Orbicularis oculi"</CONDITION>
    <CONTENT TYPE="text/html">B = Orbicularis oculi</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Inferior rectus"</CONDITION>
    <CONTENT TYPE="text/html">C = Inferior rectus</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Lateral rectus"</CONDITION>
    <CONTENT TYPE="text/html">D = Lateral rectus</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Superior rectus"</CONDITION>
    <CONTENT TYPE="text/html">E = Superior rectus</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="9008947206305907" DESCRIPTION="IOFB_M2L1_Q18_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the soft tissue structures located approximately where the labels indicate:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Inferior oblique muscle</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Maxillary nerve (CN V2)</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Oculomotor nerve (CN III)</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Pterygoid nerve</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <OPTION>Zygomatic nerve</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Superior rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">A = Superior rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Optic nerve (CN II)"</CONDITION>
    <CONTENT TYPE="text/html">B = Optic nerve (CN II)</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Oculomotor nerve (CN III)"</CONDITION>
    <CONTENT TYPE="text/html">C = Oculomotor nerve (CN III)</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Zygomatic nerve"</CONDITION>
    <CONTENT TYPE="text/html">D = Zygomatic nerve</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Maxillary nerve (CN V2)"</CONDITION>
    <CONTENT TYPE="text/html">E = Maxillary nerve (CN V2)</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="0335569587397265" DESCRIPTION="IOFB_M2L1_Q19_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html"><P>Identify the labelled structures in this axial CT section through the orbits:</P></CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">A</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">B</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">C</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">D</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Ethmoid air cell</OPTION>
      <OPTION>Frontal sinus</OPTION>
      <OPTION>Globe</OPTION>
      <OPTION>Inferior rectus muscle</OPTION>
      <OPTION>Lateral rectus muscle</OPTION>
      <OPTION>Lens</OPTION>
      <OPTION>Medial rectus muscle</OPTION>
      <OPTION>Optic nerve (CN II)</OPTION>
      <OPTION>Sphenoid sinus</OPTION>
      <OPTION>Superior rectus muscle</OPTION>
      <CONTENT TYPE="text/html">E</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Lens"</CONDITION>
    <CONTENT TYPE="text/html">A = Lens</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Globe"</CONDITION>
    <CONTENT TYPE="text/html">B = Globe</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Medial rectus muscle"</CONDITION>
    <CONTENT TYPE="text/html">C = Medial rectus muscle</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Optic nerve (CN II)"</CONDITION>
    <CONTENT TYPE="text/html">D = Optic nerve (CN II)</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Sphenoid sinus"</CONDITION>
    <CONTENT TYPE="text/html">E = Sphenoid sinus</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="0216282117199677" DESCRIPTION="IOFB_M2L1_Q20_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L1 Anatomy" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Answer the following TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. The lamboid suture separates the occiptal from frontal bones</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. The sagittal suture separates the right and left parietal bones</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. The coronal suture separates the frontal and zygomatic bones</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. The mastoid air cells lie in the temporal bone</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. The frontal sinuses may never form</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">A = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">C = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
