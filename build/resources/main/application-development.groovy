vle.url="http://localhost:8080"
file.upload.dir="C:\\Apache24\\htdocs\\lectureFiles"
image.upload.dir="C:\\Apache24\\htdocs\\images"
external.webserver="http://localhost"

sso.client.home = "http://localhost:8080/login/sso"
elearn.apps.home = "http://localhost:8080"
grails.plugin.springsecurity.sso.authenticationPoint = 'http://localhost:8090/login/auth'

grails {
   mail {
    host = "smtp.gmail.com"
	port = "465"
    username = "edward.boyle7@gmail.com"
    password = ""
    props = ["mail.smtp.auth":"true", 					   
              "mail.smtp.socketFactory.port":"465",
              "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
              "mail.smtp.socketFactory.fallback":"false"]
   }
}

//vleTests.email.to = 'Imaging.Academy@ed.ac.uk'
vleTests.email.to = 'edward.boyle@ed.ac.uk'
vleTests.email.from = 'Imaging.Academy@ed.ac.uk'
vleTests.email.subject = 'Notification of IOFB review course successful lecture test suite completion'
vleTests.email.body = '''\
<p>This student on the IOFB review course has now achieved 100% in all of the lecture tests:</p>
<p>$lastname, $firstname</p> 
'''