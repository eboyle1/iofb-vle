<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'question.label', default: 'Question')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
        <ckeditor:resources/>
    </head>
    <body>

        <g:render template="/shared-templates/test_head"/>
        
        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.create.label" args="[entityName]" /></div>
                <div class="panel-body">

                    <g:hasErrors bean="${this.question}">
                    <div class="alert alert-danger">
                        <p>Error(s)</p>
                        <ul  role="alert">
                            <g:eachError bean="${this.question}" var="error">
                            <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                            </g:eachError>
                        </ul>
                    </div>
                    </g:hasErrors>
   
                    <g:form class="form-horizontal" url="[resource:question, action:'save']">

                    <g:hiddenField name="ownerOwnerId" value="${test?.id}"/>
                    <g:hiddenField name="isVisible" value="false"/>
                    <fieldset class="form">
                        <g:render template="form"/>
                    </fieldset>
                    <br/>

                    <div class="btn-toolbar">
                        <button type="submit" class="btn btn-primary pull-right" value="${message(code: 'default.button.create.label', default: 'Create')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Create</button>
                        <g:link controller="test" action="show" id="${test.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link> 
                    </div>

                    </g:form>
                </div>  
            </div>
        </div>

    </body>
</html>
