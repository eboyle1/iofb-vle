<QML>

<QUESTION ID="3705590909421453" DESCRIPTION="IOFB_M2L2_Q1_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L2 IRMER" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">The purpose of IR(ME)R 2000 plus subsequent amendments, is to protect each patient from the harms of ionizing radiation. Which of the following principles translate that purpose into practice? Answer using TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Hold employers responsible, rather than individuals, for their actions</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Justify each medical radiation exposure in terms of risk / benefit to the patient</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Prosecute transgressions in the civil courts</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Ensure the radiation dose delivered is as low as reasonably achievable</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Devolve enforcement to local authorities</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">A = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">C = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="9738719927966276" DESCRIPTION="IOFB_M2L2_Q2_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L2 IRMER" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When considering the radiology pathway and needing to obtain an orbital radiograph in the context of detecting a metallic IOFB, match the following actions to the most relevant duty holder role which the person takes on in executing the actions:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">A. Requesting the order</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">B. Justifying the order</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">C. Authorising the order</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">D. Executing the order</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">e. Documenting the dose</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Referrer"</CONDITION>
    <CONTENT TYPE="text/html">A = Referrer</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Practitioner"</CONDITION>
    <CONTENT TYPE="text/html">B = Practitioner</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Practitioner"</CONDITION>
    <CONTENT TYPE="text/html">C = Practitioner</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Operator"</CONDITION>
    <CONTENT TYPE="text/html">D = Operator</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Operator"</CONDITION>
    <CONTENT TYPE="text/html">E = Operator</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="3818154973936827" DESCRIPTION="IOFB_M2L2_Q3_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L2 IRMER" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When considering responsibilities under IR(ME)R, match the following responsibilities to the most relevant duty holder:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">A. Establish a standard operating procedure (SOP)</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">B. Provide sufficient relevant clinical information</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">C. Ensure compliance with IR(ME)R regulations</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">D. Authorise medical radiation exposure</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">E. Optimise patient dosimetry</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Employer"</CONDITION>
    <CONTENT TYPE="text/html">A = Employer</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Referrer"</CONDITION>
    <CONTENT TYPE="text/html">B = Referrer</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Employer"</CONDITION>
    <CONTENT TYPE="text/html">C = Employer</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Practitioner"</CONDITION>
    <CONTENT TYPE="text/html">D = Practitioner</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Medical Physics Expert"</CONDITION>
    <CONTENT TYPE="text/html">E = Medical Physics Expert</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="4668529835973536" DESCRIPTION="IOFB_M2L2_Q4_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L2 IRMER" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When considering responsibilities under IR(ME)R, match the following responsibilities to the most relevant duty holder:</CONTENT>
  <ANSWER QTYPE="SEL">
    <CHOICE ID="0">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">A. Advise on quality assurance</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">B. Ensure clinical audit provisions are in place</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">C. Position patient for orbital radiograph</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">D. Justify medical radiation exposure</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>Employer</OPTION>
      <OPTION>Referrer</OPTION>
      <OPTION>Practitioner</OPTION>
      <OPTION>Operator</OPTION>
      <OPTION>Medical Physics Expert</OPTION>
      <CONTENT TYPE="text/html">E. Record the radiation dose</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "Medical Physics Expert"</CONDITION>
    <CONTENT TYPE="text/html">A = Medical Physics Expert</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "Employer"</CONDITION>
    <CONTENT TYPE="text/html">B = Employer</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "Operator"</CONDITION>
    <CONTENT TYPE="text/html">C = Operator</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "Practitioner"</CONDITION>
    <CONTENT TYPE="text/html">D = Practitioner</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "Operator"</CONDITION>
    <CONTENT TYPE="text/html">E = Operator</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="3752366003449404" DESCRIPTION="IOFB_M2L2_Q5_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M2L2 IRMER" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">When optimising an image for the purposes of screening for metallic IOFB, which of the following must be considered? Answer using TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Diagnostic reference levels</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Collimation</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Exposure selection</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Recording the exposure in a RIS</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Evaluation of the exposure</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION
      >"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
