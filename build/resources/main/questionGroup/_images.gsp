<div id="optionsDiv">
	<g:if test="${flash.ajaxerror}">
		<div class="alert alert-dismissable alert-danger">
			<button type="button" class="close" data-dismiss="alert">×</button>
			${flash.ajaxerror}
		</div>
	</g:if>

	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-8"><p><strong>Current images:</strong></p></div>
	</div>
		
	<div class="row">
		<div class="col-md-1"></div>
		<div class="col-md-9">
			<g:if test="${questionGroup.qGImages}">
				<ul class="list-group">
					<g:each in="${questionGroup.qGImages}" var="qGImage">
					 	<li class="list-group-item">
<%--
					 	${qGImage.filePath} (display text: ${qGImage.filePath})
--%>
					 	<img src="<g:resource base="${grailsApplication.config.external.webserver}" dir="images" file="${qGImage.fileName}"/>" width="100" />

					 	<div class="delete-buttons-images">
					 	<a href="#" class="pull-right text-danger" onClick="deleteOption('${qGImage?.id}')">
					 	<button class="btn btn-danger">
					 	<i class="fa fa-trash-o" aria-hidden="true"></i> Delete
					 	</button>
					 	</a>
					 	</div>

						</li>
					</g:each>
				</ul>
			</g:if>
			<g:else>
				<p><em>No images added yet</em></p>
			</g:else>
		</div>
	</div>
</div>