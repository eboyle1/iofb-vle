<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'questionGroup.label', default: 'QuestionGroup')}" />
        <title><g:message code="default.edit.label" args="[entityName]" /></title>
        <ckeditor:resources/>
        <script type="text/javascript">
            
            function deleteOption(qGImageId) {
                var params = {qGImageId:qGImageId}
                handleOption(params,"${grailsApplication.config.vle.url}/QuestionGroup/deleteQuestionGroupImage")
            };

            function handleOption(params, action) {
                var data = $.param(params)
                var response
                $.post(action,data, function(response) {
                    $('#optionsDiv').html( response )
                })  
                .fail(function() {
                    $( "#optionsDiv" ).html("<p><h3 class='text-danger'>An unexpected error occurred. Please contact user support.</h3></p>")
                })   
            };
    
        </script>
    </head>
    <body>

        <g:render template="/shared-templates/test_head" model="['test':questionGroup.owner]"/>
        <div class="panel panel-primary">
            <div class="panel-heading">Edit Question Group</div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                <g:hasErrors bean="${questionGroup}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${questionGroup}" var="error">
                            <p <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></p>
                        </g:eachError>
                    </div>
                </g:hasErrors>              
                </ul>
            
                <g:form role="form" class="form-horizontal" url="[resource:questionGroup, action:'update']" method="PUT">

                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <BR>
                <fieldset class="buttons pull-right">
              
                <g:link controller="test" action="show" id="${questionGroup.owner.id}" class="btn btn-default" ><i class='fa fa-times-circle' aria-hidden='true'></i> Cancel</g:link>

                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o" aria-hidden="true"></i> Update</button>
          
                </fieldset>
                </g:form>
                <br/>
                <hr/>

                <ul class="errors" role="alert">
                <g:hasErrors bean="${imageCommand}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${imageCommand}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>

                <div role="main">
                <div class = "title_left">
                <g:uploadForm action="uploadQuestionGroupImage">
                    <input type="file" class="btn btn-default" name="uploadedQuestionGroupImage" />
                    <br/>
                    <input type="submit" value="Upload" class="btn btn-primary"/>
                    <g:hiddenField name="questionGroupId" value="${questionGroup.id}"/>
                </g:uploadForm>
                </div>
                <div class = "title_right-test">
                <g:link controller="test" id="${questionGroup.owner.id}" class="show" action="show" class="btn btn-nav">Questions</g:link>
                </div>
            </div>

            <br/>

            <g:render template="images" bean="${questionGroup}"/>

        </div>        
    </body>
</html>
