<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title>
       Edinburgh Imaging Academy
    </title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">
    <asset:stylesheet src="application.css"/>
    <noscript>
            <style type="text/css">
                .container {display:none;}
            </style>
            <div class="noscriptmsg">
                You don't have javascript enabled.  You MUST have javascript enabled to use this site.
            </div>
    </noscript>

    <asset:javascript src="application.js"/>
    <g:layoutHead/>
</head>
<body>

    <g:render template="/layouts/header"/>  
    <g:render template="/layouts/content"/>     
    <g:render template="/layouts/footer"/>  
    
</body>
</html>
