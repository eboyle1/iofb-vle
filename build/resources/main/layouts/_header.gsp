<div class="container">		
	<g:render template="/layouts/toolbar"/>  
	<g:render template="/layouts/banner"/> 

	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<ul class="nav navbar-nav">
		  			<li><a href="${createLink(uri: '/course')}"><i class="fa fa-home"></i> Home</a></li>
		  			</li>
		  			
		  		</ul>
			</div>
				<div class="collapse navbar-collapse">
				  <ul class="nav navbar-nav navbar-right">
					
				    	<sec:ifLoggedIn>
							<li><a href="/dummy"><sec:loggedInUserInfo field="username"/> |</a></li>
							<li class="active"><g:link controller='logout' >Sign out</g:link></li>
						</sec:ifLoggedIn>
					    <sec:ifNotLoggedIn>
					    	<li><a href="#dummy">You are not signed in</a></li>
					    	<li class="divider"></li>
					    	<li class="active"><a href="${createLink(uri: '/course')}">Sign In</a></li>
					    </sec:ifNotLoggedIn>
				  </ul>
				</div>
		</div>
	</nav>

</div>
