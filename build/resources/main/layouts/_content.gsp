
	<div id="Content" class="container">
		<g:if test="${flash.message}">
			<div class="alert alert-dismissable alert-info">
				<button type="button" class="close" data-dismiss="alert">×</button>
				${flash.message}
			</div>
		</g:if>
		<g:if test="${flash.error}">
			<div class="alert alert-dismissable alert-danger">
				<button type="button" class="close" data-dismiss="alert">×</button>
				${flash.error}
			</div>
		</g:if>
		
		<!-- Show page's content -->
		<g:layoutBody />

	</div>
