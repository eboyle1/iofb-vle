	<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:test, field:'name', 'has-warning')}">
                    <g:field type="text" name="name" bean="${test}" value="${test?.name}" class="form-control" />
             </div>
        </div>

        <div class="form-group">
             <label for="module" class="col-md-2 control-label">Lecture </label>
             <div class="col-md-10">
                <div class="form-text"><f:display bean="${test}" property="lecture.name" class="form-control" /></div>
             </div>
        </div>

        <div class="form-group">
            <label for="orderNo" class="col-md-2 control-label">Order no. <span class="required-indicator">*</span></label>
             <div class="col-md-2 ${hasErrors(bean:test, field:'orderNo', 'has-warning')}">
                    <g:field type="number" min="1" max="100" name="orderNo" bean="${test}" value="${test?.orderNo}" class="form-control" />
             </div>
        </div>

        <div class="form-group">
            <label for="isVisible" class="col-md-2 control-label">Published </label>
             <div class="col-md-10 ${hasErrors(bean:test,field:'isVisible', 'has-warning')}">
                    <div class="form-checkbox"><f:widget bean="${test}" property="isVisible" class="form-check-input" /></div>
             </div>
        </div>
       
	    <div class="form-group">
            <label for="description" class="col-md-2 control-label">Description <span class="required-indicator">*</span></label>
             <div class="col-md-10 ${hasErrors(bean:test,field:'description', 'has-warning')}">
             		<g:field type="text" name="description" bean="${test}" value="${test?.description}" class="form-control"/>
             		
             </div>
        </div>

     </div>