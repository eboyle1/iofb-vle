<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <p>
        All students IOFB lecture test history
        </p>

        <table class="table table-striped">
            <thead>
                <tr>
                    <g:sortableColumn property="userId" title="User ID" />
                    <g:sortableColumn property="lastName" title="Name" />
                    <g:sortableColumn property="testName" title="Test" />
                    <g:sortableColumn property="mark" title="Highest mark (%)" />
                </tr>
            </thead>
            <tbody>
                <g:each in="${userTestSessions}" status="i" var="userTestSessionInstance">
                    <tr>
                        <td>${userTestSessionInstance.userId}</td>
                        <td>${userTestSessionInstance.firstName} ${userTestSessionInstance.lastName}</td>
                        <td>Name: ${userTestSessionInstance.testName}<br/>Lecture: ${userTestSessionInstance.lectureName}<br/>Module: ${userTestSessionInstance.moduleName}</td>
                        <td>${userTestSessionInstance.mark}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>
 
    </body>
</html>