<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'test.label', default: 'Test')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>


    <g:render template="/shared-templates/test_head" bean="${test}"/>
    <g:render template="/shared-templates/test_controls" bean="${test}"/>
    <g:render template="/shared-templates/question_controls" bean="${test}"/>
  
    <script>
       
    $(document).ready(function() {
        $('a[data-confirm]').click(function(ev) {
            var href = $(this).attr('href');
            if (!$('#dataConfirmModal').length) {
                $('body').append("<div id='dataConfirmModal' class='modal fade' role='dialog' aria-labelledby='dataConfirmLabel aria-hidden='true' tabIndex='-1'><div class='modal-dialog' role='document'><div class='modal-content'> <div class='modal-header'><button type='button' class='close' data-dismiss='modal' aria-hidden='true'>×</button><h3 id='dataConfirmLabel'>Please Confirm</h3></div><div class='modal-body'></div><div class='modal-footer'><button class='btn btn-default' data-dismiss='modal' aria-hidden='true'><i class='fa fa-times-circle' aria-hidden='true'></i> Cancel</button><a class='btn btn-danger' id='dataConfirmOK'><i class='fa fa-trash-o' aria-hidden='true'></i> Delete</a></div></div></div></div>");
            } 
            $('#dataConfirmModal').find('.modal-body').text($(this).attr('data-confirm'));
            $('#dataConfirmOK').attr('href', href);
            $('#dataConfirmModal').modal({show:true});
            return false;
        });
    });
    </script>

    </body>
</html>
