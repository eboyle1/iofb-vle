<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        Students who have 100% in all 8 IOFB lecture tests
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
            <div class="text-info">User ID</div>
            </div>
            <div class="col-md-3">
            <div class="text-info">Name</div>
            </div>
        </div>
        <br/>

        <g:each in="${userAdmins}" status="i" var="userAdminInstance">
            <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-2">
                ${userAdminInstance[0]}
            </div>
            <div class="col-md-3">
                ${userAdminInstance[1]} ${userAdminInstance[2]}
            </div>
        </div>
        </g:each>
        <br/>

        All students IOFB lecture test history
        <div class="row">

                <div class="col-md-1">
                </div>
                <div class="col-md-2">
                    <div class="text-info">User ID</div>
                </div>
                <div class="col-md-3">
                    <div class="text-info">Name</div>
                </div>
                <div class="col-md-4">
                    <div class="text-info">Test</div>
                </div>
                <div class="col-md-1">
                    <div class="text-info">Highest mark (%)</div>
                </div>
        </div>
        <br/>

        <g:each in="${userTestSessions}" status="i" var="userTestSessionInstance">

        	<div class="row">

                <div class="col-md-1">
                </div>

                <div class="col-md-2">
                    ${userTestSessionInstance[0]}
                </div>
                <div class="col-md-3">
                    ${userTestSessionInstance[1]} ${userTestSessionInstance[2]} 
                </div>
                <div class="col-md-4">
                    Name: ${userTestSessionInstance[3]}<br/>
                    Lecture: ${userTestSessionInstance[4]}<br/>
                    Module: ${userTestSessionInstance[5]}<br/>
                </div>
                <div class="col-md-1">
                    ${userTestSessionInstance[6]}
                </div>
            </div>
            <hr/>
    	</g:each>

    </body>
</html>