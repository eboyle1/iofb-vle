<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <p>
        Students who have 100% in all 8 IOFB lecture tests
        </p>

        <table class="table table-striped">
            <thead>
                <tr>
                    <g:sortableColumn property="userId" title="User ID" />
                    <g:sortableColumn property="lastName" title="Name" />
                </tr>
            </thead>
            <tbody>
                <g:each in="${userAdmins}" status="i" var="userAdminInstance">
                    <tr>
                        <td>${userAdminInstance.userId}</td>
                        <td>${userAdminInstance.firstName} ${userAdminInstance.lastName}</td>
                    </tr>
                </g:each>
            </tbody>
        </table>

     </body>
</html>