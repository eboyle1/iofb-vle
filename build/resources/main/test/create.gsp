<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'test.label', default: 'Test')}" />
        <title><g:message code="default.create.label" args="[entityName]" /></title>
    </head>
    <body>

        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${test.lecture.module.course.id}"><f:display bean="test" property="lecture.module.course.name" /></g:link> > <g:link controller="module" action="show" id="${test.lecture.module.id}"><f:display bean="test" property="lecture.module.name" /></g:link> > <g:link controller="lecture" action="listTests" id="${test.lecture.id}"><f:display bean="test" property="lecture.name" /></g:link> > Create Test</p>

        <div class="panel panel-primary">
            <div class="panel-heading"><g:message code="default.create.label" args="[entityName]" /></div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                <g:hasErrors bean="${test}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${test}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>

            <g:form url="[resource:test, action:'save']" class="form-horizontal" method="POST" >
                <g:hiddenField name="version" value="${test?.version}" />

                <fieldset class="form">
                    <g:render template="form_fields_create"/>
                </fieldset>
                <div class="btn-toolbar">
                    <button type="submit" class="btn btn-primary pull-right" value="${message(code: 'default.button.create.label', default: 'Create')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Create</button>
                    <g:link controller="lecture" action="listTests" id="${test.lecture.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
                </div>
            </g:form>
        </div>
        </div>

    </body>
</html>
