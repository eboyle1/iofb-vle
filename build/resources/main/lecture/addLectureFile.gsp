<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.addFile.label" args="[entityName]" /></title>

    </head>
    <body>     

        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${lecture.module.course.id}"><f:display bean="lecture" property="module.course.name" /></g:link> > <g:link controller="module" action="show" id="${lecture.module.id}"><f:display bean="lecture" property="module.name" /></g:link> > Add File to Lecture</p>

    	<div class="panel panel-primary">
        	<div class="panel-heading"><g:message code="default.addLectureFile.label" args="[entityName]" /></div>
        	<div class="panel-body">
             	<ul class="errors" role="alert">
                <g:hasErrors bean="${fileCommand}">
                    <div class="alert alert-dismissable alert-danger">
                        <h4>Please fix the following error(s)</h4>
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <g:eachError bean="${fileCommand}" var="error">
                			<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                		</g:eachError>
                    </div>
                </g:hasErrors>
                
                </ul>


                <p>
                <b>${lecture.name}</b>
                </p>
                
                <g:if test="${lecture.lectureFile}">
                <p>
                This lecture already has a main content file. This must be removed if a new file is added.
            	</p>
            	<p>
				File: ${lecture.lectureFile.fileName}
				</p>
				<g:form controller="lecture" id="${lecture.id}" method="DELETE">
        		<g:actionSubmit action="deleteLectureFile" value="Delete lecture file" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
        		</g:form>
                <div class="btn-toolbar">
                     <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
                </div>
				</g:if>

                <g:else>
				<p>
				This is the main content of the lecture and should be an Adobe Presenter package in .zip file format.
				</p>

        		<g:uploadForm name="upload" action="uploadLectureFile" method="post" controller="Lecture">
 
            	<g:hiddenField name="lectureId" value="${lecture.id}" />
 
            	<input type="file" class="btn btn-default" name="uploadedFile" />
            	<br/>
                Display text for file (optional): <input type="text" name="uploadedLectureFileName" />
                <br/>

                <div class="btn-toolbar">
                     <button type="submit" class="btn btn-primary pull-right" value="${message(code: 'lecture.lectureFile.upload.label', default: 'Upload')}" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Upload</button>
                     <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</g:link>
                </div>

        		</g:uploadForm>
                </g:else>
        	</div>
        </div>

    </body>
</html>