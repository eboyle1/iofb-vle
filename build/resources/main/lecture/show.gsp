<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'lecture.label', default: 'Lecture')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${lecture.module.course.id}"><f:display bean="lecture" property="module.course.name" /></g:link> > <g:link controller="module" action="show" id="${lecture.module.id}"><f:display bean="lecture" property="module.name" /></g:link> > <f:display bean="lecture" property="name" /></p>

        <div class="well">
            <p><b><f:display bean="lecture" property="name" /></b></p>
            <p><f:display bean="lecture" property="description" /></p>  
            <p>Author(s): <f:display bean="lecture" property="authors" />, Editor(s): <f:display bean="lecture" property="editors" /></p>
            <p><f:display bean="lecture" property="learningObjectives" /></p>
            <div class="box">
                <p>Lecture content</p>

                <g:if test="${lecture.lectureFile}">
                <ul><li><a href="<g:resource base="${grailsApplication.config.external.webserver}" dir="lectureFiles" file="${lecture.id}/${lecture.lectureFile.baseFileName}/index.htm"/>"><f:display bean="lecture" property="lectureFile.name" /></a></li></ul>
                </g:if>

                <p>Supporting material</p>

                <g:if test="${lecture.contentFiles}">
                    <ul>
                    <g:each in="${lecture.contentFiles}" var="contentFile">
                    <li><a href="<g:resource base="${grailsApplication.config.external.webserver}" dir="lectureFiles" file="${lecture.id}/${contentFile.fileName}"/>">${contentFile.name}</a></li>
                    </g:each>
                    </ul>
                </g:if>

                <p>Tests</p>

                <g:if test="${lecture.tests}">
                    <ul>
                    <g:each in="${lecture.tests.sort{it.orderNo}}" var="test">
                    <li><g:link controller="userTest" action="beginTest" params="[testId:test.id]">${test.name}</g:link></li>
                    </g:each>
                    </ul>
                </g:if>

            </div>

            <div class="show-buttons">
            <g:form url="[resource:lecture, action:'delete']" method="DELETE">
                <div class="btn-toolbar">       

                    <g:link controller="lecture" action="edit" id="${lecture.id}" class="btn btn-primary">
                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</g:link>
<%--
                    <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure? This action cannot be undone.');"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
--%>                    
                    <p:renderLectureDeleteButton curLecture="${lecture}"/>

                    <g:link controller="module" action="show" id="${lecture.module.id}" class="btn btn-nav">Module</g:link>

                </div>
            </g:form>
            </div>

        </div>
    </body>
</html>
