<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'module.label', default: 'Module')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

        <div  role="main">
            <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${module.course.id}"><f:display bean="module" property="course.name" /></g:link> > <f:display bean="module" property="name" /></p>
        <div class = "title_left">
            <h3>Lectures for module:<br/> <f:display bean="module" property="name" /></h3>
        </div>
        <div class = "title_right">
            <g:link controller="module" id="${module.id}" action="studentView" class="btn btn-nav">View module</g:link>
            <g:link controller="lecture" action="create" params="[module:module.id]" class="btn btn-nav">New lecture</g:link>
        </div>
        </div>

        <table class="table table-striped">
            <thead>
                <tr>                   
                    <%--<g:sortableColumn property="name" title="Name" />--%>
                    <th><div class="name-header">Name</div></th>
                    <th><div class="action-header">Action</div></th>
                </tr>
            </thead>
            <tbody>
                <g:each in="${module.lectures.sort{it.orderNo}}" status="i" var="lectureInstance">
                    <tr>
                        <td><g:link controller="lecture" action="show" id="${lectureInstance.id}">${lectureInstance.name}</g:link></td>
                        <td>
                        <div class="action">
                        <div class="dropdown">
                        <button class="dropbtn" type="button" data-toggle="dropdown">Select <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><g:link controller="lecture" action="edit" id="${lectureInstance.id}">Edit lecture</g:link></li>
                            <li><g:link controller="lecture" action="addLectureFile" id="${lectureInstance.id}">Add lecture file</g:link></li>
                            <li><g:link controller="lecture" action="addContentFile" id="${lectureInstance.id}">Add supporting files</g:link></li>
                            <li><g:link controller="test" action="create" params="[lecture:lectureInstance.id]">Create test</g:link></li>
                            <li><g:link controller="lecture" action="listTests" id="${lectureInstance.id}">List tests</g:link></li>
                            <li>
                            <g:form controller="lecture" id="${lectureInstance.id}" method="DELETE">
<%--
                            <g:actionSubmit class="delete-button" action="delete" value="Delete lecture" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
--%>
                            <p:renderLectureDeleteLink curLecture="${lectureInstance}"/>
                            </g:form>
                            </li>
                        </ul>
                        </div>    
                        </div>                          
                        </td>
                    </tr>
                </g:each>
            </tbody>
        </table>
<%--
        <div class="pagination">
            <p:bootstrapPagination total="${module.lectures?.size() ?: 0}" domainBean="uk.ac.ed.bric.elearn.vle.Module" />
        </div>
--%>
        </body>
</html>
