<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'module.label', default: 'Module')}" />
        <title><g:message code="default.show.label" args="[entityName]" /></title>
    </head>
    <body>

    	<div  role="main">
            <p><g:link controller="course" action="index">Courses</g:link> > <g:link controller="course" action="show" id="${module.course.id}"><f:display bean="module" property="course.name" /></g:link> > <f:display bean="module" property="name" /></p>
	        <div class = "title_left">
    			<h3>Module: <f:display bean="module" property="name" /></h3>
            </div>
        	<div class = "title_right">
            </div>
        </div>

        <div class="listLectures">
        Contents:
        <div class="listLectures-indent">
        <g:each in="${module.lectures.sort{it.orderNo}}" status="i" var="lectureInstance">
            <f:display bean="${lectureInstance}" property="orderNo" />.
            <a href="#${lectureInstance.id}"><f:display bean="${lectureInstance}" property="name" /></a>
            <br/>
        </g:each>
        </div>
        </div>

        <div class="listLectures">
       	<g:each in="${module.lectures.sort{it.orderNo}}" status="i" var="lectureInstance">
        <g:if test="${lectureInstance.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]'}">
			<div class="well">

    	        <p><a name="${lectureInstance.id}"></a><b><f:display bean="${lectureInstance}" property="name" /></b></p>
<%--
                <div class="row">
                    <div class="col-md-4">
                        <p>Description: <f:display bean="${lectureInstance}" property="description" /></p> 
                        <p>Author(s): <f:display bean="${lectureInstance}" property="authors" />, Editor(s): <f:display bean="${lectureInstance}" property="editors" /></p>
                    </div>
                    <div class="col-md-8">
                        <f:display bean="${lectureInstance}" property="learningObjectives" />
                    </div>
                </div>
--%>
<%--
            	<p><f:display bean="${lectureInstance}" property="description" /></p>  
            	<p>Author(s): <f:display bean="${lectureInstance}" property="authors" />, Editor(s): <f:display bean="${lectureInstance}" property="editors" /></p>
            	<p><f:display bean="${lectureInstance}" property="learningObjectives" /></p>
--%>
               	<div class="box">
<%--
                	<p>Lecture content</p>
--%>
                	<g:if test="${lectureInstance.lectureFile}">
                		<ul><li><a href="<g:resource base="${grailsApplication.config.external.webserver}" dir="lectureFiles" file="${lectureInstance.id}/${lectureInstance.lectureFile.baseFileName}/index.htm"/>"><h4><f:display bean="${lectureInstance}" property="lectureFile.name" /></h4></a></li></ul>
                	</g:if>
<%--
                	<p>Supporting material</p>
--%>
                	<g:if test="${lectureInstance.contentFiles}">
                    	<ul>
                    	<g:each in="${lectureInstance.contentFiles.sort{it.fileType}}" var="contentFile">
                    		<li><a href="<g:resource base="${grailsApplication.config.external.webserver}" dir="lectureFiles" file="${lectureInstance.id}/${contentFile.fileName}"/>">${contentFile.name}</a></li>
 		                </g:each>
                    	</ul>
                	</g:if>
<%--
                	<p>Test your understanding</p>
--%>
                	<g:if test="${lectureInstance.tests}">
                    	<ul>
                    	<g:each in="${lectureInstance.tests.sort{it.orderNo}}" var="test">
                        <g:if test="${test.isVisible || sec.loggedInUserInfo(field:'authorities') == '[ROLE_ADMIN]'}">
                    		<li>
                            <g:link controller="userTest" action="beginTest" params="[testId:test.id]">Test your understanding with ${test.name}</g:link>
                            <g:set var="testTaken" value="${false}" />
                            <g:each in="${userTestSessions}" status="j" var="userTestSessionInstance">
                                <g:if test="${userTestSessionInstance[1] == test.id}">
                                    &nbsp;&nbsp;Your highest mark for this test: ${userTestSessionInstance[2]}%
                                    <g:set var="testTaken" value="${true}" />
                                </g:if>
                            </g:each>
                            <g:if test="${!testTaken}">
                                &nbsp;&nbsp;You have not taken this test yet
                            </g:if>
                             </li>
                        </g:if>
                    	</g:each>
                    	</ul>
<%--
                        <div class="test-indent"><p>An unlimited number of attempts are allowed for each test. You have to eventually achieve 100% in each test in order to complete this course</p></div>
--%>
               		</g:if>
                    <g:else>
                        <div class="test-indent"><p>This lecture has no tests</p></div>
                    </g:else>

            	</div>
            </div>
        </g:if>    
        </g:each>
    	</div>
<%--
    	<div class="well">
    		<p><b>Resources</b></p><f:display bean="module" property="moduleResource" />
    	</div>
--%>
    </body>
</html>
