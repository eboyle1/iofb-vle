<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edinburgh Imaging Academy</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

          <ckeditor:resources/>

</head>
<body>


     <div class="panel panel-primary">
            <div class="panel-heading">New Lecture</div>
            <div class="panel-body">
                <ul class="errors" role="alert">
                
                
                </ul>
    
            <form action="/lecture/update/1" method="post" class="form-horizontal" ><input type="hidden" name="_method" value="PUT" id="_method" />
                <input type="hidden" name="version" value="0" id="version" />
                <fieldset class="form">
                    	<div class="col-md-12">

        <div class="form-group">
            <label for="name" class="col-md-2 control-label">Name <span class="required-indicator">*</span></label>
             <div class="col-md-10 ">
                    <input type="text" name="name" value="" class="form-control" id="name" />
             </div>
        </div>

		<div class="form-group">
	        <label for="title" class="col-md-2 control-label">Title <span class="required-indicator">*</span></label>
	         <div class="col-md-10 ">
	         		<input type="text" name="title" value="" class="form-control" id="title" />
	         </div>
	    </div>

       
	    <div class="form-group">
            <label for="description" class="col-md-2 control-label">Description <span class="required-indicator">*</span></label>
             <div class="col-md-10 ">
             		<input type="text" name="description" value="" class="form-control" id="description" />
             		
             </div>
        </div>

        <div class="form-group">
            <label for="authors" class="col-md-2 control-label">Author(s) </label>
             <div class="col-md-8 ">
                    <input type="text" name="authors" value="" class="form-control" id="authors" />
                    
             </div>
        </div>

        <div class="form-group">
            <label for="editors" class="col-md-2 control-label">Editor(s) </label>
             <div class="col-md-10 ">
                    <input type="text" name="editors" value="" class="form-control" id="editors" />
                    
             </div>
        </div>
       
               <div class="form-group">
        <label for="learningObjectives" class="col-md-2 control-label">Learning Objectives </label>
            <div class="col-md-10">
                <textarea id="learningObjectives" name="learningObjectives">
                  
                </textarea>
<script type="text/javascript">
CKEDITOR.replace('learningObjectives', {
customConfig: '/assets/ckconfig.js',
filebrowserLinkBrowseUrl: '/ck/ofm?fileConnector=/ck/ofm/filemanager&type=File&viewMode=grid',
filebrowserImageBrowseUrl: '/ck/ofm?fileConnector=/ck/ofm/filemanager&type=Image&viewMode=grid',
filebrowserImageUploadUrl: '/ck/uploader?type=Image',
toolbar: 'custom',
height: '300px',
width: '100%'}
);
</script>

            </div>
        </div>

        <div class="form-group">
        <label for="lectureContent" class="col-md-2 control-label">Lecture Content </label>
            <div class="col-md-10">
                <textarea id="lectureContent" name="lectureContent">
                  
                </textarea>
<script type="text/javascript">
CKEDITOR.replace('lectureContent', {
customConfig: '/assets/ckconfig.js',
filebrowserLinkBrowseUrl: '/ck/ofm?fileConnector=/ck/ofm/filemanager&type=File&viewMode=grid',
filebrowserImageBrowseUrl: '/ck/ofm?fileConnector=/ck/ofm/filemanager&type=Image&viewMode=grid',
filebrowserImageUploadUrl: '/ck/uploader?type=Image',
toolbar: 'custom',
height: '300px',
width: '100%'}
);
</script>

            </div>
        </div>
    
    </div>

    


	

                </fieldset>
                <div class="btn-toolbar">
                    <button type="submit" class="btn btn-primary pull-right" value="Update" ><i class="fa fa-floppy-o" aria-hidden="true"></i> Create</button>
                    <a href="/lecture/index" class="btn btn-default pull-right"><i class="fa fa-times-circle" aria-hidden="true"></i> Cancel</a>
                </div>
            </form>
        </div>
        </div>
    
            
 </body>
</html