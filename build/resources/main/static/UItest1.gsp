<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Edinburgh Imaging Academy</title>

    <asset:link rel="icon" href="favicon.ico" type="image/x-ico" />

    <g:javascript>

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction1() {
    document.getElementById("myDropdown1").classList.toggle("show1");
}

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction2() {
    document.getElementById("myDropdown2").classList.toggle("show2");
}

/* When the user clicks on the button, 
toggle between hiding and showing the dropdown content */
function myFunction3() {
    document.getElementById("myDropdown3").classList.toggle("show3");
}

// Close the dropdown menu if the user clicks outside of it
window.onclick = function(event) {
  if (!event.target.matches('.dropbtn1')) {

    var dropdowns1 = document.getElementsByClassName("dropdown-content1");
    var i1;
    for (i1 = 0; i1 < dropdowns1.length; i1++) {
      var openDropdown1 = dropdowns1[i1];
      if (openDropdown1.classList.contains('show1')) {
        openDropdown1.classList.remove('show1');
      }
    }
  }

  if (!event.target.matches('.dropbtn2')) {

    var dropdowns2 = document.getElementsByClassName("dropdown-content2");
    var i2;
    for (i2 = 0; i2 < dropdowns2.length; i2++) {
      var openDropdown2 = dropdowns2[i2];
      if (openDropdown2.classList.contains('show2')) {
        openDropdown2.classList.remove('show2');
      }
    }
  }

  if (!event.target.matches('.dropbtn3')) {

    var dropdowns3 = document.getElementsByClassName("dropdown-content3");
    var i3;
    for (i3 = 0; i3 < dropdowns3.length; i3++) {
      var openDropdown3 = dropdowns3[i3];
      if (openDropdown3.classList.contains('show3')) {
        openDropdown3.classList.remove('show3');
      }
    }
  }
}

	</g:javascript>

	<style>
.btn1 {
    background-color: #4CAF50;
    color: white;
    padding: 5px;
    font-size: 14px;
    border: none;
    cursor: pointer;
}

.btn1:hover, .btn1:focus {
    background-color: #3e8e41;
}

.btn1 a {
    color: white;
    text-decoration: none;
}

	/* Dropdown Button */
.dropbtn1 {
    background-color: #4CAF50;
    color: white;
    padding: 5px;
    font-size: 14px;
    border: none;
    cursor: pointer;
}

.dropbtn2 {
    background-color: #4CAF50;
    color: white;
    padding: 5px;
    font-size: 14px;
    border: none;
    cursor: pointer;
}

.dropbtn3 {
    background-color: #4CAF50;
    color: white;
    padding: 5px;
    font-size: 14px;
    border: none;
    cursor: pointer;
}

/* Dropdown button on hover & focus */
.dropbtn1:hover, .dropbtn1:focus {
    background-color: #3e8e41;
}

/* Dropdown button on hover & focus */
.dropbtn2:hover, .dropbtn2:focus {
    background-color: #3e8e41;
}

/* Dropdown button on hover & focus */
.dropbtn3:hover, .dropbtn3:focus {
    background-color: #3e8e41;
}

/* The container <div> - needed to position the dropdown content */
.dropdown1 {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content1 {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content1 a {
    color: black;
    padding: 5px 14px;
    text-decoration: none;
    display: block;
}

/* The container <div> - needed to position the dropdown content */
.dropdown2 {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content2 {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content2 a {
    color: black;
    padding: 5px 14px;
    text-decoration: none;
    display: block;
}

/* The container <div> - needed to position the dropdown content */
.dropdown3 {
    position: relative;
    display: inline-block;
}

/* Dropdown Content (Hidden by Default) */
.dropdown-content3 {
    display: none;
    position: absolute;
    background-color: #f9f9f9;
    min-width: 160px;
    box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
    z-index: 1;
}

/* Links inside the dropdown */
.dropdown-content3 a {
    color: black;
    padding: 5px 14px;
    text-decoration: none;
    display: block;
}

/* Change color of dropdown links on hover */
.dropdown-content1 a:hover {background-color: #f1f1f1}

/* Change color of dropdown links on hover */
.dropdown-content2 a:hover {background-color: #f1f1f1}

/* Change color of dropdown links on hover */
.dropdown-content3 a:hover {background-color: #f1f1f1}

/* Show the dropdown menu (use JS to add this class to the .dropdown-content container when the user clicks on the dropdown button) */
.show1 {display:block;}
.show2 {display:block;}
.show3 {display:block;}

.title_left
{
    float:left;
}
.title_right
{
    float:right;
    margin-top:25px;
    margin-right:140px;
}
</style>

</head>
<body>

   <div  role="main">
   	    <div class ="name">
	    <div class = "title_left">
            <h3>Courses</h3>
                    </div>
            <div class = "title_right">
            <button class="btn1"><a href="/static/UItest4">New course</a></button>
            </div>
        </div>

            <table class="table table-striped">
            <thead>
                    <tr>                
                        <g:sortableColumn property="title" title="Title" />

                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>

                    <tr>             
                        <td><a href="/static/UItest2">Intraorbital Foreign Body Reviewing</a></td>

                        <td>
   
                        <div class="dropdown1">
  						<button onclick="myFunction1()" class="dropbtn1">Select action</button>
  						<div id="myDropdown1" class="dropdown-content1">
 						<a href="/static/UItest2">List modules</a>
						<a href="#">New module</a>
						<a href="#">Edit course</a>
						<a href="#">Delete course</a>
                      	<a href="#">View/edit welcome page</a>	
						<a href="#">New welcome page</a>
						<a href="#">View/edit staff page</a>
						<a href="#">New staff page</a>
						</div>
						</div>

                        </td>
                    </tr>


                     <tr>             
                        <td>Course 2</td>
 
                        <td>
                        	
                        <div class="dropdown2">
  						<button onclick="myFunction2()" class="dropbtn2">Select action</button>
  						<div id="myDropdown2" class="dropdown-content2">
 						<a href="#">List modules</a>
						<a href="#">New module</a>
						<a href="#">Edit course</a>
						<a href="#">Delete course</a>
                      	<a href="#">View/edit welcome page</a>	
						<a href="#">New welcome page</a>
						<a href="#">View/edit staff page</a>
						<a href="#">New staff page</a>
						</div>
						</div>

                        </td>
                    </tr>

                    <tr>             
                        <td>Course 3</td>

                        <td>
                        	
                        <div class="dropdown3">
  						<button onclick="myFunction3()" class="dropbtn3">Select action</button>
  						<div id="myDropdown3" class="dropdown-content3">
 						<a href="#">List modules</a>
						<a href="#">New module</a>
						<a href="#">Edit course</a>
						<a href="#">Delete course</a>
                      	<a href="#">View/edit welcome page</a>	
						<a href="#">New welcome page</a>
						<a href="#">View/edit staff page</a>
						<a href="#">New staff page</a>
						</div>
						</div>
							
                        </td>
                    </tr>

                </tbody>
            </table>
            <div class="pagination">
 	              <p:bootstrapPagination total="3" domainBean="uk.ac.ed.bric.elearn.vle.Lecture" />
            </div>
    </div>
            
 </body>
</html>