<QML>

<QUESTION ID="7500060100562055" DESCRIPTION="IOFB_M1L1_Q1_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L1 Overview" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">With regards to principles behind screening for metallic IOFBs, answer the following as TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Orbital tissues fix metallic IOFBs in place by fibrosis</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Magnetic torque may cause a metallic IOFBs to move</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Metallic IOFBs may damage orbital tissues irreparably if they move</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. The issues around metallic IOFBs causing damage are only theoretical</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Metallic IOFBs can only be detected on plain orbital radiography</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">A = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">D = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">E = FALSE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="4836405917906130" DESCRIPTION="IOFB_M1L1_Q2_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L1 Overview" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">With regards to consideration of radiographer led metallic IOFB screening &amp; orbital radiograph reporting, which of the following are likely to be INCREASED or DECREASED:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>INCREASED</OPTION>
      <OPTION>DECREASED</OPTION>
      <CONTENT TYPE="text/html">A. MR patient list efficiency</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>INCREASED</OPTION>
      <OPTION>DECREASED</OPTION>
      <CONTENT TYPE="text/html">B. MR patient list disruption</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>INCREASED</OPTION>
      <OPTION>DECREASED</OPTION>
      <CONTENT TYPE="text/html">C. Number of wasted MR scanning slots</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>INCREASED</OPTION>
      <OPTION>DECREASED</OPTION>
      <CONTENT TYPE="text/html">D. Time waiting for radiologist review of orbital radiographs</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>INCREASED</OPTION>
      <OPTION>DECREASED</OPTION>
      <CONTENT TYPE="text/html">E. Speed of decision making when patients present for MR with a history of metallic IOFB exposure</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "INCREASED"</CONDITION>
    <CONTENT TYPE="text/html">A = INCREASED</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "DECREASED"</CONDITION>
    <CONTENT TYPE="text/html">B = DECREASED</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "DECREASED"</CONDITION>
    <CONTENT TYPE="text/html">C = DECREASED</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "DECREASED"</CONDITION>
    <CONTENT TYPE="text/html">D = DECREASED</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "INCREASED"</CONDITION>
    <CONTENT TYPE="text/html">E = INCREASED</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="2489837794305512" DESCRIPTION="IOFB_M1L1_Q3_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L1 Overview" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">With regards to establishing a specialist radiographer metallic IOFB reporting service, answer the following as TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. Radiographers must attend and pass approved training</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. Radiographers may work outwith an approved framework</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. Radiographers need not inform the employer</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. Radiographers must participate in regular audit</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. Radiographers must demonstrate competency</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">B = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "FALSE"</CONDITION>
    <CONTENT TYPE="text/html">C = FALSE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="8934549908984452" DESCRIPTION="IOFB_M1L1_Q4_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L1 Overview" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">With regards to gathering information from a patient being screened for MR, which of the following indicate a higher likelihood of metallic IOFB? Answer the following as TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. The patient worked as a sheet metal worker</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. The patient has visited an ophthalmologist for eye trauma</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. The patient's hobby includes chiselling</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. The patient's hobby includes shooting</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. The patient received blast injuries while deployed as a soldier</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

<QUESTION ID="1818721002028121" DESCRIPTION="IOFB_M1L1_Q5_2015_AF" TOPIC="MVMGradSchool\IOFB 2015-2016\AF M1L1 Overview" STATUS="Normal" CEILING="5">
  <CONTENT TYPE="text/html">Some patients may not disclose that they have had a metallic IOFB injury. Which of the following may raise suspicion that a patient will not disclose a relevant history? Answer the following as TRUE or FALSE:</CONTENT>
  <ANSWER QTYPE="MAT">
    <CHOICE ID="0">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">A. The patient is absolutely certain an MR scan will answer his health questions</CONTENT>
    </CHOICE>
    <CHOICE ID="1">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">B. The patient is accompanied by her spouse because she has dementia</CONTENT>
    </CHOICE>
    <CHOICE ID="2">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">C. The patient is accompanied by a translator because he only speaks Mandarin</CONTENT>
    </CHOICE>
    <CHOICE ID="3">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">D. The patient asks you to speak very loudly into her hearing aid</CONTENT>
    </CHOICE>
    <CHOICE ID="4">
      <OPTION>TRUE</OPTION>
      <OPTION>FALSE</OPTION>
      <CONTENT TYPE="text/html">E. The patient is accompanied by his carer due to longstanding autism</CONTENT>
    </CHOICE>
  </ANSWER>
  <OUTCOME ID="0" ADD="1" CONTINUE="TRUE">
    <CONDITION>"0" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">A = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="1" ADD="1" CONTINUE="TRUE">
    <CONDITION>"1" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">B = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="2" ADD="1" CONTINUE="TRUE">
    <CONDITION>"2" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">C = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="3" ADD="1" CONTINUE="TRUE">
    <CONDITION>"3" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">D = TRUE</CONTENT>
  </OUTCOME>
  <OUTCOME ID="4" ADD="1" CONTINUE="TRUE">
    <CONDITION>"4" MATCHES "TRUE"</CONDITION>
    <CONTENT TYPE="text/html">E = TRUE</CONTENT>
  </OUTCOME>
</QUESTION>

</QML>
